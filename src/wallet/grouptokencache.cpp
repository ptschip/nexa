// Copyright (c) 2015-2022 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
#include "wallet/grouptokencache.h"
#include "consensus/grouptokens.h"
#include "main.h"

bool GetTokenDescription(const CScript &script, std::vector<std::string> &_vDesc)
{
    _vDesc.clear();

    CScript::const_iterator pc = script.begin();
    opcodetype op;
    std::vector<unsigned char> vchRet;

    // Check we have an op_return
    script.GetOp(pc, op, vchRet);
    if (op != OP_RETURN)
        return false;

    // Check for correct group id
    script.GetOp(pc, op, vchRet);
    uint32_t grpId;
    std::stringstream ss;
    std::reverse(vchRet.begin(), vchRet.end());
    ss << std::hex << HexStr(vchRet);
    ss >> grpId;
    if (grpId != DEFAULT_OP_RETURN_GROUP_ID)
        return false;

    // Get labels
    int count = 0;
    while (script.GetOp(pc, op, vchRet))
    {
        if (op != OP_0)
        {
            if (count != 4)
            {
                if (count == 3)
                {
                    // Convert hash stored as a vector of unsigned chars to a string.
                    uint256 hash(&vchRet.data()[0]);
                    _vDesc.push_back(hash.ToString());
                }
                else
                {
                    std::string s(vchRet.begin(), vchRet.end());
                    _vDesc.push_back(s);
                }
            }
            else // 5th parameter in op return is the number of decimals
            {
                uint8_t amt;
                if (0 <= op && op <= OP_PUSHDATA4)
                {
                    amt = CScriptNum(vchRet, false, CScriptNum::MAXIMUM_ELEMENT_SIZE_64_BIT).getint64();
                }
                else if (op == 0)
                    amt = 0;
                else
                    amt = op - OP_1 + 1;
                _vDesc.push_back(std::to_string(amt));
            }
        }
        else
            _vDesc.push_back("");
        count++;
    }

    if (!_vDesc.empty())
    {
        while (_vDesc.size() < 4)
        {
            _vDesc.push_back("");
        }
        while (_vDesc.size() < 5)
        {
            _vDesc.push_back("0");
        }
    }
    else
    {
        std::vector<std::string> vEmptyDesc{"", "", "", "", "0"};
        _vDesc.swap(vEmptyDesc);
    }
    return true;
}

void AccumulateTokenData(CTransactionRef ptx,
    CCoinsViewCache &view,
    std::map<CGroupTokenID, CAuth> &accumulatedAuthorities,
    std::map<CGroupTokenID, CAmount> &accumulatedMintages)
{
    if (ptx->IsCoinBase())
        return;

    struct Amounts
    {
        CAmount nTokenInputs = 0;
        CAmount nTokenOutputs = 0;
    };
    std::map<CGroupTokenID, Amounts> mintages;

    // Get mintages/authorities from the inputs
    CAuth authority;
    for (const CTxIn &txin : ptx->vin)
    {
        Coin coin;
        view.GetCoin(txin.prevout, coin);
        if (!coin.IsSpent())
        {
            const CTxOut &txout = coin.out;
            CGroupTokenInfo tg(txout);

            // Get the mintages in the inputs
            if (tg.associatedGroup != NoGroup && !tg.isAuthority())
            {
                mintages[tg.associatedGroup].nTokenInputs += tg.quantity;
            }

            // Get the authorities in the inputs
            if (tg.associatedGroup != NoGroup && tg.isAuthority())
            {
                if (accumulatedAuthorities.count(tg.associatedGroup))
                {
                    std::swap(authority, accumulatedAuthorities[tg.associatedGroup]);
                }
                authority.nMint -= tg.allowsMint();
                authority.nMelt -= tg.allowsMelt();
                authority.nRenew -= tg.allowsRenew();
                authority.nRescript -= tg.allowsRescript();
                authority.nSubgroup -= tg.allowsSubgroup();
                accumulatedAuthorities[tg.associatedGroup] = authority;
            }
        }
        else
        {
            DbgAssert(!coin.IsSpent(), );
        }
    }

    // Get the mintages/authorities in the outputs.
    bool fHaveAuthority = false;
    bool fHaveOpReturn = false;
    for (size_t i = 0; i < ptx->vout.size(); i++)
    {
        const CTxOut &out = ptx->vout[i];
        CGroupTokenInfo tg(out.scriptPubKey);

        // Get mintages from the outputs
        if ((tg.associatedGroup != NoGroup) && !tg.isAuthority())
        {
            mintages[tg.associatedGroup].nTokenOutputs += tg.quantity;
        }

        // Get authorities from the outputs
        if (out.scriptPubKey[0] == OP_RETURN)
        {
            fHaveOpReturn = true;
            continue;
        }

        if ((tg.associatedGroup != NoGroup) && tg.isAuthority())
        {
            fHaveAuthority = true;

            if (accumulatedAuthorities.count(tg.associatedGroup))
            {
                std::swap(authority, accumulatedAuthorities[tg.associatedGroup]);
            }

            authority.nMint += tg.allowsMint();
            authority.nMelt += tg.allowsMelt();
            authority.nRenew += tg.allowsRenew();
            authority.nRescript += tg.allowsRescript();
            authority.nSubgroup += tg.allowsSubgroup();
            accumulatedAuthorities[tg.associatedGroup] = authority;
        }
    }

    // If inputs for a group id equals zero and ouputs > 0, then it's a MINT.
    // if intputs > outputs, then it's a MELT.
    for (auto &it : mintages)
    {
        CAmount &nTokenInputs = it.second.nTokenInputs;
        CAmount &nTokenOutputs = it.second.nTokenOutputs;

        // Check for Mint
        if (nTokenInputs == 0 && nTokenOutputs > 0)
        {
            if (!accumulatedMintages.count(it.first))
                accumulatedMintages[it.first] = nTokenOutputs;
            else
                accumulatedMintages[it.first] += nTokenOutputs;
        }

        // Check for Melt
        if (nTokenInputs > nTokenOutputs)
        {
            CAmount nMeltAmount = nTokenInputs - nTokenOutputs;
            if (!accumulatedMintages.count(it.first))
                accumulatedMintages[it.first] = -nMeltAmount;
            else
                accumulatedMintages[it.first] -= nMeltAmount;
        }
    }

    // if we have both an authority and op_return then look for and update
    // the token description information
    if (fHaveAuthority && fHaveOpReturn)
    {
        tokencache.ProcessTokenDescriptions(ptx);
    }
}

// Token Description Cache methods
void CTokenDescCache::AddTokenDesc(const CGroupTokenID &_grpID, const std::vector<std::string> &_desc)
{
    // When we add a token description we add it only to the database but not the cache.  We instead
    // add values to the cache when reading from the database, when needed. This way we only fill the in
    // memory cache with values we know are needed for the token wallet.  These values would be pulled
    // into the cache during node startup so there is no real performance loss during normal wallet use.
    LOCK(cs_tokencache);
    cache.erase(_grpID);
    ptokenDesc->WriteDesc(_grpID, _desc);
}

const std::vector<std::string> CTokenDescCache::GetTokenDesc(const CGroupTokenID &_grpID)
{
    std::vector<std::string> _desc;

    LOCK(cs_tokencache);
    if (cache.count(_grpID))
    {
        return cache[_grpID];
    }
    else if (ptokenDesc->ReadDesc(_grpID, _desc))
    {
        // Limit the size of cache.  If the cache size is exceeded, which is unlikely, then
        // we don't add any more values to the cache and we rather get descrptions directly
        // from the database.
        if (cache.size() <= nMaxCacheSize)
        {
            cache.emplace(_grpID, _desc);
        }
        return _desc;
    }
    else
    {
        return {};
    }
}

void CTokenDescCache::EraseTokenDesc(const CGroupTokenID &_grpID)
{
    LOCK(cs_tokencache);
    cache.erase(_grpID);
}

void CTokenDescCache::SetSyncFlag(const bool fSet)
{
    LOCK(cs_tokencache);
    ptokenDesc->WriteSyncFlag(fSet);
}

bool CTokenDescCache::GetSyncFlag()
{
    bool fSet = false;
    LOCK(cs_tokencache);
    ptokenDesc->ReadSyncFlag(fSet);

    return fSet;
}

void CTokenDescCache::ProcessTokenDescriptions(CTransactionRef ptx)
{
    bool fNewDesc = false;
    for (size_t i = 0; i < ptx->vout.size(); i++)
    {
        const CTxOut &out = ptx->vout[i];
        CGroupTokenInfo tg(out.scriptPubKey);
        if ((tg.associatedGroup != NoGroup) && tg.isAuthority())
        {
            // If we have an authority then we have to check all the outputs again for an OP_RETURN
            for (size_t j = 0; j < ptx->vout.size(); j++)
            {
                if (i == j) // skip the one we already checked in the upper loop
                    continue;

                // find op_return associated with the tx if there is one
                const CTxOut &out2 = ptx->vout[j];
                if (out2.scriptPubKey[0] == OP_RETURN)
                {
                    std::vector<std::string> vDesc;
                    if (!GetTokenDescription(out2.scriptPubKey, vDesc))
                    {
                        return;
                    }

                    // return null authorites for now
                    vDesc.push_back("0");
                    vDesc.push_back("0");
                    vDesc.push_back("0");
                    vDesc.push_back("0");
                    vDesc.push_back("0");

                    AddTokenDesc(tg.associatedGroup, vDesc);
                    fNewDesc = true;
                    break;
                }
            }
            if (fNewDesc)
                break;
        }
    }
}

void CTokenDescCache::ApplyTokenAuthorities(std::map<CGroupTokenID, CAuth> &accumulatedAuthorities)
{
    LOCK(cs_tokencache);
    for (auto &it : accumulatedAuthorities)
    {
        std::vector<std::string> vDescNone{"", "", "", "", "0", "0", "0", "0", "0", "0"};
        std::vector<std::string> vDesc = tokencache.GetTokenDesc(it.first);

        // Special case if the token was originally created "new" but without any
        // ticker/name/url/hash/decimals defined.  It will not have had an OP_RETURN in
        // which case there would not be an initial description string in the tracking database created
        // so here we add one when we get our first authority to track.....
        if (vDesc.empty())
        {
            std::swap(vDesc, vDescNone);
        }

        if (vDesc.size() >= 10)
        {
            // Get authorities
            int nMint = 0;
            int nMelt = 0;
            int nRenew = 0;
            int nRescript = 0;
            int nSubgroup = 0;
            try
            {
                nMint = stoi(vDesc[5]) + it.second.nMint;
                nMelt = stoi(vDesc[6]) + it.second.nMelt;
                nRenew = stoi(vDesc[7]) + it.second.nRenew;
                nRescript = stoi(vDesc[8]) + it.second.nRescript;
                nSubgroup = stoi(vDesc[9]) + it.second.nSubgroup;
            }
            catch (...)
            {
                DbgAssert(false, );
            }

            std::vector<std::string> vNewDesc = {vDesc[0], vDesc[1], vDesc[2], vDesc[3], vDesc[4]};
            vNewDesc.push_back(std::to_string(nMint));
            vNewDesc.push_back(std::to_string(nMelt));
            vNewDesc.push_back(std::to_string(nRenew));
            vNewDesc.push_back(std::to_string(nRescript));
            vNewDesc.push_back(std::to_string(nSubgroup));

            AddTokenDesc(it.first, vNewDesc);
        }
        else
            DbgAssert(false, ); // should never happen
    }
}

void CTokenDescCache::RemoveTokenAuthorities(std::map<CGroupTokenID, CAuth> &accumulatedAuthorities)
{
    LOCK(cs_tokencache);
    for (auto &it : accumulatedAuthorities)
    {
        auto vDesc = tokencache.GetTokenDesc(it.first);
        if (vDesc.size() >= 10)
        {
            // Get authorities
            int nMint = 0;
            int nMelt = 0;
            int nRenew = 0;
            int nRescript = 0;
            int nSubgroup = 0;
            try
            {
                nMint = stoi(vDesc[5]) - it.second.nMint;
                nMelt = stoi(vDesc[6]) - it.second.nMelt;
                nRenew = stoi(vDesc[7]) - it.second.nRenew;
                nRescript = stoi(vDesc[8]) - it.second.nRescript;
                nSubgroup = stoi(vDesc[9]) - it.second.nSubgroup;
            }
            catch (...)
            {
                DbgAssert(false, );
            }

            std::vector<std::string> vNewDesc = {vDesc[0], vDesc[1], vDesc[2], vDesc[3], vDesc[4]};
            vNewDesc.push_back(std::to_string(nMint));
            vNewDesc.push_back(std::to_string(nMelt));
            vNewDesc.push_back(std::to_string(nRenew));
            vNewDesc.push_back(std::to_string(nRescript));
            vNewDesc.push_back(std::to_string(nSubgroup));

            AddTokenDesc(it.first, vNewDesc);
        }
        else
            DbgAssert(false, ); // should not happen
    }
}


// Token Mint Cache methods
void CTokenMintCache::AddToCache(const CGroupTokenID &_grpID, const CAmount _mint)
{
    AssertLockHeld(cs_tokenmint);

    // Limit the size of cache.  If the cache size is exceeded, which is unlikely, then
    // we erase the first value and add the new one to the cache. This way any items with
    // high cache hits will always be in memory since mintage lookups could become quite
    // common for some type of tokens such as "game itmes" used multiplayer video games.
    while (cache.size() >= nMaxCacheSize)
    {
        // remove a random element
        auto iter = cache.begin();
        std::advance(iter, GetRand(cache.size() - 1));
        cache.erase(iter);
    }
    cache[_grpID] = _mint;
}

void CTokenMintCache::AddTokenMint(const CGroupTokenID &_grpID, const CAmount _mint)
{
    LOCK(cs_tokenmint);
    CAmount nCurrentMint = GetTokenMint(_grpID) + _mint;
    if (nCurrentMint > 0)
    {
        AddToCache(_grpID, nCurrentMint);
        ptokenMint->WriteMint(_grpID, nCurrentMint);
    }
}

void CTokenMintCache::RemoveTokenMint(const CGroupTokenID &_grpID, const CAmount _melt)
{
    LOCK(cs_tokenmint);
    CAmount nCurrentMint = GetTokenMint(_grpID);
    if (nCurrentMint >= _melt)
    {
        nCurrentMint -= _melt;
    }
    else
    {
        DbgAssert(nCurrentMint >= _melt, );
        return;
    }

    AddToCache(_grpID, nCurrentMint);
    ptokenMint->WriteMint(_grpID, nCurrentMint);
}

uint64_t CTokenMintCache::GetTokenMint(const CGroupTokenID &_grpID)
{
    CAmount nMint;

    LOCK(cs_tokenmint);
    if (cache.count(_grpID))
    {
        return cache[_grpID];
    }
    else if (ptokenMint->ReadMint(_grpID, nMint))
    {
        AddToCache(_grpID, nMint);
        return nMint;
    }
    else
    {
        return 0;
    }
}

void CTokenMintCache::ApplyTokenMintages(std::map<CGroupTokenID, CAmount> &accumulatedMintages)
{
    for (auto &it : accumulatedMintages)
    {
        if (it.second > 0)
            AddTokenMint(it.first, it.second);
        else
            RemoveTokenMint(it.first, abs(it.second));
    }
}

void CTokenMintCache::RemoveTokenMintages(std::map<CGroupTokenID, CAmount> &accumulatedMintages)
{
    for (auto &it : accumulatedMintages)
    {
        if (it.second > 0)
            RemoveTokenMint(it.first, it.second);
        else
            AddTokenMint(it.first, abs(it.second));
    }
}

void CTokenMintCache::SetSyncFlag(const bool fSet)
{
    LOCK(cs_tokenmint);
    ptokenDesc->WriteSyncFlag(fSet);
}

bool CTokenMintCache::GetSyncFlag()
{
    bool fSet = false;
    LOCK(cs_tokenmint);
    ptokenDesc->ReadSyncFlag(fSet);

    return fSet;
}
