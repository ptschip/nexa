<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>Cliquer à droite pour modifier l&apos;adresse ou l&apos;étiquette</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Créer une nouvelle adresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Copier l&apos;adresse courante sélectionnée dans le presse-papiers</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>&amp;Copier l&apos;adresse</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Supprimer l&apos;adresse actuellement sélectionnée de la liste</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Exporter les données de l&apos;onglet courant vers un fichier</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>&amp;Exporter</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>Choisir l&apos;adresse à laquelle envoyer des pièces</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Choisir l&apos;adresse avec laquelle recevoir des pièces</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>C&amp;hoisir</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>Adresses d&apos;envoi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Adresses de réception</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Voici vos adresses Nexa pour envoyer des paiements. Vérifiez toujours le montant et l&apos;adresse du destinataire avant d&apos;envoyer des pièces.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Voici vos adresses Nexa pour recevoir des paiements. Il est recommandé d&apos;utiliser une nouvelle adresse de réception pour chaque transaction.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>Copier l&apos;é&amp;tiquette</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modifier</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>Exporter la liste d&apos;adresses</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Valeurs séparées par des virgules (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>L&apos;exportation a échoué</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Une erreur est survenue lors de l&apos;enregistrement de la liste d&apos;adresses vers %1. Veuillez ressayer plus tard.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(aucune étiquette)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Fenêtre de dialogue de la phrase de passe</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Saisir la phrase de passe</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Nouvelle phrase de passe</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Répéter la phrase de passe</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>Chiffrer le portefeuille</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>Cette opération nécessite votre phrase de passe pour déverrouiller le portefeuille.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Déverrouiller le portefeuille</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>Cette opération nécessite votre phrase de passe pour déchiffrer le portefeuille.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Déchiffrer le portefeuille</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Changer la phrase de passe</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>Confirmer le chiffrement du portefeuille</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Avertissement : si vous chiffrez votre portefeuille et perdez votre phrase de passe, vous &lt;b&gt;PERDREZ TOUS VOS NEXA COINS&lt;/b&gt; !</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Êtes-vous sûr de vouloir chiffrer votre portefeuille ?</translation>
    </message>
    <message>
        <location line="+119"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>Avertissement : la touche Verr. Maj. est activée !</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Portefeuille chiffré</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Saisissez une nouvelle phrase de passe pour le portefeuille.&lt;br/&gt;Veuillez utiliser une phrase composée de &lt;b&gt;dix caractères aléatoires ou plus&lt;/b&gt;, ou bien de &lt;b&gt;huit mots ou plus&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Saisir l&apos;ancienne phrase de passe puis la nouvelle phrase de passe du portefeuille.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation>%1 va se fermer maintenant pour terminer le processus de cryptage. N&apos;oubliez pas que le cryptage de votre portefeuille ne peut pas entièrement protéger vos pièces contre le vol par des logiciels malveillants infectant votre ordinateur.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation>IMPORTANT&#xa0;: toutes les sauvegardes précédentes que vous avez effectuées de votre fichier de portefeuille doivent être remplacées par le fichier de portefeuille crypté nouvellement généré. Pour des raisons de sécurité, vous ne devez plus conserver les sauvegardes précédentes non cryptées, car vos fonds seront en danger si quelqu&apos;un y accède.</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>Le chiffrement du portefeuille a échoué</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>Le chiffrement du portefeuille a échoué en raison d&apos;une erreur interne. Votre portefeuille n&apos;a pas été chiffré.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>Les phrases de passe saisies ne correspondent pas.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>Le déverrouillage du portefeuille a échoué</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>La phrase de passe saisie pour déchiffrer le portefeuille était incorrecte.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>Le déchiffrage du portefeuille a échoué</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>La phrase de passe du portefeuille a été modifiée avec succès.</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation>IP/masque réseau</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation>Banni jusqu&apos;au</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>Agent utilisateur</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation>Raison de l&apos;interdiction</translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+351"/>
        <source>Sign &amp;message...</source>
        <translation>&amp;Signer le message...</translation>
    </message>
    <message>
        <location line="+435"/>
        <source>Synchronizing with network...</source>
        <translation>Synchronisation avec le réseau en cours…</translation>
    </message>
    <message>
        <location line="-556"/>
        <source>&amp;Overview</source>
        <translation>&amp;Vue d&apos;ensemble</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>Nœud</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>Afficher une vue d’ensemble du portefeuille</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation>Demander des paiements (génère des codes QR et %1&#xa0;: URI)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>&amp;Transactions</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Parcourir l&apos;historique des transactions</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation>&amp;Jetons</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation>Parcourir ou envoyer des jetons</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation>Historique des jetons</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation>Parcourir l’historique des jetons</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>Q&amp;uitter</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Quitter l’application</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation>&amp;À propos de %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation>Afficher les informations sur %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>À propos de &amp;Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Afficher des informations sur Qt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;Options…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation>Modifier les options de configuration pour %1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation>&amp;Unlimited...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation>Modifier les options de Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>&amp;Chiffrer le portefeuille...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>Sauvegarder le &amp;portefeuille...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Restore Wallet...</source>
        <translation>&amp;Restaurer le portefeuille...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation>Restaurer le portefeuille depuis un autre emplacement</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>&amp;Changer la phrase de passe...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>Adresses d&apos;&amp;envoi...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>Adresses de &amp;réception...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>Ouvrir un &amp;URI...</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Importing blocks from disk...</source>
        <translation>Importation des blocs à partir du disque...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>Réindexation des blocs sur le disque...</translation>
    </message>
    <message>
        <location line="-554"/>
        <source>Send coins to a Nexa address</source>
        <translation>Envoyer des pièces à une adresse Nexa</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Backup wallet to another location</source>
        <translation>Sauvegarder le portefeuille vers un autre emplacement</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Modifier la phrase de passe utilisée pour le chiffrement du portefeuille</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>Fenêtre de &amp;débogage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Ouvrir une console de débogage et de diagnostic</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>&amp;Vérifier un message...</translation>
    </message>
    <message>
        <location line="+522"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-785"/>
        <source>Wallet</source>
        <translation>Portefeuille</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>&amp;Envoyer</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>&amp;Recevoir</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;Afficher / Cacher</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>Afficher ou masquer la fenêtre principale</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>Chiffrer les clefs privées de votre portefeuille</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Signer les messages avec vos adresses Nexa pour prouver que vous les détenez</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>Vérifier les messages pour vous assurer qu&apos;ils ont été signés avec les adresses Nexa spécifiées</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>&amp;Réglages</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Barre d&apos;outils des onglets</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Afficher la liste d&apos;adresses d&apos;envoi et d&apos;étiquettes utilisées</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Afficher la liste d&apos;adresses de réception et d&apos;étiquettes utilisées</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>Options de ligne de &amp;commande</translation>
    </message>
    <message numerus="yes">
        <location line="+390"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n connexion active avec le réseau Nexa</numerusform>
            <numerusform>%n connexions actives avec le réseau Nexa</numerusform>
        </translation>
    </message>
    <message>
        <location line="+34"/>
        <source>No block source available...</source>
        <translation>Aucune source de blocs disponible...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>%n bloc d&apos;historique transactionnel a été traité</numerusform>
            <numerusform>%n blocs d&apos;historique transactionnel ont été traités</numerusform>
        </translation>
    </message>
    <message>
        <location line="+27"/>
        <source>%1 behind</source>
        <translation>%1 en retard</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>Le dernier bloc reçu avait été généré il y a %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Les transactions après ceci ne sont pas encore visibles.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location line="-86"/>
        <source>Up to date</source>
        <translation>À jour</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Open a %1: URI or payment request</source>
        <translation>Ouvrir un %1&#xa0;: URI ou demande de paiement</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>Afficher le message d&apos;aide %1 pour obtenir une liste avec les options de ligne de commande Nexa possibles</translation>
    </message>
    <message>
        <location line="+194"/>
        <source>%1 client</source>
        <translation>client %1</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Catching up...</source>
        <translation>Rattrapage en cours…</translation>
    </message>
    <message>
        <location line="+164"/>
        <source>Date: %1
</source>
        <translation>Date : %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Montant : %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Type : %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Étiquette : %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Adresse : %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Transaction envoyée</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Transaction entrante</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation>La génération de clé HD est &lt;b&gt;activée&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation>La génération de clé HD est &lt;b&gt;désactivée&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>Le portefeuille est &lt;b&gt;chiffré&lt;/b&gt; et est actuellement &lt;b&gt;déverrouillé&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>Le portefeuille est &lt;b&gt;chiffré&lt;/b&gt; et actuellement &lt;b&gt;verrouillé&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>Sélection des pièces</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>Quantité :</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>Octets :</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>Montant&#xa0;:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>Priorité :</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>Frais :</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Poussière :</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Après les frais :</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Monnaie :</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>Tout (dé)sélectionner</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>Mode arborescence</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>Mode liste</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>Montant</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>Reçu avec une étiquette</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>Reçu avec une adresse</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>Confirmations</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>Confirmée</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>Priorité</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>Copier l’adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Copier l’étiquette</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>Copier le montant</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>Copier l&apos;ID de la transaction</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>Verrouiller ce qui n&apos;est pas dépensé</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>Déverrouiller ce qui n&apos;est pas dépensé</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>Copier la quantité</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Copier les frais</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Copier le montant après les frais</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Copier les octets</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Copier la priorité</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Copier la poussière</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Copier la monnaie</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>la plus élevée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>plus élevée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>élevée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>moyennement-élevée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>moyenne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>moyennement-basse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>basse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>plus basse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>la plus basse</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 verrouillé)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>aucun</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>Cette étiquette devient rouge si la taille de la transaction est plus grande que 1 000 octets.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>Cette étiquette devient rouge si la priorité est plus basse que « moyenne ».</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>Cette étiquette devient rouge si un destinataire reçoit un montant inférieur à %1.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>Peut varier +/- %1 satoshi(s) par entrée.</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>yes</source>
        <translation>oui</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>non</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>Ceci signifie que des frais d&apos;au moins %1 par ko sont exigés.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>Peut varier +/- 1 octet par entrée.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>Les transactions à priorité plus haute sont plus à même d&apos;être incluses dans un bloc.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(aucune étiquette)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>monnaie de %1 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(monnaie)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>Modifier l&apos;adresse</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>&amp;Étiquette</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>L&apos;étiquette associée à cette entrée de la liste d&apos;adresses</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>L&apos;adresse associée à cette entrée de la liste d&apos;adresses. Ceci ne peut être modifié que pour les adresses d&apos;envoi.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>&amp;Address</source>
        <translation>&amp;Adresse</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>Nouvelle adresse de réception</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>Nouvelle adresse d’envoi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>Modifier l’adresse de réception</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>Modifier l’adresse d&apos;envoi</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>L’adresse fournie «&#xa0;%1&#xa0;» est déjà présente dans le carnet d&apos;adresses.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>L&apos;adresse fournie «&#xa0;%1&#xa0;» n&apos;est pas une adresse Nexa valide.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>Impossible de déverrouiller le portefeuille.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>Échec de génération de la nouvelle clef.</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>Un nouveau répertoire de données sera créé.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>Le répertoire existe déjà. Ajoutez %1 si vous voulez créer un nouveau répertoire ici.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>Le chemin existe déjà et n&apos;est pas un répertoire.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>Impossible de créer un répertoire de données ici.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>version</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1-bit)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation>À propos de %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>Options de ligne de commande</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>Utilisation&#xa0;:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>options de ligne de commande</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>Bienvenue</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation>Bienvenue à %1.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation>Comme c&apos;est la première fois que le programme est lancé, vous pouvez choisir où %1 stockera ses données.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation>%1 va télécharger et stocker une copie de la chaîne de blocs Nexa. Au moins %2 Go de données seront stockées dans ce répertoire, et elles augmenteront avec le temps. Le portefeuille sera également stocké dans ce répertoire.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>Utiliser le répertoire de données par défaut</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>Utiliser un répertoire de données personnalisé :</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>Erreur : le répertoire de données spécifié « %1 » ne peut pas être créé.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n Go d&apos;espace libre disponible</numerusform>
            <numerusform>%n Go d&apos;espace libre disponibles</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(sur %n Go nécessaire)</numerusform>
            <numerusform>(sur %n Go nécessaires)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Les paramètres de mise en forme du trafic en amont ne peuvent pas être vides</translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation>Les informations affichées peuvent être obsolètes. Votre portefeuille se synchronise automatiquement avec le réseau Nexa après l&apos;établissement d&apos;une connexion, mais ce processus n&apos;est pas encore terminé. Cela signifie que les transactions récentes ne seront pas visibles et que le solde ne sera pas à jour tant que ce processus ne sera pas terminé.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation>Dépenser des pièces peut ne pas être possible pendant cette phase&#xa0;!</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation>Quantité de blocs restants</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation>inconnu...</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation>Horodatage du dernier bloc</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation>Progrès</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation>Augmentation de la progression par heure</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation>calculateur...</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation>Temps restant estimé jusqu&apos;à la synchronisation</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation>Inconnu. Réindexation (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation>Inconnu. Réindexation...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation>Inconnu. Synchronisation des en-têtes (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation>Inconnu. Synchronisation des en-têtes...</translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>Ouvrir un URI</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>Ouvrir une demande de paiement à partir d&apos;un URI ou d&apos;un fichier</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI :</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>Choisir le fichier de demande de paiement</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>Choisir le fichier de demande de paiement à ouvrir</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>Réglages &amp;principaux</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Nombre d&apos;exétrons de &amp;vérification de script</translation>
    </message>
    <message>
        <location line="+211"/>
        <source>Allow incoming connections</source>
        <translation>Permettre les transactions entrantes</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>Adresse IP du mandataire (par ex. IPv4 : 127.0.0.1 / IPv6 : ::1)</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>Minimiser au lieu de quitter l&apos;application lorsque la fenêtre est fermée. Si cette option est activée, l&apos;application ne sera fermée qu&apos;en sélectionnant Quitter dans le menu.</translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>URL de tiers (par ex. un explorateur de blocs) apparaissant dans l&apos;onglet des transactions comme éléments du menu contextuel. %s dans l&apos;URL est remplacé par le hachage de la transaction. Les URL multiples sont séparées par une barre verticale |.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>URL de transaction d&apos;un tiers</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Options actives de ligne de commande qui annulent les options ci-dessus :</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>Réinitialiser toutes les options du client aux valeurs par défaut.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Réinitialisation des options</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>&amp;Réseau</translation>
    </message>
    <message>
        <location line="-161"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = auto, &lt; 0 = laisser ce nombre de cœurs inutilisés)</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>W&amp;allet</source>
        <translation>&amp;Portefeuille</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Expert</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation>Activer les fonctions de &amp;contrôle des pièces</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>Si vous désactivé la dépense de la monnaie non confirmée, la monnaie d&apos;une transaction ne peut pas être utilisée tant que cette transaction n&apos;a pas reçu au moins une confirmation. Ceci affecte aussi comment votre solde est calculé.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>&amp;Dépenser la monnaie non confirmée</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lorsque les transactions instantanées sont activées, vous pouvez dépenser immédiatement des transactions non confirmées.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation>&amp;Transactions instantanées</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lors de la création et de l&apos;envoi de transactions, la consolidation automatique créera automatiquement, si nécessaire, une chaîne de transactions dont les entrées ne dépassent pas la limite d&apos;entrée consensuelle.&lt;/p&gt;&lt;/body &gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation>Consolidation automatique</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation>&amp;Réanalyser le portefeuille au démarrage</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Ouvrir le port du client Nexa automatiquement sur le routeur. Ceci ne fonctionne que si votre routeur supporte l&apos;UPnP et si la fonctionnalité est activée.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Mapper le port avec l&apos;&amp;UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation>Acceptez les connexions de l&apos;extérieur.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Se connecter au réseau Nexa par un mandataire SOCKS5.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>Se &amp;connecter par un mandataire SOCKS5 (mandataire par défaut) :</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>&amp;IP du serveur mandataire&#xa0;:</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;Port&#xa0;:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Port du serveur mandataire (par ex. 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Utilisé pour rejoindre les pairs par :</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Se connecter au réseau Nexa au travers d&apos;un mandataire SOCKS5 séparé pour les services cachés de Tor.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Utiliser un mandataire SOCKS5 séparé pour atteindre les pairs grâce aux services cachés de Tor :</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>&amp;Fenêtre</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Afficher uniquement une icône système après minimisation.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>&amp;Minimiser dans la barre système au lieu de la barre des tâches</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation>M&amp;inimiser lors de la fermeture</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>&amp;Langue de l&apos;interface utilisateur&#xa0;:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>La langue de l&apos;interface utilisateur peut être définie ici. Ce paramètre prendra effet après le redémarrage de %1.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>&amp;Unité d&apos;affichage des montants&#xa0;:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Choisissez la sous-unité par défaut pour l&apos;affichage dans l&apos;interface et lors de l&apos;envoi de pièces.</translation>
    </message>
    <message>
        <location line="-502"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Afficher ou non les fonctions de contrôle des pièces.</translation>
    </message>
    <message>
        <location line="-137"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>Démarrer automatiquement %1 après la connexion au système.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>&amp;Démarrer %1 lors de la connexion au système</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lancer automatiquement une réindexation complète de la base de données, une seule fois, au prochain démarrage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation>Réindexer au démarrage</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Initier automatiquement une resynchronisation complète de la blockchain au prochain démarrage (une seule fois).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation>Resynchroniser les données de bloc au démarrage</translation>
    </message>
    <message>
        <location line="+261"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation>Indique si le proxy SOCKS5 par défaut fourni est utilisé pour atteindre des pairs via ce type de réseau.</translation>
    </message>
    <message>
        <location line="+387"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+124"/>
        <source>default</source>
        <translation>par défaut</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>aucune</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Confirm options reset</source>
        <translation>Confirmer la réinitialisation des options</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>Le redémarrage du client est nécessaire pour activer les changements.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Le client sera arrêté. Voulez-vous continuer ?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Ce changement demanderait un redémarrage du client.</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>L&apos;adresse de serveur mandataire fournie est invalide.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>Les informations affichées peuvent être obsolètes. Votre portefeuille est automatiquement synchronisé avec le réseau Nexa lorsque la connexion s&apos;établit, or ce processus n&apos;est pas encore terminé.</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>Watch-only:</source>
        <translation>Juste-regarder :</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>Disponible :</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>Votre solde actuel pouvant être dépensé</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation>En attente :</translation>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Total des transactions qui doivent encore être confirmées et qu&apos;il n&apos;est pas encore possible de dépenser</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>Immature :</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Le solde généré n&apos;est pas encore mûr</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Balances</source>
        <translation>Soldes</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Total:</source>
        <translation>Total :</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>Votre solde total actuel</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>Votre balance actuelle en adresses juste-regarder</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation>Disponible :</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Transactions récentes</translation>
    </message>
    <message>
        <location line="-317"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Transactions non confirmées vers des adresses juste-regarder</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Le solde miné dans des adresses juste-regarder, qui n&apos;est pas encore mûr</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Solde total actuel dans des adresses juste-regarder</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+470"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>Gestion des URIs</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Invalid payment address %1</source>
        <translation>Adresse de paiement invalide %1</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>La demande de paiement est rejetée</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>Le réseau de la demande de paiement ne correspond pas au réseau du client.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Payment request is not initialized.</source>
        <translation>La demande de paiement n&apos;est pas initialisée.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>Le paiement demandé d&apos;un montant de %1 est trop faible (considéré comme de la poussière).</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>Erreur de demande de paiement</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation>Impossible de démarrer le gestionnaire Cliquer pour payer</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>L&apos;URL de récupération de la demande de paiement est invalide : %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>L&apos;URI ne peut pas être analysé&#xa0;! Ceci peut être causé par une adresse Nexa invalide ou par des paramètres d&apos;URI mal formés.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>Gestion des fichiers de demande de paiement</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>Le fichier de demande de paiement ne peut pas être lu ! Ceci peut être causé par un fichier de demande de paiement invalide.</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Payment request expired.</source>
        <translation>Demande de paiement expirée.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>Les demandes de paiements non vérifiées à des scripts de paiement personnalisés ne sont pas prises en charge.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>Demande de paiement invalide.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation>Remboursement de %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>La demande de paiement %1 est trop grande (%2 octets, %3 octets permis).</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>Erreur de communication avec %1 : %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>La demande de paiement ne peut pas être analysée !</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>Mauvaise réponse du serveur %1</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Payment acknowledged</source>
        <translation>Le paiement a été confirmé</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Network request error</source>
        <translation>Erreur de demande réseau</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+106"/>
        <source>User Agent</source>
        <translation>Agent utilisateur</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation>Nœud/service</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Temps de ping</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Montant</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation>Entrez une adresse NEXA (par exemple %1)</translation>
    </message>
    <message>
        <location line="+832"/>
        <source>%1 d</source>
        <translation>%1 j</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 min</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 s</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>N.D.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation>
            <numerusform>%n seconde</numerusform>
            <numerusform>%n secondes</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n heure</numerusform>
            <numerusform>%n heures</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n jour</numerusform>
            <numerusform>%n jours</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n semaine</numerusform>
            <numerusform>%n semaines</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 et %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n an</numerusform>
            <numerusform>%n ans</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Sauvegarder l&apos;image...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>&amp;Copier l&apos;image</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>Sauvegarder le code QR</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>Image PNG (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>N.D.</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>Version du client</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>&amp;Informations</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>Fenêtre de débogage</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location line="-587"/>
        <source>Using BerkeleyDB version</source>
        <translation>Version BerkeleyDB utilisée</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Startup time</source>
        <translation>Heure de démarrage</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>Nombre de connexions</translation>
    </message>
    <message>
        <location line="+415"/>
        <source>Block chain</source>
        <translation>Chaîne de blocs</translation>
    </message>
    <message>
        <location line="-408"/>
        <source>Current number of blocks</source>
        <translation>Nombre actuel de blocs</translation>
    </message>
    <message>
        <location line="+884"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Reçu</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Envoyé</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>&amp;Peers</source>
        <translation>&amp;Pairs</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Pairs bannis</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Choisir un pair pour voir l&apos;information détaillée.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>Dans la liste blanche</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Starting Block</source>
        <translation>Bloc de départ</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>En-têtes synchronisés</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>Blocs synchronisés</translation>
    </message>
    <message>
        <location line="-2488"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>Agent utilisateur</translation>
    </message>
    <message>
        <location line="-2370"/>
        <source>Datadir</source>
        <translation>Datadir</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Last block time (time since)</source>
        <translation>Heure du dernier bloc (temps écoulé depuis)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation>Taille du dernier bloc</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation>Transactions dans le pool Tx</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation>Transactions dans le pool orphelin</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation>Messages dans le pool CAPD</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation>Pool Tx - utilisation</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation>Pool de Tx - txns par seconde</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation>XThin (Totaux)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation>XThin (moyennes sur 24&#xa0;heures)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation>Compact (Totaux)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation>Compact (moyennes sur 24 heures)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation>Graphene (Totaux)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation>Graphene (moyennes sur 24 heures)</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>Ouvrez le fichier journal de débogage %1 à partir du répertoire de données actuel. Cela peut prendre quelques secondes pour les fichiers journaux volumineux.</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation>Bloquer la propagation</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation>Groupements de transactions</translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Decrease font size</source>
        <translation>Diminuer la taille de la police</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>Augmenter la taille de la police</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>&amp;Transaction Rate</source>
        <translation>&amp;Taux de transactions</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation>Taux instantané (1s)</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation>Culminer</translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation>24 heures</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation>Affiché</translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation>Taux lissé (60s)</translation>
    </message>
    <message>
        <location line="+769"/>
        <source>Services</source>
        <translation>Services</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Ban Score</source>
        <translation>Pointage des bannissements</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Temps de connexion</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Dernier envoi</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Dernière réception</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Temps de ping</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>La durée d&apos;un ping actuellement en cours.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Attente du ping</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Décalage temporel</translation>
    </message>
    <message>
        <location line="-2524"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>&amp;Console</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Network Traffic</source>
        <translation>Trafic &amp;réseau</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>&amp;Nettoyer</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Totaux</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>Entrant :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>Sortant :</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>Journal de débogage</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>Nettoyer la console</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation>&amp;Déconnecter le nœud</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>Bannir le nœud pendant</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 &amp;heure</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 &amp;jour</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 &amp;semaine</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 &amp;an</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>&amp;Réhabiliter le nœud</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>Bienvenue dans la console RPC %1.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>Utiliser les touches de curseur pour naviguer dans l&apos;historique et &lt;b&gt;Ctrl-L&lt;/b&gt; pour effacer l&apos;écran.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>Taper &lt;b&gt;help&lt;/b&gt; pour afficher une vue générale des commandes proposées.</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 o</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 Ko</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 Mo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 Go</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(ID de nœud : %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>par %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>jamais</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>Entrant</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>Sortant</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+110"/>
        <source>&amp;Amount:</source>
        <translation>&amp;Montant :</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Label:</source>
        <translation>&amp;Étiquette&#xa0;:</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>&amp;Message:</source>
        <translation>M&amp;essage :</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Réutilise une adresse de réception précédemment utilisée. Réutiliser une adresse pose des problèmes de sécurité et de vie privée. N&apos;utilisez pas cette option sauf si vous générez à nouveau une demande de paiement déjà faite.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>Ré&amp;utiliser une adresse de réception existante (non recommandé)</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+23"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>Un message optionnel à joindre à la demande de paiement qui sera affiché à l&apos;ouverture de celle-ci. Note : le message ne sera pas envoyé avec le paiement par le réseau Nexa.</translation>
    </message>
    <message>
        <location line="-63"/>
        <location line="+56"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>Un étiquette optionnelle à associer à la nouvelle adresse de réception</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Utiliser ce formulaire pour demander des paiements. Tous les champs sont  &lt;b&gt;optionnels&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="-56"/>
        <location line="+7"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>Un montant optionnel à demander. Laisser ceci vide ou à zéro pour ne pas demander de montant spécifique.</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Clear all fields of the form.</source>
        <translation>Effacer tous les champs du formulaire.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Requested payments history</source>
        <translation>Historique des paiements demandés</translation>
    </message>
    <message>
        <location line="-197"/>
        <source>&amp;Request payment</source>
        <translation>&amp;Demande de paiement</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Coin freezing locks coins to make them temporarily unspendable. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le gel des pièces verrouille les pièces pour les rendre temporairement inutilisables. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Coin &amp;Freeze</source>
        <translation>Monnaie &amp; Gel</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Afficher la demande choisie (identique à un double-clic sur une entrée)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Afficher</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Enlever les entrées sélectionnées de la liste</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+53"/>
        <source>Copy URI</source>
        <translation>Copier l&apos;URI</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Copier l’étiquette</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Copier le message</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copier le montant</translation>
    </message>
</context>
<context>
    <name>ReceiveFreezeDialog</name>
    <message>
        <location filename="../forms/receivefreezedialog.ui" line="+14"/>
        <source>Coin Freeze</source>
        <translation></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>WARNING! Freezing coins means they will be UNSPENDABLE until the release date or block specified below.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Freeze until block :</source>
        <translation></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify the block when coins are released from the freeze. Coins are UNSPENDABLE until the freeze block.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+99"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="-57"/>
        <source>OR</source>
        <translation></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Freeze until date and time :</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Specify the future date and time when coins are released from the freeze. Coins are UNSPENDABLE until after the freeze date and time.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>&amp;Reset</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+R</source>
        <translation></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>Code QR</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>Copier l&apos;&amp;URI</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>Copier l&apos;&amp;adresse</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Sauvegarder l&apos;image...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>Demande de paiement à %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Informations de paiement</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>Montant</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Freeze until</source>
        <translation>Congeler jusqu&apos;à</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>L&apos;URI résultant est trop long, essayez de réduire le texte d&apos;étiquette / de message.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Erreur d&apos;encodage de l&apos;URI en code QR.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Amount</source>
        <translation>Montant</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(pas d&apos;étiquette)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>(pas de message)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(aucun montant)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+633"/>
        <source>Send Coins</source>
        <translation>Envoyer des pièces</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Fonctions de contrôle des pièces</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation>Sélectionnez les pièces spécifiques que vous souhaitez utiliser dans cette transaction</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>Entrants...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>choisi automatiquement</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Fonds insuffisants !</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Quantité :</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Octets :</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Montant :</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>Priorité :</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Frais :</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>After Fee:</source>
        <translation>Après les frais :</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Monnaie :</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>Si ceci est actif mais l&apos;adresse de monnaie rendue est vide ou invalide, la monnaie sera envoyée vers une adresse nouvellement générée.</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>Adresse personnalisée de monnaie rendue</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>Frais de transaction :</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation>Choisissez vos frais de transaction.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation>Choisir...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>réduire les paramètres des frais</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>per kilobyte</source>
        <translation>par kilo-octet</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>Si les frais personnalisés sont définis à 1 000 satoshis et que la transaction est seulement de 250 octets, donc le « par kilo-octet » ne paiera que 250 satoshis de frais, alors que le « total au moins » paiera 1 000 satoshis. Pour des transactions supérieures à un kilo-octet, les deux paieront par kilo-octets.</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation>total au moins</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation>Montant des frais de douane</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>Il est correct de payer les frais minimum tant que le volume transactionnel est inférieur à l&apos;espace dans les blocs. Mais soyez conscient que ceci pourrait résulter en une transaction n&apos;étant jamais confirmée une fois qu&apos;il y aura plus de transactions que le réseau ne pourra en traiter.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(lire l&apos;infobulle)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation>Utilisez le montant des frais recommandé. Vous pouvez sélectionner un délai de confirmation plus rapide ou plus lent en déplaçant le curseur «&#xa0;Durée de confirmation&#xa0;».</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation>Recommandés :</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation>Choisissez un montant de frais personnalisés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation>Personnalisés : </translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(Les frais intelligents ne sont pas encore initialisés. Ceci prend habituellement quelques blocs...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation></translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation>Temps de confirmation :</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>rapide</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>Envoyer si possible une transaction sans frais</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(la confirmation pourrait prendre plus longtemps)</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>Envoyer à plusieurs destinataires à la fois</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Ajouter un &amp;destinataire</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>Effacer tous les champs du formulaire.</translation>
    </message>
    <message>
        <location line="-880"/>
        <source>Dust:</source>
        <translation>Poussière :</translation>
    </message>
    <message>
        <location line="+883"/>
        <source>Clear &amp;All</source>
        <translation>&amp;Tout nettoyer</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Current Balance</source>
        <translation>Solde actuel</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>Solde&#xa0;:</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>Confirm the send action</source>
        <translation>Confirmer l’action d&apos;envoi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>E&amp;nvoyer</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>Confirmer l’envoi des pièces</translation>
    </message>
    <message>
        <location line="-59"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 à %2</translation>
    </message>
    <message>
        <location line="-289"/>
        <source>Copy quantity</source>
        <translation>Copier la quantité</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copier le montant</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>Copier les frais</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Copier le montant après les frais</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Copier les octets</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Copier la priorité</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy change</source>
        <translation>Copier la monnaie</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Libellé public&#xa0;:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING!!! DESTINATION IS A FREEZE ADDRESS&lt;br&gt;UNSPENDABLE UNTIL&lt;/b&gt; %1 &lt;br&gt;*************************************************&lt;br&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;b&gt;ATTENTION&#xa0;!!! LA DESTINATION EST UNE ADRESSE DE GEL &lt;br&gt; IMPOSSIBLE JUSQU&apos;À&lt;/b&gt; %1 &lt;br&gt;******************************** ******************&lt;br&gt;</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Total Amount %1</source>
        <translation>Montant total %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>ou</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>Le montant à payer doit être supérieur à 0.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>Le montant dépasse votre solde.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>Le montant dépasse votre solde lorsque les frais de transaction de %1 sont inclus.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction creation failed!</source>
        <translation>La création de la transaction a échoué !</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>La transaction a été rejetée ! Ceci peut arriver si certaines pièces de votre portefeuille étaient déjà dépensées, par exemple si vous avez utilisé une copie de wallet.dat et que des pièces ont été dépensées dans la copie sans être marquées comme telles ici.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>Des frais supérieurs à %1 sont considérés comme ridiculement élevés.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>Demande de paiement expirée.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation>L&apos;étiquette publique dépasse la limite de</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation>Payer seulement les frais exigés de %1</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>L&apos;adresse du destinataire est invalide. Veuillez la vérifier.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>Adresse identique trouvée : chaque adresse ne devrait être utilisée qu&apos;une fois.</translation>
    </message>
    <message>
        <location line="+252"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Avertissement : adresse Nexa invalide</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Confirm custom change address</source>
        <translation>Confirmer le changement d&apos;adresse personnalisé</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation>L&apos;adresse que vous avez sélectionnée pour le changement ne fait pas partie de ce portefeuille. Tout ou partie des fonds de votre portefeuille peuvent être envoyés à cette adresse. Es-tu sûr?</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(pas d&apos;étiquette)</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Warning: Unknown change address</source>
        <translation>Avertissement : adresse de monnaie rendue inconnue</translation>
    </message>
    <message>
        <location line="-784"/>
        <source>Copy dust</source>
        <translation>Copier la poussière</translation>
    </message>
    <message>
        <location line="+298"/>
        <source>Are you sure you want to send?</source>
        <translation>Êtes-vous sûr de vouloir envoyer ?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>ajouté en tant que frais de transaction</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+86"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>&amp;Montant :</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>&amp;Payer à :</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>Choisir une adresse déjà utilisée</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Private Description:</source>
        <translation>Descriptif privé&#xa0;:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation>&amp;Label privé :</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>L&apos;adresse Nexa à laquelle envoyer le paiement</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Coller l&apos;adresse depuis le presse-papiers</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>Enlever cette entrée</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Un message qui était attaché à la pièce : URI qui sera stocké avec la transaction pour votre référence. Remarque : Ce message ne sera pas envoyé sur le réseau Nexa.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation>Saisissez une étiquette privée pour cette adresse afin de l&apos;ajouter à la liste des adresses utilisées</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation>Saisissez un libellé public pour cette transaction</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation>Label public&#xa0;:</translation>
    </message>
    <message>
        <location line="-180"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>Les frais seront déduits du montant envoyé. Le destinataire recevra moins de coins que le montant saisi dans le champ de montant. Si plusieurs destinataires sont sélectionnés, les frais seront partagés également..</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Amount to send</source>
        <translation>Montant à envoyer</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>S&amp;oustraire les frais du montant</translation>
    </message>
    <message>
        <location line="+643"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Cette demande de paiement n&apos;est pas authentifiée.</translation>
    </message>
    <message>
        <location line="+556"/>
        <source>This is an authenticated payment request.</source>
        <translation>Cette demande de paiement est authentifiée.</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Un message qui était joint à l&apos;URI %1 et qui sera stocké avec la transaction pour référence. Note : ce message ne sera pas envoyé par le réseau Nexa.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation>Saisissez une étiquette privée pour cette adresse afin de l&apos;ajouter à votre carnet d&apos;adresses</translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-541"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>Payer à :</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>Mémo :</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>%1 est en train de s&apos;arrêter...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>Ne pas fermer l&apos;ordinateur jusqu&apos;à la disparition de cette fenêtre.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Signatures - Signer / Vérifier un message</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>&amp;Signer un message</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Vous pouvez signer des messages/accords avec vos adresses pour prouver que vous pouvez recevoir des coins à ces dernières. Faites attention de ne rien signer de vague ou au hasard, car des attaques d&apos;hameçonnage pourraient essayer de vous faire signer avec votre identité afin de l&apos;usurper. Ne signez que des déclarations entièrement détaillées et avec lesquelles vous êtes d&apos;accord.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>L&apos;adresse Nexa avec laquelle signer le message</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Choisir une adresse précédemment utilisée</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Coller une adresse depuis le presse-papiers</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Saisir ici le message que vous désirez signer</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Signature</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Copier la signature actuelle dans le presse-papiers</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Signer le message pour prouver que vous détenez cette adresse Nexa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>Signer le &amp;message</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Réinitialiser tous les champs de signature de message</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>&amp;Tout nettoyer</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>&amp;Vérifier un message</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>Saisissez ci-dessous l&apos;adresse de destinataire, le message (assurez-vous de copier exactement les retours à la ligne, les espaces, les tabulations, etc.) et la signature pour vérifier le message. Faites attention à ne pas déduire davantage de la signature que ce qui est contenu dans le message signé même, pour éviter d&apos;être trompé par une attaque d&apos;homme du milieu. Notez que ceci ne fait que prouver que le signataire reçoit l&apos;adresse et ne peut pas prouver la provenance d&apos;une transaction.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>L&apos;adresse Nexa avec laquelle le message a été signé</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Vérifier le message pour vous assurer qu&apos;il a bien été signé par l&apos;adresse Nexa spécifiée</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>Vérifier le &amp;message</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Réinitialiser tous les champs de vérification de message</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>Cliquez sur «&#xa0;Signer le message&#xa0;» pour générer la signature</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>Le déverrouillage du portefeuille a été annulé.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Le message a été signé.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Message vérifié.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation>Détails du jeton</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ce volet affiche une description détaillée du jeton&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Toutes</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Aujourd’hui</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Cette semaine</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Ce mois-ci</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Le mois dernier</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Cette année</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Intervalle…</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Reçue avec</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation>Envoyé</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>À vous-même</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation>Frapper</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation>Entrez l&apos;ID du jeton pour rechercher</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Montant min.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation>Copier l&apos;identifiant du jeton</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copier le montant</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>Copier la transaction idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>Copier la transaction brute</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Afficher les détails de la transaction</translation>
    </message>
    <message>
        <location line="+162"/>
        <source>Export Token History</source>
        <translation>Exporter l’historique des jetons</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Valeurs séparées par des virgules (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Confirmée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>Juste-regarder :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation>identifiant de transaction</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation>ID de jeton</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation>Montant du jeton</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>L&apos;exportation a échoué</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation>Une erreur s&apos;est produite lors de la tentative d&apos;enregistrement de l&apos;historique du jeton dans %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Exportation réussie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation>L&apos;historique du jeton a été enregistré avec succès dans %1.</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Range:</source>
        <translation>Intervalle&#xa0;:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>à</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+244"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation>ID de jeton</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation>Montant net</translation>
    </message>
    <message numerus="yes">
        <location line="+49"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Ouvert pour %n bloc de plus</numerusform>
            <numerusform>Ouvert pour %n blocs de plus</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>Ouvert jusqu&apos;à %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Hors ligne</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Non confirmé</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Confirmation (%1 sur %2 confirmations recommandées)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Confirmée (%1 confirmations)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>En conflit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Double dépensé</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Transaction abandonnée</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Immature (%1 confirmations, sera disponible après %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Ce bloc n’a été reçu par aucun autre nœud et ne sera probablement pas accepté&#xa0;!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Généré mais pas accepté</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Reçue avec</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Reçue de</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation>Envoyé</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Paiement à vous-même</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation>Frapper</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation>Fondre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Label public</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location line="+323"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>État de la transaction. Laissez le pointeur de la souris sur ce champ pour voir le nombre de confirmations.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Date et heure de réception de la transaction.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Type de transaction.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Une adresse juste-regarder est-elle impliquée dans cette transaction.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Intention/but de la transaction défini par l&apos;utilisateur.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Montant ajouté ou enlevé au solde.</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation>Jetons</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Amount: </source>
        <translation>Montant: </translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Le nombre de jetons à envoyer&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation>  Payer à: </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation>L&apos;adresse Nexa à laquelle envoyer le paiement</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>Choisir l&apos;adresse précédemment utilisée</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Coller l&apos;adresse depuis le presse-papiers</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation>Enlever cette entrée</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Envoyer des jetons&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation>Envoyer</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation>ID de jeton</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation>L&apos;identifiant unique du jeton</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation>L&apos;identifiant du groupe de jetons</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation>Le nom du jeton</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>Téléscripteur</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation>Le symbole boursier du jeton</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation>Solde</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation>Solde de jetons confirmé</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation>En attente</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation>Solde de jetons non confirmé</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation>Sous</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation>Si cette case est cochée, cet élément est un sous-groupe.</translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+367"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation>chaîne</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation>nombre</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation>NaN</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation>Tirage total:</translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation>La frappe des jetons n&apos;est pas disponible car la base de données a besoin d&apos;une réindexation</translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation>Téléscripteur:</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation>Hachage:</translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation>Décimales:</translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation>Autorités actuelles&#xa0;:</translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation>Les autorités de jeton ne sont pas disponibles car la base de données a besoin d&apos;une réindexation</translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation>Solde:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation>En attente:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation>Êtes-vous sûr de vouloir envoyer ?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation>&lt;b&gt;Jeton(s)&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation>Confirmer l&apos;envoi des jetons</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>Ko/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+36"/>
        <source>Open until %1</source>
        <translation>Ouvert jusqu&apos;à %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>conflicted</source>
        <translation>en conflit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/hors ligne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/non confirmée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 confirmations</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>État</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, diffusée à travers %n nœud</numerusform>
            <numerusform>, diffusée à travers %n nœuds</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Généré</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+128"/>
        <source>From</source>
        <translation>De</translation>
    </message>
    <message>
        <location line="-148"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+114"/>
        <source>To</source>
        <translation>À</translation>
    </message>
    <message>
        <location line="-215"/>
        <source>double spent</source>
        <translation>double dépensé</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation>transaction abandonnée</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>change address</source>
        <translation>changement d&apos;adresse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>votre propre adresse</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+119"/>
        <source>watch-only</source>
        <translation>juste-regarder</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+28"/>
        <location line="+113"/>
        <source>label</source>
        <translation>étiquette</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+63"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation>Étiquette publique&#xa0;:</translation>
    </message>
    <message>
        <location line="-167"/>
        <source>Freeze until</source>
        <translation>Congeler jusqu&apos;à</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+213"/>
        <source>Credit</source>
        <translation>Crédit</translation>
    </message>
    <message numerus="yes">
        <location line="-361"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>arrive à maturité dans %n bloc de plus</numerusform>
            <numerusform>arrive à maturité dans %n blocs de plus</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>refusé</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+218"/>
        <source>Debit</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location line="-237"/>
        <source>Total debit</source>
        <translation>Débit total</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Crédit total</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Frais de transaction</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Montant net</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+12"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction Idem</source>
        <translation>Idem transaction</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation>Taille des transactions</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Marchand</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Les pièces générées doivent mûrir pendant %1 blocs avant de pouvoir être dépensées. Lorsque vous avez généré ce bloc, il a été diffusé sur le réseau pour être ajouté à la chaîne de blocs. S’il échoue a intégrer la chaîne, son état sera modifié en « non accepté » et il ne sera pas possible de le dépenser. Ceci peut arriver occasionnellement si un autre nœud génère un bloc à quelques secondes du votre.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Token ID</source>
        <translation>ID de jeton</translation>
    </message>
    <message>
        <location line="-50"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Ticker</source>
        <translation>Téléscripteur</translation>
    </message>
    <message>
        <location line="-54"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Decimals</source>
        <translation>Décimales</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>Melt</source>
        <translation>Fondre</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+34"/>
        <source>Mint</source>
        <translation>Frapper</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Amount Sent</source>
        <translation>Montant envoyé</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Amount Received</source>
        <translation>Montant reçu</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Debug information</source>
        <translation>Informations de débogage</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Transaction</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>Entrants</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+28"/>
        <location line="+34"/>
        <location line="+55"/>
        <source>Amount</source>
        <translation>Montant</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>vrai</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>faux</translation>
    </message>
    <message>
        <location line="-496"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, n’a pas encore été diffusée avec succès</translation>
    </message>
    <message numerus="yes">
        <location line="-40"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Ouvert pour %n bloc de plus</numerusform>
            <numerusform>Ouvert pour %n blocs de plus</numerusform>
        </translation>
    </message>
    <message>
        <location line="+73"/>
        <source>unknown</source>
        <translation>inconnu</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>Détails de la transaction</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Ce panneau affiche une description détaillée de la transaction</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation>tps</translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Immature (%1 confirmations, sera disponible après %2)</translation>
    </message>
    <message numerus="yes">
        <location line="-29"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Ouvert pour %n bloc de plus</numerusform>
            <numerusform>Ouvert pour %n blocs de plus</numerusform>
        </translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Address or Label</source>
        <translation>Adresse ou étiquette</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Open until %1</source>
        <translation>Ouvert jusqu&apos;à %1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Confirmée (%1 confirmations)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Ce bloc n’a été reçu par aucun autre nœud et ne sera probablement pas accepté&#xa0;!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Généré mais pas accepté</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Offline</source>
        <translation>Hors ligne</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Non confirmé</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Confirmation (%1 sur %2 confirmations recommandées)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Conflicted</source>
        <translation>En conflit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Double dépensé</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Transaction abandonnée</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Reçue avec</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Reçue de</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Envoyée à</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Paiement à vous-même</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Miné</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Label public</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>juste-regarder</translation>
    </message>
    <message>
        <location line="+276"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>État de la transaction. Laissez le pointeur de la souris sur ce champ pour voir le nombre de confirmations.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Date et heure de réception de la transaction.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Type de transaction.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Une adresse juste-regarder est-elle impliquée dans cette transaction.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Intention/but de la transaction défini par l&apos;utilisateur.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Montant ajouté ou enlevé au solde.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Toutes</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Aujourd’hui</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Cette semaine</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Ce mois-ci</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Le mois dernier</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Cette année</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Intervalle…</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Reçue avec</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Envoyée à</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>À vous-même</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Miné</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Saisir une adresse ou une étiquette à rechercher</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Montant min.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Copier l’adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Copier l’étiquette</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Copier le montant</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy raw transaction</source>
        <translation>Copier la transaction brute</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>Modifier l’étiquette</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Afficher les détails de la transaction</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Export Transaction History</source>
        <translation>Exporter l&apos;historique des transactions</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Watch-only</source>
        <translation>Juste-regarder :</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Exporting Failed</source>
        <translation>L&apos;exportation a échoué</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>Une erreur est survenue lors de l&apos;enregistrement de l&apos;historique des transactions vers %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Exportation réussie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>L&apos;historique des transactions a été sauvegardée avec succès vers %1.</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Valeurs séparées par des virgules (*.csv)</translation>
    </message>
    <message>
        <location line="-175"/>
        <source>Copy transaction idem</source>
        <translation>Copier la transaction idem</translation>
    </message>
    <message>
        <location line="+184"/>
        <source>Confirmed</source>
        <translation>Confirmée</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Range:</source>
        <translation>Intervalle&#xa0;:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>à</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Unité d&apos;affichage des montants. Cliquer pour choisir une autre unité.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation>Unlimited</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation>&amp;Exploitation minière</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation>Le plus gros bloc qui sera miné</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation>Taille de bloc maximale générée (octets)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation>&amp;Réseau</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation>Restrictions de bande passante en Ko/s (cochez pour activer)&#xa0;:</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation>Envoyer</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation>Recevoir</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation>Options actives de ligne de commande qui annulent les options ci-dessus :</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation>Réinitialiser toutes les options du client aux valeurs par défaut.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Réinitialisation des options</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation>Confirmer la réinitialisation des options</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation>Il s&apos;agit d&apos;une réinitialisation globale de tous les paramètres&#xa0;!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation>Le redémarrage du client est nécessaire pour activer les changements.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Le client sera arrêté. Voulez-vous continuer ?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Les paramètres de mise en forme du trafic en amont ne peuvent pas être vides</translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation>Les paramètres de mise en forme du trafic doivent être supérieurs à 0.</translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>Aucun portefeuille de chargé.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation>État de la base de données de jetons</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation>Portefeuille de jetons non disponible car un -reindex est nécessaire. Cliquez sur &quot;Ok&quot; pour effectuer une réindexation unique au prochain démarrage. Arrêtez ensuite Nexa-Qt et redémarrez pour terminer le processus.</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+291"/>
        <source>Send Coins</source>
        <translation>Envoyer des pièces</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>&amp;Exporter</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Exporter les données de l&apos;onglet courant vers un fichier</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>Sauvegarder le portefeuille</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Données de portefeuille (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>Échec de la sauvegarde</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>Une erreur est survenue lors de l&apos;enregistrement des données de portefeuille vers %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>Les données de portefeuille ont été enregistrées avec succès vers %1</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Backup Successful</source>
        <translation>Sauvegarde réussie</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation>Restaurer le portefeuille</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation>Échec de la restauration</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation>Une erreur s&apos;est produite lors de la tentative de restauration des données du portefeuille sur %1.</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+56"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>L&apos;élagage est configuré au-dessous du minimum de %d Mio. Veuillez utiliser un nombre plus élevé.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>Élagage : la dernière synchronisation de portefeuille va par-delà les données élaguées.  Vous devez -reindex (réindexer, télécharger de nouveau toute la chaîne de blocs en cas de nœud élagué)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Rescans are not possible in pruned mode. You will need to use -reindex which will download the whole blockchain again.</source>
        <translation>Les rebalayages sont impossibles en mode élagage. Vous devrez utiliser -reindex, ce qui téléchargera de nouveau la chaîne de blocs en entier.</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>Erreur : une erreur interne fatale s&apos;est produite. Voir debug.log pour plus de détails</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Pruning blockstore...</source>
        <translation>Élagage du magasin de blocs...</translation>
    </message>
    <message>
        <location line="-165"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Distribué sous la licence MIT d&apos;utilisation d&apos;un logiciel. Consultez le fichier joint COPYING ou &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>La base de données de blocs contient un bloc qui semble provenir du futur. Cela pourrait être causé par la date et l&apos;heure erronées de votre ordinateur. Ne reconstruisez la base de données de blocs que si vous êtes certain que la date et l&apos;heure de votre ordinateur sont justes.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>Ceci est une pré-version de test - l&apos;utiliser à vos risques et périls - ne pas l&apos;utiliser pour miner ou pour des applications marchandes</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>AVERTISSEMENT : un nombre anormalement élevé de blocs a été généré, %d blocs reçus durant les %d dernières heures (%d attendus)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>AVERTISSEMENT : vérifiez votre connexion réseau, %d blocs reçus durant les %d dernières heures (%d attendus)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>Avertissement : le réseau ne semble pas totalement d&apos;accord ! Quelques mineurs semblent éprouver des difficultés.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>Avertissement : nous ne semblons pas être en accord complet avec nos pairs ! Vous pourriez avoir besoin d&apos;effectuer une mise à niveau, ou d&apos;autres nœuds du réseau pourraient avoir besoin d&apos;effectuer une mise à niveau.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation>Vous essayez de restaurer le même portefeuille que celui que vous essayez de remplacer.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation>Vous ne pouvez pas exécuter &quot;-salvagewallet&quot; comme portefeuille HD.

Veuillez relancer Nexa avec &quot;-usehd=0&quot;.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Corrupted block database detected</source>
        <translation>Base corrompue de données des blocs détectée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>Voulez-vous reconstruire la base de données des blocs maintenant ?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>Erreur lors de l&apos;initialisation de la base de données des blocs</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Erreur lors de l&apos;initialisation de l&apos;environnement de la base de données du portefeuille %s !</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error opening block database</source>
        <translation>Erreur lors de l&apos;ouverture de la base de données des blocs</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>Erreur : l&apos;espace disque est faible !</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Échec de l&apos;écoute sur un port quelconque. Utilisez -listen=0 si vous voulez ceci.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>Importation...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>Bloc de genèse incorrect ou introuvable. Mauvais répertoire de données pour le réseau ?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Adresse -onion invalide : « %s »</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Not enough file descriptors available.</source>
        <translation>Pas assez de descripteurs de fichiers proposés.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>L&apos;élagage ne peut pas être configuré avec une valeur négative.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>Le mode élagage n&apos;est pas compatible avec -txindex.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Signing token transaction failed</source>
        <translation>Échec de la transaction du jeton de signature</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>Le commentaire d&apos;agent utilisateur (%s) contient des caractères dangereux.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>Vérification des blocs en cours...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>Vérification du portefeuille en cours...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation>En attendant Genesis Block...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>Le portefeuille %s réside en dehors du répertoire de données %s</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Erreur : l&apos;écoute des connexions entrantes a échoué (l&apos;écoute a retourné l&apos;erreur %s)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>Le montant de la transaction est trop bas pour être envoyé une fois que les frais ont été déduits</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>Ce produit comprend des logiciels développés par le projet OpenSSL pour être utilisés dans la boîte à outils OpenSSL &lt;https://www.openssl.org/&gt; et un logiciel cryptographique écrit par Eric Young, ainsi qu&apos;un logiciel UPnP écrit par Thomas Bernard.</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain</source>
        <translation>Vous devez reconstruire la base de données en utilisant -reindex afin de revenir au mode sans élagage. Ceci retéléchargera complètement la chaîne de blocs.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Activating best chain...</source>
        <translation>Activation de la meilleure chaîne...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>Impossible de résoudre l&apos;adresse -whitebind : «&#xa0;%s&#xa0;»</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Information</source>
        <translation>Informations</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>Masque réseau invalide spécifié dans -whitelist : « %s »</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>Un port doit être spécifié avec -whitebind : « %s »</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Signing transaction failed</source>
        <translation>La signature de la transaction a échoué</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation>Démarrage de txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>Le montant de la transaction est trop bas pour que les frais soient payés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Ceci est un logiciel expérimental.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Montant de la transaction trop bas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>Les montants de transaction doivent être positifs</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation>La transaction a %d sorties. Le nombre maximum de sorties autorisées est %d</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation>La transaction de %d octets est trop volumineuse. Le maximum autorisé est de %d octets</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>La transaction est trop grosse pour la politique de frais</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>Impossible de se lier à %s sur cet ordinateur (bind a retourné l&apos;erreur %s)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location line="-42"/>
        <source>Loading addresses...</source>
        <translation>Chargement des adresses…</translation>
    </message>
    <message>
        <location line="-79"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation>Wallet n&apos;est pas protégé par mot de passe. Vos fonds sont peut-être à risque! Allez dans « Règlages », puis sélectionnez « Chiffrere le portefeuille » pour créer un mot de passe.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Adresse -proxy invalide&#xa0;: «&#xa0;%s&#xa0;»</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>Réseau inconnu spécifié sur -onlynet&#xa0;: «&#xa0;%s&#xa0;»</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>Impossible de résoudre l&apos;adresse -bind&#xa0;: «&#xa0;%s&#xa0;»</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>Impossible de résoudre l&apos;adresse -externalip&#xa0;: «&#xa0;%s&#xa0;»</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Loading block index...</source>
        <translation>Chargement de l’index des blocs…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>Chargement du portefeuille…</translation>
    </message>
    <message>
        <location line="-38"/>
        <source>Cannot downgrade wallet</source>
        <translation>Impossible de revenir à une version inférieure du portefeuille</translation>
    </message>
    <message>
        <location line="-131"/>
        <source>Nexa</source>
        <translatorcomment>Nexa</translatorcomment>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>Les développeurs %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation>Bitcoin Bitcoin XT et Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation>-wallet.maxTxFee est fixé très haut&#xa0;! Des frais aussi importants pourraient être payés sur une seule transaction.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation>-wallet.payTxFee est fixé très haut&#xa0;! Il s&apos;agit des frais de transaction que vous paierez si vous envoyez une transaction.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>Impossible d&apos;obtenir un verrou sur le répertoire de données %s. %s est probablement déjà en cours d&apos;exécution.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation>Impossible de localiser les informations d&apos;identification RPC. Aucun cookie d&apos;authentification n&apos;a été trouvé et aucun mot de passe rpc n&apos;est défini dans le fichier de configuration (%s)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation>Le fichier de configuration de déploiement &apos;%s&apos; contenait des données non valides - voir debug.log</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>Erreur lors du chargement de %s&#xa0;: vous ne pouvez pas activer la HD sur un portefeuille non HD déjà existant</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>Erreur lors de la lecture de %s&#xa0;! Toutes les clés sont lues correctement, mais les données de transaction ou les entrées du carnet d&apos;adresses peuvent être manquantes ou incorrectes.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation>Erreur lors de la lecture de la base de données des pièces.
Détails&#xa0;: %s

Voulez-vous réindexer au prochain redémarrage&#xa0;?</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation>Échec de l&apos;écoute sur tous les ports P2P. Échec comme demandé par -bindallorfail.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation>Frais : %ld est supérieur au frais maximal autorisé configuré de : %ld. Pour changer, définissez &apos;wallet.maxTxFee&apos;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation>Montant non valide pour -wallet.maxTxFee=&lt;montant&gt;&#xa0;: &apos;%u&apos; (doit être au moins égal aux frais de relais min de %s pour éviter les transactions bloquées)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation>Montant invalide pour -wallet.payTxFee=&lt;montant&gt;&#xa0;: &apos;%u&apos; (doit être au moins %s)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>Veuillez vérifier que la date et l&apos;heure de votre ordinateur sont correctes&#xa0;! Si votre horloge est erronée, %s ne fonctionnera pas correctement.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation>Réduction de -maxconnections de %d à %d en raison des limitations du descripteur de fichier (unix) ou des limitations winsocket fd_set (windows). Si vous êtes un utilisateur Windows, il existe une limite supérieure stricte de 1024 qui ne peut pas être modifiée en ajustant la configuration du nœud.</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation>La longueur totale de la chaîne de version du réseau avec les uacomments ajoutés dépassait la longueur maximale (%i) et a été tronquée. Réduisez le nombre ou la taille des uacomments pour éviter la troncature.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation>La transaction a %d entrées et %d sorties. Les entrées maximales autorisées sont %d et les sorties maximales sont %d</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation>La transaction a %d&#xa0;entrées. Le nombre maximal d&apos;entrées autorisées est %d. Essayez de réduire les intrants en transférant une plus petite quantité.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation>Avertissement&#xa0;: Impossible d&apos;ouvrir le fichier&#xa0;CSV de configuration de déploiement &apos;%s&apos; pour le lire</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>Avertissement&#xa0;: Versions de bloc inconnues en cours d&apos;extraction&#xa0;! Il est possible que des règles inconnues soient en vigueur</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>Avertissement&#xa0;: fichier de portefeuille corrompu, données récupérées&#xa0;! %s d&apos;origine enregistré sous %s dans %s&#xa0;; si votre solde ou vos transactions sont incorrects, vous devez restaurer à partir d&apos;une sauvegarde.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation>Vous essayez d&apos;utiliser -wallet.auto mais ni -spendzeroconfchange ni -wallet.instant ne sont activés</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation>Vous ne pouvez pas envoyer de transactions gratuites si vous avez configuré un -relay.limitFreeRelay de zéro</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation>La «&#xa0;Restaurer le portefeuille&#xa0;» a réussi et une sauvegarde du portefeuille précédent a été enregistrée dans&#xa0;:&#xa0;%s.


Lorsque vous cliquez sur « OK », Nexa s&apos;arrêtera pour terminer le processus.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation>%s </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s corrompu, la récupération a échoué</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation>-maxtxpool doit être d&apos;au moins %d Mo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation>-xthinbloomfiltersize doit être d&apos;au moins %d octets</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot write default address</source>
        <translation>Impossible d&apos;écrire l&apos;adresse par défaut</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation>Commettre de la transaction a échoué.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>Copyright (C) 2015-%i Les Bitcoin Unlimited développeurs</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation>Fichier de configuration de déploiement &apos;%s&apos; introuvable</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s</source>
        <translation>Erreur lors du chargement de %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>Erreur lors du chargement de %s&#xa0;: portefeuille corrompu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>Erreur lors du chargement de %s&#xa0;: le portefeuille nécessite une version plus récente de %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>Erreur lors du chargement de %s&#xa0;: vous ne pouvez pas désactiver la HD sur un portefeuille HD déjà existant</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation>Erreur&#xa0;: Keypool est épuisé, veuillez d&apos;abord appeler keypoolrefill</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>La vérification d&apos;intégrité de l&apos;initialisation a échoué. %s est en train de s&apos;arrêter.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation>Fonds insuffisants pour ce jeton. J&apos;ai besoin de %d plus.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation>Fonds insuffisants ou fonds non confirmés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation>Keypool est épuisé, veuillez d&apos;abord appeler keypoolrefill</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation>Chargement d&apos;Orphanpool</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation>Chargement du TxPool</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation>Chargement de la liste de bannissement</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Opening Block database...</source>
        <translation>Ouverture de la base de données des blocs...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation>Ouverture de la base de données Coins Cache...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Description database...</source>
        <translation>Ouverture de la base de données de description des jetons...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Mintage database...</source>
        <translation>Ouverture de la base de données Token Mintage...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation>Ouverture de la base de données UTXO...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation>Portions Copyright (C) 2009-%i Les Bitcoin Core développeurs</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation>Portions Copyright (C) 2014-%i Les Bitcoin XT développeurs</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation>Réaccepter les transactions Wallet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>Nouvelle analyse…</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation>Désactivez la consolidation automatique et réessayez d&apos;envoyer.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>Impossible de se lier à %s sur cet ordinateur. %s est probablement déjà en cours d&apos;exécution.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation>Impossible de démarrer les services RPC. Voir le journal de débogage pour plus de détails.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation>Mise à jour de la base de données de blocs... Cela peut prendre un certain temps.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation>Mise à jour de la base de données txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation>Mise à jour de la base de données txindex...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>Le portefeuille devait être réécrit&#xa0;: redémarrez %s pour terminer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zapping all transactions from wallet...</source>
        <translation>Zapping toutes les transactions du portefeuille...</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Done loading</source>
        <translation>Chargement terminé</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
</context>
</TS>
