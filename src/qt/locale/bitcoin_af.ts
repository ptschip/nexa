<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="af">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>Regs-kliek om die adres of etiket te verander</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Skep &apos;n nuwe adres</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Nuut</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Dupliseer die geselekteerde adres na die sisteem se geheuebord</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;Dupliseer</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>&amp;Dupliseer Adres</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Verwyder die adres wat u gekies het van die lys</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Voer die inligting op hierdie bladsy uit na &apos;n leer</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>&amp;Voer uit</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>&amp;Vee uit</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>Kies die adres waarheen u munte wil stuur</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Kies die adres wat die munte moet ontvang</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>Kies</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>Stuurders adresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Ontvanger adresse</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Hierdie is die adresse vanwaar u Nexa betalings stuur. U moet altyd die bedrag en die adres van die ontvanger nagaan voordat u enige munte stuur.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Hierdie is die adresse waar u Nexas sal ontvang. Ons beveel aan dat u &apos;n nuwe adres kies vir elke transaksie</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>Kopieer &amp;etiketteer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>&amp;Verander</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>Voer adreslys uit</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Comma separated file (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>Uitvoer was onsuksesvol</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Die adreslys kon nie in %1  gestoor word nie.  Probeer asseblief weer.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(geen etiket)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Wagwoord Dialoog</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Tik u wagwoord in</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Nuwe wagwoord</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Herhaal nuwe wagwoord</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>Kodifiseer beursie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>U het u beursie se wagwoord nodig om toegang tot u beursie te verkry.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Sluit beursie oop</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>U het u beursie se wagwoord nodig om u beursie se kode te ontsyfer.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Ontsleutel beursie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Verander wagwoord</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>Bevestig dat die beursie gekodifiseer is</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Waarskuwing: Indien u die beursie kodifiseer en u vergeet u wagwoord &lt;b&gt;VERLOOR U AL U NEXA COINS&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Is u seker dat u die beursie wil kodifiseer?</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Wallet decryption failed</source>
        <translation>Wallet-dekripsie het misluk</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>Wallet-wagfrase is suksesvol verander.</translation>
    </message>
    <message>
        <location line="+50"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>WAARSKUWING:  Outomatiese Kapitalisering is aktief op u sleutelbord!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Beursie gekodifiseer</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Tik die nuwe wagwoord vir u beursie.&lt;br/&gt;Gerbuik asseblief &apos;n wagwoord met &lt;b&gt;tien of meer lukrake karakters&lt;/b&gt;, of &lt;b&gt;agt of meer woorde&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Tik die ou en die nuwe wagwoorde vir die beursie.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation>%1 sal nou toemaak om die enkripsieproses te voltooi. Onthou dat die enkripteer van jou beursie nie jou munte ten volle kan beskerm teen gesteel word deur wanware wat jou rekenaar besmet nie.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation>BELANGRIK: Enige vorige rugsteun wat jy van jou beursielêer gemaak het, moet vervang word met die nuutgegenereerde, geënkripteerde beursielêer. Om sekuriteitsredes moet jy nie meer vorige ongeënkripteerde rugsteune hou nie, aangesien jou fondse in gevaar sal wees as iemand toegang daartoe kry.</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>Kodifikasie was onsuksesvol</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>Weens &apos;n interne fout het kodifikasie het nie geslaag nie.  U beursie is nie gekodifiseer nie</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>Die wagwoorde stem nie ooreen nie.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>Die beursie is nie oopgesluit nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>U het die verkeerde wagwoord ingetik.</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>Banned Until</source>
        <translation>Verban tot</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>IP/Netmask</source>
        <translation>IP/Netmasker</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>Gebruikersagent</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation>Ban Rede</translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+786"/>
        <source>Synchronizing with network...</source>
        <translation>Netwerk-sinkronisasie...</translation>
    </message>
    <message>
        <location line="-556"/>
        <source>&amp;Overview</source>
        <translation>&amp;Oorsig</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>Node</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>Vertoon &apos;n algemene oorsig van die beursie</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>&amp;Transactions</source>
        <translation>&amp;Transaksies</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Blaai deur transaksiegeskiedenis</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation>&amp;Tekens</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation>Blaai deur of stuur tekens</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation>Tekengeskiedenis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation>Blaai deur Token Geskiedenis</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>&amp;Verlaat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Stop en verlaat die applikasie</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation>&amp;Oor %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation>Wys inligting oor %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>Oor &amp;Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Wys inligting oor Qt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;Opsies</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation>Verander konfigurasieopsies vir %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;Wys / Versteek</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Unlimited...</source>
        <translation>&amp;Unlimited...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation>Verander Bitcoin Unlimited Options</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>&amp;Kodifiseer Beursie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>&amp;Rugsteun-kopie van Beursie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Restore Wallet...</source>
        <translation>&amp;Herstel beursie …</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation>Herstel beursie vanaf &apos;n ander plek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>&amp;Verander Wagwoord</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sign &amp;message...</source>
        <translation>Teken &amp;boodskap...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Verify message...</source>
        <translation>&amp;Verifieer boodskap...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Debug window</source>
        <translation>&amp;Ontfout venster</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Maak ontfoutings- en diagnostiese konsole oop</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Sending addresses...</source>
        <translation>&amp;Versending adresse...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>&amp;Ontvanger adresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>Oop &amp; URI...</translation>
    </message>
    <message>
        <location line="+633"/>
        <source>Date: %1
</source>
        <translation>Datum: %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Bedrag: %1</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Tipe: %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Etiket: %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Adres: %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Gestuurde transaksie</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Inkomende transaksie</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation>HD-sleutelgenerering is &lt;b&gt;geaktiveer&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation>HD-sleutelgenerering is &lt;b&gt;gedeaktiveer&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>Beursie is&lt;b&gt;geïnkripteer&lt;/b&gt; en tans &lt;b&gt;ontsluit&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>Beursie is&lt;b&gt;geïnkripteer&lt;/b&gt; en tans &lt;b&gt;gesluit&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="-303"/>
        <source>Importing blocks from disk...</source>
        <translation>Besig om blokke vanaf die hardeskyf in te voer...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>Besig met herindeksering van blokke op hardeskyf...</translation>
    </message>
    <message>
        <location line="-554"/>
        <source>Send coins to a Nexa address</source>
        <translation>Stuur munte na &apos;n Nexa adres</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Backup wallet to another location</source>
        <translation>Maak &apos;n rugsteun-kopié van beursie na &apos;n ander stoorplek</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Verander die wagwoord wat ek vir kodifikasie van my beursie gebruik</translation>
    </message>
    <message>
        <location line="+219"/>
        <source>%1 client</source>
        <translation>%1 kliënt</translation>
    </message>
    <message numerus="yes">
        <location line="+193"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n aktiewe verbinding(s) met Nexa-netwerk</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+34"/>
        <source>No block source available...</source>
        <translation>Geen blokbron beskikbaar nie...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>Verwerk %n blokke van transaksiegeskiedenis.</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Up to date</source>
        <translation>Op datum</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Catching up...</source>
        <translation>Inhaal...</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-785"/>
        <source>Wallet</source>
        <translation>Beursie</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>&amp;Stuur</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>&amp;Ontvang</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Show or hide the main Window</source>
        <translation>Wys of versteek die hoofbladsy</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>Kodifiseer die private sleutes wat aan jou beursie gekoppel is.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Onderteken boodskappe met u Nexa adresse om u eienaarskap te bewys</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>Verifieër boodskappe om seker te maak dat dit met die gespesifiseerde Nexa adresse</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>&amp;Command-line options</source>
        <translation>&amp;Bevelreël opsies</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>Wys die %1 hulpboodskap om &apos;n lys met moontlike Nexa-opdragreëlopsies te kry</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>&amp;File</source>
        <translation>&amp;Lêer</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>&amp;Instellings</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Orebalk</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Vertoon die lys van gebruikte versendingsadresse en etikette</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Vertoon die lys van gebruikte ontvangers-adresse en etikette</translation>
    </message>
    <message>
        <location line="+466"/>
        <source>%1 behind</source>
        <translation>%1 agter</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>Laaste ontvange blok is %1 gelede gegenereer.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Transaksies hierna sal nog nie sigbaar wees nie.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Waarskuwing</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>Inligting</translation>
    </message>
    <message>
        <location line="-647"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation>Versoek betalings (genereer QR-kodes en %1: URI&apos;s)</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Open a %1: URI or payment request</source>
        <translation>Skep &apos;n %1: URI of betalingsversoek</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>Kopieer Adres</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopieer etiketteer</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>Kopieer bedrag</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>Kopieer transaksie-ID</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>Sluit onbestee</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>Ontsluit onbestede</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>Kopieer hoeveelheid</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Kopieer fooi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Kopie na fooi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Kopieer grepe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Kopieer prioriteit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Kopieer stof</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>kopie verandering</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>hoogste</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>hoër</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>hoog</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>medium-hoog</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>medium</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>laag-medium</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>laag</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>laer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>laagste</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 gesluit)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>geen</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>geen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>Hierdie etiket word rooi as die transaksiegrootte groter as 1000 grepe is.</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>Dit beteken &apos;n fooi van ten minste %1 per kB word vereis.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>Kan +/- 1 greep per invoer wissel.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>Transaksies met &apos;n hoër prioriteit is meer geneig om in &apos;n blok ingesluit te word.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>Hierdie etiket word rooi as die prioriteit kleiner as &quot;medium&quot; is.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>Hierdie etiket word rooi as enige ontvanger &apos;n bedrag kleiner as %1 ontvang.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>Kan +/- %1 satoshi(s) per invoer wissel.</translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(geen etiket)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>verander vanaf %1 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(verandering)</translation>
    </message>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>Muntkeuse</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>Hoeveel:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>Grepe:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>Bedrag:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>Prioriteit:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>Fooi:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Stof:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Na fooi:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Verandering:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>(on) kies maksimum insette</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>Boommodus</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>Lysmodus</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>Bedrag</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>Ontvang met etiket</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>Ontvang met adres</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>Bevestigings</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>Bevestig</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>Prioriteit</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>Wysig adres</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>&amp;Etiket</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>Die etiket wat met hierdie adreslysinskrywing geassosieer word</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Address</source>
        <translation>&amp;Adres</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>Die adres wat met hierdie adreslysinskrywing geassosieer word. Dit kan slegs gewysig word vir die stuur van adresse.</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>Nuwe ontvangsadres</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>Nuwe stuuradres</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>Wysig ontvangsadres</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>Wysig stuuradres</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>Die ingevoerde adres &quot;%1&quot; is nie &apos;n geldige Nexa-adres nie.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>Die ingevoerde adres &quot;%1&quot; is reeds in die adresboek.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not unlock wallet.</source>
        <translation>Kon nie beursie ontsluit nie.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>Nuwe sleutelgenerasie het misluk.</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>&apos;n Nuwe datagids sal geskep word.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>naam</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>Gids bestaan reeds. Voeg %1 by as jy van plan is om &apos;n nuwe gids hier te skep.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>Pad bestaan reeds en is nie &apos;n gids nie.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>Kan nie datagids hier skep nie.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>weergawe</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1-bis)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation>Oor %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>Bevelreël opsies</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>Gebruik:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>bevelreël opsies</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>Welkom</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation>Welkom by %1.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation>Aangesien dit die eerste keer is dat die program geloods word, kan jy kies waar %1 sy data sal stoor.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation>%1 sal &apos;n kopie van die Nexa-blokketting aflaai en stoor. Minstens %2GB se data sal in hierdie gids gestoor word, en dit sal mettertyd groei. Die beursie sal ook in hierdie gids gestoor word.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>Gebruik die verstekdatagids</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>Gebruik &apos;n gepasmaakte datagids:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>Fout: Gespesifiseerde datagids &quot;%1&quot; kan nie geskep word nie.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n GB vrye spasie beskikbaar</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(van %n GB benodig)</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Stroomop verkeersvormingparameters kan nie leeg wees nie</translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation>Die vertoonde inligting kan verouderd wees. Jou beursie sinchroniseer outomaties met die Nexa-netwerk nadat &apos;n verbinding tot stand gebring is, maar hierdie proses is nog nie voltooi nie. Dit beteken dat onlangse transaksies nie sigbaar sal wees nie, en die balans sal nie op datum wees totdat hierdie proses voltooi is nie.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation>Die besteding van munte is dalk nie moontlik gedurende daardie fase nie!</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation>Aantal blokkies oor</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation>onbekend...</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation>Laaste bloktyd</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation>Vordering</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation>Vordering verhoog per uur</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation>bereken...</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation>Geskatte tyd oor tot gesinkroniseer</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation>Versteek</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation>Onbekend. Herindekseer (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation>Onbekend. Herindekseer...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation>Onbekend. Sinkroniseer tans opskrifte (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation>Onbekend. Sinkroniseer tans opskrifte...</translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>Maak URI oop</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>Maak betalingversoek vanaf URI of lêer oop</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>Kies betalingsversoeklêer</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>Kies betalingversoeklêer om oop te maak</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Opsies</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>&amp;Hoof</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>Begin %1 outomaties nadat jy by die stelsel aangemeld het.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>&amp;Begin %1 by stelselaanmelding</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Inisieer outomaties &apos;n, slegs een keer, volledige databasis herindeks met die volgende opstart.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation>Herindeks by opstart</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Aantal skrip- en verifikasiedrade</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = outo, &lt;0 = laat soveel kerns vry)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Inisieer outomaties &apos;n volledige blokketting-hersinchronisasie met die volgende opstart (slegs een keer).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation>Hersinkroniseer blokdata tydens opstart</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>W&amp;allet</source>
        <translation>B&amp;eursie</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Kenner</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Of u muntbeheerkenmerke moet wys of nie.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable coin &amp;control features</source>
        <translation>Aktiveer muntbeheerkenmerke</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>As jy die besteding van onbevestigde verandering deaktiveer, kan die verandering van &apos;n transaksie nie gebruik word totdat daardie transaksie ten minste een bevestiging het nie. Dit beïnvloed ook hoe jou balans bereken word.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>&amp;Spandeer onbevestigde verandering</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wanneer Kitstransaksies geaktiveer is, kan jy onbevestigde transaksies onmiddellik spandeer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation>&amp;Onmiddellike transaksies</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wanneer transaksies geskep en gestuur word, sal outo-konsolideer, indien nodig, outomaties &apos;n ketting transaksies skep wat insette het wat nie groter is as die konsensus-invoerlimiet nie.&lt;/p&gt;&lt;/body &gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation>Outo-konsolideer</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation>&amp;Skandeer beursie weer tydens opstart</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Network</source>
        <translation>&amp;Netwerk</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Maak die Nexa-kliëntpoort outomaties op die router oop. Dit werk net wanneer jou router UPnP ondersteun en dit is geaktiveer.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Kaartpoort met &amp;UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation>Aanvaar verbindings van buite.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Allow incoming connections</source>
        <translation>Laat inkomende verbindings toe</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Koppel aan die Nexa-netwerk deur &apos;n SOCKS5-instaanbediener.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>&amp;Koppel deur SOCKS5-instaanbediener (verstek-instaanbediener):</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>Proxy &amp;IP:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>IP-adres van die instaanbediener (bv. IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="-180"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;Poort:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Poort van die instaanbediener (bv. 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Word gebruik om eweknieë te bereik via:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation>Wys of die verskafde verstek SOCKS5-instaanbediener gebruik word om eweknieë via hierdie netwerktipe te bereik.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Koppel aan die Nexa-netwerk deur &apos;n aparte SOCKS5-instaanbediener vir Tor-versteekte dienste.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Gebruik aparte SOCKS5-instaanbediener om eweknieë te bereik via Tor-versteekte dienste:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>&amp;Venster</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Wys slegs &apos;n skinkbordikoon nadat die venster geminimaliseer is.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>&amp;Minimaliseer na die skinkbord in plaas van die taakbalk</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>Minimaliseer in plaas daarvan om die toepassing te verlaat wanneer die venster toe is. Wanneer hierdie opsie geaktiveer is, sal die toepassing slegs gesluit word nadat u Verlaat in die kieslys gekies het.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>M&amp;inimize on close</source>
        <translation>M&amp;inimaliseer op naby</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>&amp;Vertoon</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>Gebruikerskoppelvlak en taal:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>Die gebruikerskoppelvlaktaal kan hier ingestel word. Hierdie instelling sal in werking tree nadat %1 herbegin is.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>&amp;Eenheid om bedrae te wys in:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Kies die verstek onderafdeling eenheid om te wys in die koppelvlak en wanneer munte gestuur word.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>Kies die verstek onderafdeling eenheid om te wys in die koppelvlak en wanneer &apos;n woord gestuur word.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>Derdeparty-transaksie-URL&apos;e</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Aktiewe opdragreëlopsies wat bogenoemde opsies ignoreer:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>Stel alle kliëntopsies terug na verstek.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Terugopsies</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Kanselleer</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+124"/>
        <source>default</source>
        <translation>verstek</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>geen</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Confirm options reset</source>
        <translation>Bevestig opsies-terugstelling</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>Kliënt herbegin vereis om veranderinge te aktiveer.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Kliënt sal gesluit word. Wil jy voortgaan?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Hierdie verandering sal &apos;n kliënt herbegin vereis.</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>Die verskafde proxy-adres is ongeldig.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Vorm</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Balances</source>
        <translation>Saldo&apos;s</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>Die vertoonde inligting kan verouderd wees. Jou beursie sinchroniseer outomaties met die Nexa-netwerk nadat &apos;n verbinding tot stand gebring is, maar hierdie proses is nog nie voltooi nie.</translation>
    </message>
    <message>
        <location line="-333"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Onbevestigde transaksies na kyk-alleen-adresse</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Totaal van transaksies wat nog bevestig moet word en nog nie tel vir die besteebare saldo nie</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Ontgin balans in net-kyk-adresse wat nog nie verval het nie</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Total:</source>
        <translation>Totaal:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Ontgin balans wat nog nie volwasse is nie</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Immature:</source>
        <translation>Onvolwasse:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current total balance</source>
        <translation>Jou huidige totale saldo</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Huidige totale balans in net-kyk-adresse</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Watch-only:</source>
        <translation>Kyk net:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>Beskikbaar:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>Jou huidige besteebare saldo</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>Jou huidige saldo in net-kyk-adresse</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Pending:</source>
        <translation>Hangende:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Spendable:</source>
        <translation>Spandeerbaar:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Onlangse transaksies</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+375"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>Betalingsversoekfout</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation>Kan nie klik-om-te-betaal hanteerder begin nie</translation>
    </message>
    <message>
        <location line="+95"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>URI hantering</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>Betaalversoek haal URL is ongeldig: %1</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid payment address %1</source>
        <translation>Ongeldige betalingadres %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>URI kan nie ontleed word nie! Dit kan veroorsaak word deur &apos;n ongeldige Nexa-adres of wanvormde URI-parameters.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>Betaalversoek lêer hantering</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>Betalingsversoeklêer kan nie gelees word nie! Dit kan veroorsaak word deur &apos;n ongeldige betalingsversoeklêer.</translation>
    </message>
    <message>
        <location line="+65"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>Betalingsversoek is afgekeur</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>Betalingsversoeknetwerk pas nie by kliëntnetwerk nie.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Payment request expired.</source>
        <translation>Betalingsversoek het verval.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment request is not initialized.</source>
        <translation>Betalingsversoek is nie geïnisialiseer nie.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>Ongeverifieerde betalingsversoeke na gepasmaakte betaalskrifte word nie ondersteun nie.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>Ongeldige betalingsversoek.</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>Versoekte betalingsbedrag van %1 is te klein (beskou as stof).</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Refund from %1</source>
        <translation>Terugbetaling vanaf %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>Betalingsversoek %1 is te groot (%2 grepe, toegelaat %3 grepe).</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>Kon nie met %1 kommunikeer nie: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>Betalingsversoek kan nie ontleed word nie!</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>Slegte antwoord vanaf bediener %1</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Network request error</source>
        <translation>Netwerkversoekfout</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment acknowledged</source>
        <translation>Betaling erken</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+106"/>
        <source>Node/Service</source>
        <translation>Nodus/diens</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>Gebruikersagent</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Ping tyd</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation>Voer &apos;n NEXA-adres in (bv. %1)</translation>
    </message>
    <message>
        <location line="+832"/>
        <source>%1 d</source>
        <translation>%1 d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 u</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 s</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>NVT</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation>
            <numerusform>%n sekonde</numerusform>
            <numerusform>%n sekondes</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation>
            <numerusform>%n minuut</numerusform>
            <numerusform>%n minute</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n urr</numerusform>
            <numerusform>%n urr</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n dag</numerusform>
            <numerusform>%n dae</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n week</numerusform>
            <numerusform>%n weke</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 en %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n jaar</numerusform>
            <numerusform>%n jare</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Bedrag</translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Stoor prent...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>&amp;Kopieer prent</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>Stoor QR-kode</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG-prent (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+14"/>
        <source>Debug window</source>
        <translation>Ontfout venster</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Information</source>
        <translation>&amp;Inligting</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Client version</source>
        <translation>Kliënt weergawe</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>Gebruikersagent</translation>
    </message>
    <message>
        <location line="-2380"/>
        <source>Using BerkeleyDB version</source>
        <translation>Gebruik BerkeleyDB weergawe</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Datadir</source>
        <translation>Data gids</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Startup time</source>
        <translation>Opstart tyd</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>Aantal verbindings</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Current number of blocks</source>
        <translation>Huidige aantal blokke</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block time (time since)</source>
        <translation>Laaste bloktyd (tyd sedert)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation>Laaste blokgrootte</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation>Transaksies in Tx-poel</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation>Transaksies in weespoel</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation>Boodskappe in CAPD-poel</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation>Tx swembad - gebruik</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation>Tx swembad - txns per sekonde</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation>XThin (totale)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation>XThin (24-uur-gemiddeldes)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation>Kompak (totale)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation>Kompak (24-uur-gemiddeldes)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation>Grafeen (totale)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation>Grafeen (24-uur-gemiddeldes)</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Debug log file</source>
        <translation>Ontfout loglêer</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>Maak die %1 ontfoutingsloglêer oop vanaf die huidige datagids. Dit kan &apos;n paar sekondes neem vir groot loglêers.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Open</source>
        <translation>&amp;Maak oop</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>NVT</translation>
    </message>
    <message>
        <location line="-2407"/>
        <source>Block Propagation</source>
        <translation>Blokvoortplanting</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation>Transaksiepoele</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Block chain</source>
        <translation>Blokketting</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Network</source>
        <translation>Netwerk</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>General</source>
        <translation>Algemeen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Console</source>
        <translation>&amp;Konsole</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Decrease font size</source>
        <translation>Verminder lettergrootte</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>Verhoog lettergrootte</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Clear console</source>
        <translation>Maak konsole skoon</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>&amp;Network Traffic</source>
        <translation>&amp;Netwerkverkeer</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>&amp;Duidelik</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Totale</translation>
    </message>
    <message>
        <location line="+64"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Ontvang</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Gestuur</translation>
    </message>
    <message>
        <location line="-1550"/>
        <source>&amp;Transaction Rate</source>
        <translation>&amp;Transaksiekoers</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation>Oombliklike koers (1s)</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation>Piek</translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation>Looptyd</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation>24 ure</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation>Vertoon</translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation>Gladde koers (60s)</translation>
    </message>
    <message>
        <location line="+539"/>
        <source>&amp;Peers</source>
        <translation>&amp;Eweknieë</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Verban eweknieë</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Kies &apos;n eweknie om gedetailleerde inligting te sien.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>Gewitlys</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>Rigting</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Weergawe</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Services</source>
        <translation>Dienste</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Starting Block</source>
        <translation>Beginblok</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>Gesinkroniseerde opskrifte</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>Gesinkroniseerde blokke</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Ban Score</source>
        <translation>Ban telling</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Verbindingstyd</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Laaste Stuur</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Laaste ontvangs</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Ping tyd</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>Die duur van &apos;n tans uitstaande ping.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Ping wag</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Tydsverrekening</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-736"/>
        <source>&amp;Disconnect Node</source>
        <translation>&amp;Ontkoppel nodus</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>Verbied Node vir</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 uur</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 dag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 week</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 jaar</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>&amp;Unban Node</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>Welcome to the %1 RPC console.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>Gebruik op- en afpyltjies om geskiedenis te navigeer, en &lt;b&gt;Ctrl-L&lt;/b&gt; om skerm skoon te maak.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>Tik &lt;b&gt;help&lt;/b&gt; vir &apos;n oorsig van beskikbare opdragte.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>In:</source>
        <translation>In:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>Uit:</translation>
    </message>
    <message>
        <location line="+90"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation>Gestrem</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(node-id: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>via %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>nooit</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>Inkomende</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>Uitgaande</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>Onbekend</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+45"/>
        <source>&amp;Request payment</source>
        <translation>&amp;Versoek betaling</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Clear all fields of the form.</source>
        <translation>Vee alle velde van die vorm uit.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Duidelik</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+7"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>&apos;n Opsionele bedrag om aan te vra. Laat dit leeg of nul om nie &apos;n spesifieke bedrag te versoek nie.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Amount:</source>
        <translation>&amp;Bedrag:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+56"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>&apos;n Opsionele etiket om met die nuwe ontvangsadres te assosieer.</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>&amp;Label:</source>
        <translation>&amp;Etiket</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Hergebruik een van die voorheen gebruikte ontvangsadresse. Die hergebruik van adresse het sekuriteit- en privaatheidskwessies. Moenie dit gebruik nie, tensy &apos;n betalingsversoek wat voorheen gemaak is, hergenereer word.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>Hergebruik &apos;n bestaande ontvangsadres (nie aanbeveel nie)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Gebruik hierdie vorm om betalings aan te vra. Alle velde is &lt;b&gt;opsioneel&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+23"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>&apos;n Opsionele boodskap om aan die betalingsversoek te heg, wat vertoon sal word wanneer die versoek oopgemaak word. Let wel: Die boodskap sal nie saam met die betaling oor die Nexa-netwerk gestuur word nie.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>&amp;Message:</source>
        <translation>&amp;Boodskap</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Coin freezing locks coins to make them temporarily unspendable. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Muntbevriesing sluit munte om dit tydelik onbesteebaar te maak. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Coin &amp;Freeze</source>
        <translation>Muntbevriesing</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Requested payments history</source>
        <translation>Versoek betalingsgeskiedenis</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Wys die geselekteerde versoek (doen dieselfde as om op &apos;n inskrywing te dubbelklik)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Wys</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Verwyder die geselekteerde inskrywings uit die lys</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Verwyder</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+53"/>
        <source>Copy URI</source>
        <translation>Kopieer URI</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopieer etiketteer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Kopieer boodskap</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopieer bedrag</translation>
    </message>
</context>
<context>
    <name>ReceiveFreezeDialog</name>
    <message>
        <location filename="../forms/receivefreezedialog.ui" line="+14"/>
        <source>Coin Freeze</source>
        <translation></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>WARNING! Freezing coins means they will be UNSPENDABLE until the release date or block specified below.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Freeze until block :</source>
        <translation></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify the block when coins are released from the freeze. Coins are UNSPENDABLE until the freeze block.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+99"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="-57"/>
        <source>OR</source>
        <translation></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Freeze until date and time :</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Specify the future date and time when coins are released from the freeze. Coins are UNSPENDABLE until after the freeze date and time.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>&amp;Reset</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+R</source>
        <translation></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>Versoek betaling aan %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Betaling Inligting</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>Bedrag</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Etiket</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>Boodskap</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Freeze until</source>
        <translation>Vries tot</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>Gevolglike URI te lank, probeer om die teks vir etiket/boodskap te verminder.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Kon nie URI in QR-kode enkodeer nie.</translation>
    </message>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>QR kode</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>Kopieer &amp;URI</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>&amp;Dupliseer Adres</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Stoor prent...</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>Etiket</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Boodskap</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>(no label)</source>
        <translation>(geen etiket)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>(geen boodskap)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(geen bedrag)</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Amount</source>
        <translation>Bedrag</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="+68"/>
        <source>Copy quantity</source>
        <translation>Kopieer hoeveelheid</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopieer bedrag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>Kopieer fooi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Kopie na fooi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Kopieer grepe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Kopieer prioriteit</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Kopieer stof</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>kopie verandering</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Publieke etiket:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 tot %2</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING!!! DESTINATION IS A FREEZE ADDRESS&lt;br&gt;UNSPENDABLE UNTIL&lt;/b&gt; %1 &lt;br&gt;*************************************************&lt;br&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;b&gt;WAARSKUWING!!! BESTEMMING IS &apos;N VRIESADRES&lt;br&gt;ONBESTEDEBAAR TOT&lt;/b&gt; %1 &lt;br&gt;********************************* ******************&lt;br&gt;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Are you sure you want to send?</source>
        <translation>Is jy seker jy wil stuur?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>bygevoeg as transaksiefooi</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Total Amount %1</source>
        <translation>Totaal Bedrag: %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>of</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Confirm send coins</source>
        <translation>Bevestig stuur munte</translation>
    </message>
    <message>
        <location line="+185"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>Die ontvangeradres is nie geldig nie. Kontroleer asseblief weer.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>Die bedrag om te betaal moet groter as 0 wees.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>Die bedrag oorskry jou saldo.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>Die totaal oorskry jou saldo wanneer die %1 transaksiefooi ingesluit is.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>Duplikaatadres gevind: adresse moet slegs een keer elk gebruik word.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction creation failed!</source>
        <translation>Kon nie transaksie skep nie!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>Die transaksie is afgekeur! Dit kan gebeur as sommige van die munte in jou beursie reeds spandeer is, soos as jy &apos;n kopie van wallet.dat gebruik het en munte is in die kopie spandeer, maar nie gemerk as hier spandeer nie.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>&apos;n Fooi hoër as %1 word as &apos;n absurde hoë fooi beskou.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>Betalingsversoek het verval.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation>Publieke etiket oorskry limiet van</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation>Betaal slegs die vereiste fooi van %1</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Waarskuwing: Ongeldige Nexa-adres</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Warning: Unknown change address</source>
        <translation>Waarskuwing: Onbekende verander adres</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirm custom change address</source>
        <translation>Bevestig pasgemaakte veranderingsadres</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation>Die adres wat jy gekies het vir verandering is nie deel van hierdie beursie nie. Enige of alle fondse in jou beursie kan na hierdie adres gestuur word. Is jy seker?</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(geen etiket)</translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="-253"/>
        <source>Send Coins</source>
        <translation>Stuur munte</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Muntbeheerkenmerke</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation>Kies spesifieke munte wat jy in hierdie transaksie wil gebruik</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>Insette...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>outomaties gekies</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Onvoldoende fondse!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Hoeveel:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Grepe:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Bedrag:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>Prioriteit:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Fooi:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Stof:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Na fooi:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Verandering:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>As dit geaktiveer is, maar die veranderingsadres leeg of ongeldig is, sal verandering na &apos;n nuutgegenereerde adres gestuur word.</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>Pasgemaakte verander adres</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>Transaksie Fooi</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation>Kies jou transaksiefooi.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation>Kies...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>fooi-instellings ineenstort</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide</source>
        <translation>Versteek</translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>As die pasgemaakte fooi gestel is op 1000 satoshis en die transaksie is slegs 250 grepe, dan betaal &quot;per kilogrepe&quot; slegs 250 satoshis in fooi, terwyl &quot;totaal ten minste&quot; 1000 satoshis betaal. Vir transaksies groter as &apos;n kilogreep betaal albei per kilogreep.</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>per kilobyte</source>
        <translation>per kilogreep</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>total at least</source>
        <translation>altesaam ten minste</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation>Pasgemaakte fooi bedrag</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>Om net die minimum fooi te betaal, is net goed solank daar minder transaksievolume is as spasie in die blokke. Maar wees bewus daarvan dat dit in &apos;n nooit-bevestigende transaksie kan eindig sodra daar meer aanvraag na nexa-transaksies is as wat die netwerk kan verwerk.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(lees die nutswenk)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation>Gebruik die aanbevole fooibedrag. Jy kan &apos;n vinniger of stadiger bevestigingstyd kies deur die &quot;Bevestigingstyd&quot;-skuifbalk te skuif.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation>Aanbeveel:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation>Kies &apos;n pasgemaakte fooibedrag</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation>Pasgemaak:</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(Slimfooi is nog nie geïnisialiseer nie. Dit neem gewoonlik &apos;n paar blokke...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation>Transaksie bevestiging tyd</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation>Bevestigingstyd:</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation>normaal</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>vinnig</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>Stuur as &apos;n nul-fooi transaksie indien moontlik</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(bevestiging kan langer neem)</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Confirm the send action</source>
        <translation>Bevestig die stuuraksie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>&amp;Stuur</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Clear all fields of the form.</source>
        <translation>Vee alle velde van die vorm uit.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear &amp;All</source>
        <translation>&amp;Vee alles uit</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Send to multiple recipients at once</source>
        <translation>Stuur aan verskeie ontvangers gelyktydig</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Voeg &amp;ontvanger by</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Current Balance</source>
        <translation>Huidige balaans</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>Saldo:</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+44"/>
        <source>Amount to send</source>
        <translation>Bedrag om te stuur</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>Die fooi sal afgetrek word van die bedrag wat gestuur word. Die ontvanger sal minder munte ontvang as wat jy in die bedragveld invoer. As verskeie ontvangers gekies word, word die fooi gelykop verdeel.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>&amp;Trek fooi van bedrag af</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Private Description:</source>
        <translation>Privaat beskrywing:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation>Privaat &amp;Etiket:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>&amp;Bedrag:</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>Betaal &amp;Aan:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Die Nexa-adres om die betaling na te stuur</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>Kies voorheen gebruikte adres</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Plak adres vanaf knipbord</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>Verwyder hierdie inskrywing</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>&apos;n Boodskap wat aan die muntstuk geheg is: URI wat saam met die transaksie gestoor sal word vir jou verwysing. Let wel: Hierdie boodskap sal nie oor die Nexa-netwerk gestuur word nie.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation>Voer &apos;n private etiket vir hierdie adres in om dit by die lys van gebruikte adresse te voeg</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation>Voer &apos;n publieke etiket vir hierdie transaksie in</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation>Publieke etiket:</translation>
    </message>
    <message>
        <location line="+466"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Dit is &apos;n ongeverifieerde betalingsversoek.</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>Betaal aan:</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>Memo:</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>This is an authenticated payment request.</source>
        <translation>Dit is &apos;n geverifieerde betalingsversoek.</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>&apos;n Boodskap wat aan die %1 URI geheg is wat saam met die transaksie gestoor sal word vir jou verwysing. Let wel: Hierdie boodskap sal nie oor die Nexa-netwerk gestuur word nie.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation>Voer &apos;n private etiket vir hierdie adres in om dit by jou adresboek te voeg</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>% 1 sluit tans af...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>Moenie die rekenaar afskakel voordat hierdie venster verdwyn nie.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Handtekeninge - Teken / Verifieer &apos;n boodskap</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>Teken &amp;Boodskap...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Jy kan boodskappe/ooreenkomste met jou adresse teken om te bewys dat jy munte kan ontvang wat aan hulle gestuur word. Wees versigtig om nie enigiets vaag of lukraak te teken nie, aangesien uitvissing-aanvalle jou kan probeer mislei om jou identiteit aan hulle te teken. Teken slegs volledig gedetailleerde verklarings waartoe jy instem.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>Die Nexa-adres om die boodskap mee te onderteken</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Kies voorheen gebruikte adres</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Plak adres vanaf knipbord</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Voer die boodskap wat jy wil onderteken hier in</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Handtekening</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Kopieer die huidige handtekening na die stelselknipbord</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Teken die boodskap om te bewys dat jy hierdie Nexa-adres besit</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>&amp;Teken Boodskap...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Stel alle tekenboodskapvelde terug</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>Vee alles uit</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>&amp;Verifieer Boodskap...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>Voer die ontvanger se adres, boodskap in (maak seker jy kopieer lynbreuke, spasies, oortjies, ens. presies) en handtekening hieronder om die boodskap te verifieer. Wees versigtig om nie meer in die handtekening te lees as wat in die getekende boodskap self is nie, om te verhoed dat jy deur &apos;n man-in-die-middel-aanval mislei word. Let daarop dat dit slegs bewys dat die ondertekenende party met die adres ontvang, dit kan nie senderskap van enige transaksie bewys nie!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>Die Nexa-adres waarmee die boodskap onderteken is</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Verifieer die boodskap om te verseker dat dit met die gespesifiseerde Nexa-adres onderteken is</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>Verifieer &amp;Boodskap...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Stel alle verifieerboodskapvelde terug</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>Klik &quot;Teken boodskap&quot; om handtekening te genereer</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>Beursie-ontsluiting is gekanselleer.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Boodskap onderteken.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Boodskap geverifieer.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation>Tokenbesonderhede</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hierdie paneel wys &apos;n gedetailleerde beskrywing van die teken&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Almal</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Vandag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Hierdie week</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Hierdie maand</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Laaste maand</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Hierdie jar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Reeks...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Ontvang met</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation>Gestuur</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Aan jouself</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation>Munt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Ander</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation>Voer token-ID in om te soek</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Min bedrag</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation>Kopieer token-ID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopieer bedrag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>Kopieer transaksie-idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>Kopieer rou transaksie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Wys transaksiebesonderhede</translation>
    </message>
    <message>
        <location line="+162"/>
        <source>Export Token History</source>
        <translation>Voer Token Geskiedenis uit</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Komma-geskeide lêer (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Bevestig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>Kyk net</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Tipe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Etiket</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation>Transaksie ID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation>Teken ID</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation>Tokenbedrag</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>Uitvoer was onsuksesvol</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation>Kon nie die tekengeskiedenis na %1 probeer stoor nie.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Uitvoer suksesvol</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation>Die tekengeskiedenis is suksesvol na %1 gestoor.</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Range:</source>
        <translation>Reeks:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>aan</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+244"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Tipe</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation>Teken ID</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation>Netto bedrag</translation>
    </message>
    <message numerus="yes">
        <location line="+49"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Maak oop vir nog %n blok(e)</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>Maak oop tot %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Vanlyn</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Onbevestig</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Bevestig tans (% 1 van % 2 aanbevole bevestigings)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Bevestig (% 1 bevestigings)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>Konflik</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Dubbel bestee</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Verlate</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Onvolwasse (% 1 bevestigings, sal beskikbaar wees na % 2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Hierdie blok is nie deur enige ander nodusse ontvang nie en sal waarskynlik nie aanvaar word nie!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Gegenereer maar nie aanvaar nie</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Ontvang met</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Ontvang van</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation>Gestuur</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Betaling aan jouself</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation>Munt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation>Smelt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Openbare etiket</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Ander</translation>
    </message>
    <message>
        <location line="+323"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Transaksie status. Beweeg oor hierdie veld om die aantal bevestigings te wys.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Datum en tyd wat die transaksie ontvang is.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Tipe transaksie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Of &apos;n net-kykadres by hierdie transaksie betrokke is of nie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Gebruiker-gedefinieerde bedoeling/doel van die transaksie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Bedrag verwyder uit of bygevoeg by balans.</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation>Tekens</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Amount: </source>
        <translation>Bedrag: </translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Die aantal tokens om te stuur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation>Betaal aan:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Die Nexa-adres om die betaling na te stuur</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>Kies voorheen gebruikte adres</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Plak adres vanaf knipbord</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation>Verwyder hierdie inskrywing</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stuur tekens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation>Stuur</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation>Teken ID</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation>Die unieke token-identifiseerder</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation>Die tokengroepidentifiseerder</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation>Die naam van die teken</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>Tikker</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation>Die token-tikker-simbool</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation>Saldo</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation>Bevestigde tokenbalans</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation>Hangende</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation>Onbevestigde tokenbalans</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation>Sub</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation>As dit gemerk is, is hierdie item &apos;n subgroep.</translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+367"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation>string</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation>nommer</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation>NaN</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation>Totale oplage:</translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation>Token-opmaak is nie beskikbaar nie omdat die databasis &apos;n herindeks benodig</translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation>Naam:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation>Tikker:</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation>Hasj:</translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation>Desimale:</translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation>Huidige Owerhede:</translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation>Tokenowerhede is nie beskikbaar nie omdat die databasis &apos;n herindeks benodig</translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation>Saldo:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation>Hangende:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation>Is jy seker jy wil stuur?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation>&lt;b&gt;Teken(s)&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation>Bevestig stuurtokens</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message numerus="yes">
        <location filename="../transactiondesc.cpp" line="+34"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Maak oop vir nog %n blok(e)</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open until %1</source>
        <translation>Maak oop tot %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>double spent</source>
        <translation>dubbel bestee</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation>verlate</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>conflicted</source>
        <translation>conflik</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/vanlyn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/onbevestig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 bevestigings</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, is nog nie suksesvol uitgesaai nie</translation>
    </message>
    <message numerus="yes">
        <location line="+2"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, uitgesaai deur %n nodus(s)</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Oopbare etiket:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Gegenereer</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+114"/>
        <source>To</source>
        <translation>Aan</translation>
    </message>
    <message>
        <location line="-163"/>
        <location line="+13"/>
        <location line="+128"/>
        <source>From</source>
        <translation>Van</translation>
    </message>
    <message>
        <location line="-128"/>
        <source>unknown</source>
        <translation>onbekend</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>change address</source>
        <translation>adres verander</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>eie adres</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+119"/>
        <source>watch-only</source>
        <translation>wyk net</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+28"/>
        <location line="+113"/>
        <source>label</source>
        <translation>etiket</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+63"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation>Openbare etiket:</translation>
    </message>
    <message>
        <location line="-167"/>
        <source>Freeze until</source>
        <translation>Vries tot</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+213"/>
        <source>Credit</source>
        <translation>Krediet</translation>
    </message>
    <message numerus="yes">
        <location line="-361"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>verval in nog %n blok(e)</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>Nie aanvaar nie</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+218"/>
        <source>Debit</source>
        <translation>Debiet</translation>
    </message>
    <message>
        <location line="-237"/>
        <source>Total debit</source>
        <translation>Totaal debiet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Totaal krediet</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Transaksie fooi</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Netto bedrag</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+12"/>
        <source>Message</source>
        <translation>Boodskap</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Comment</source>
        <translation>kommentaar lewer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction Idem</source>
        <translation>Transaksie Idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation>Transaksie grootte</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Handelaar</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Gegenereerde munte moet %1 blokke verouder voordat hulle spandeer kan word. Toe jy hierdie blok gegenereer het, is dit na die netwerk uitgesaai om by die blokketting gevoeg te word. As dit nie daarin slaag om in die ketting te kom nie, sal sy toestand verander na &quot;nie aanvaar nie&quot; en dit sal nie besteebaar wees nie. Dit kan soms gebeur as &apos;n ander nodus &apos;n blok binne &apos;n paar sekondes van joune genereer.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Token ID</source>
        <translation>Teken ID</translation>
    </message>
    <message>
        <location line="-50"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Ticker</source>
        <translation>Tikker</translation>
    </message>
    <message>
        <location line="-54"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Decimals</source>
        <translation>Desimale</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>Melt</source>
        <translation>Smelt</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+34"/>
        <source>Mint</source>
        <translation>Munt</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Amount Sent</source>
        <translation>Bedrag gestuur</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Amount Received</source>
        <translation>Bedrag ontvang</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Debug information</source>
        <translation>Ontfout inligting</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Transaksie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>Insette</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+28"/>
        <location line="+34"/>
        <location line="+55"/>
        <source>Amount</source>
        <translation>Bedrag</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>waar</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>onwaar</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>Transaksiebesonderhede</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Hierdie paneel toon &apos;n gedetailleerde beskrywing van die transaksie</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation>tps</translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Tipe</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address or Label</source>
        <translation>Adres of etiket</translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Maak oop vir nog %n blok(e)</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>Maak oop tot %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Vanlyn</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Onbevestig</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Bevestig tans (% 1 van % 2 aanbevole bevestigings)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Bevestig (% 1 bevestigings)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>Konflik</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Dubbel bestee</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Verlate</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Onvolwasse (% 1 bevestigings, sal beskikbaar wees na % 2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Hierdie blok is nie deur enige ander nodusse ontvang nie en sal waarskynlik nie aanvaar word nie!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Gegenereer maar nie aanvaar nie</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Received with</source>
        <translation>Ontvang met</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Ontvang van</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Gestuur na</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Betaling aan jouself</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Gemyn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Openbare etiket</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Ander</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>wyk net:</translation>
    </message>
    <message>
        <location line="+276"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Transaksie status. Beweeg oor hierdie veld om die aantal bevestigings te wys.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Datum en tyd wat die transaksie ontvang is.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Tipe transaksie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Of &apos;n net-kykadres by hierdie transaksie betrokke is of nie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Gebruiker-gedefinieerde bedoeling/doel van die transaksie.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Bedrag verwyder uit of bygevoeg by balans.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+353"/>
        <source>Exporting Failed</source>
        <translation>Uitvoer was onsuksesvol</translation>
    </message>
    <message>
        <location line="-22"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Comma separated file (*.csv)</translation>
    </message>
    <message>
        <location line="-254"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Almal</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Vandag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Hierdie week</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Hierdie maand</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Laaste maand</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Hierdie jar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Reeks...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Ontvang met</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Gestuur na</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Aan jouself</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Gemyn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Ander</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Voer adres of etiket in om te soek</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Min bedrag</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Kopieer Adres</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopieer etiketteer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopieer bedrag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>Kopieer transaksie-idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>Kopieer rou transaksie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>Wysig etiket</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Wys transaksiebesonderhede</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Export Transaction History</source>
        <translation>Voer transaksiegeskiedenis uit</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Bevestig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>Kyk net</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Tipe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Etiket</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>Daar was &apos;n fout om die transaksiegeskiedenis na %1 te stoor.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Uitvoer suksesvol</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>Die transaksiegeskiedenis is suksesvol gestoor na %1.</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Range:</source>
        <translation>Reeks:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>aan</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+840"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Eenheid om bedrae in te wys. Klik om &apos;n ander eenheid te kies.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation>Unlimited</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation>&amp;Mynbou</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation>Die grootste blok wat ontgin gaan word</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation>Maksimum gegenereerde blokgrootte (grepe)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation>&amp;Netwerk</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation>Bandwydtebeperkings in KBytes/sek (merk om te aktiveer):</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation>Stuur</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation>Maks</translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation>Ontvang</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation>Aktiewe opdragreëlopsies wat bogenoemde opsies ignoreer:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation>Stel alle kliëntopsies terug na verstek.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Terugopsies</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Kanselleer</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation>Bevestig opsies-terugstelling</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation>Dit is &apos;n globale terugstelling van alle instellings!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation>Kliënt herbegin vereis om veranderinge te aktiveer.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Kliënt sal gesluit word. Wil jy voortgaan?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Stroomop verkeersvormingparameters kan nie leeg wees nie</translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation>Verkeersvormingsparameters moet groter as 0 wees.</translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>Geen beursie is gelaai nie.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation>Token-databasisstatus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation>Token-beursie nie beskikbaar nie omdat &apos;n -herindeks nodig is. Klik &quot;OK&quot; om &apos;n eenmalige herindeks uit te voer by die volgende opstart. Skakel dan Nexa-Qt af en herbegin om die proses te voltooi.</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+291"/>
        <source>Send Coins</source>
        <translation>Stuur munte</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>&amp;Voer uit</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Voer die inligting op hierdie bladsy uit na &apos;n leer</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>Rugsteun beursie</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Beursie Data (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>Rugsteun het misluk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>Kon nie die beursiedata na %1 probeer stoor nie.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Backup Successful</source>
        <translation>Rugsteun suksesvol</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>Die beursiedata is suksesvol na %1 gestoor.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation>Herstel beursie</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation>Herstel het misluk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation>Kon nie die beursiedata na %1 terugstel nie.</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+12"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>Die %s ontwikkelaars</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation>Bitcoin Bitcoin XT en Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation>-wallet.maxTxFee is baie hoog gestel! So groot fooie kan op &apos;n enkele transaksie betaal word.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation>-wallet.payTxFee is baie hoog gestel! Dit is die transaksiefooi wat jy sal betaal as jy &apos;n transaksie stuur.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>Kan nie &apos;n slot op datagids %s kry nie. %s loop waarskynlik reeds.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation>Kon nie RPC-eiebewyse opspoor nie. Geen stawingkoekie kon gevind word nie, en geen rpcpassword is in die konfigurasielêer gestel nie (%s)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation>Ontplooiing konfigurasie lêer &apos;%s&apos; het ongeldige data bevat - sien debug.log</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Versprei onder die MIT sagteware lisensie, sien die meegaande lêer COPYING of &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>Kon nie %s laai nie: Jy kan nie HD aktiveer op &apos;n reeds bestaande nie-HD-beursie nie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>Kon nie %s lees nie! Alle sleutels lees korrek, maar transaksiedata of adresboekinskrywings kan dalk ontbreek of verkeerd wees.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation>Kon nie van die muntdatabasis lees nie.
Besonderhede: %s

Wil jy herindekseer met die volgende herbegin?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Fout: Kon nie luister na inkomende verbindings nie (luister het fout %s teruggegee)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation>Kon nie op alle P2P-poorte luister nie. Misluk soos versoek deur -bindallorfail.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation>Fooi: %ld is groter as die gekonfigureerde maksimum toegelate fooi van: %ld. Om te verander, stel &apos;wallet.maxTxFee&apos;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation>Ongeldige bedrag vir -wallet.maxTxFee=&lt;bedrag&gt;: &apos;%u&apos; (moet ten minste die minrelay-fooi van %s wees om transaksies wat vashaak te voorkom)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation>Ongeldige bedrag vir -wallet.payTxFee=&lt;bedrag&gt;: &apos;%u&apos; (moet ten minste %s wees)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>Kontroleer asseblief dat jou rekenaar se datum en tyd korrek is! As jou horlosie verkeerd is, sal %s nie behoorlik werk nie.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>Snoei gekonfigureer onder die minimum van %d MiB. Gebruik asseblief &apos;n hoër nommer.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>Snoei: laaste beursie-sinchronisasie gaan verder as gesnoeide data. Jy moet -herindekseer (laai die hele blokketting weer af in die geval van gesnoeide nodus)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation>Vermindering van -net.maxConnections van %d na %d as gevolg van lêerbeskrywingbeperkings (unix) of winsocket fd_set-beperkings (windows). As jy &apos;n Windows-gebruiker is, is daar &apos;n harde boonste limiet van 1024 wat nie verander kan word deur die nodus se konfigurasie aan te pas nie.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Rescans are not possible in pruned mode. You will need to use -reindex which will download the whole blockchain again.</source>
        <translation>Herskanderings is nie moontlik in snoeimodus nie. Jy sal -reindex moet gebruik wat die hele blokketting weer sal aflaai.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>Die blok databasis bevat &apos;n blok wat blyk te wees uit die toekoms. Dit kan wees as gevolg van jou rekenaar se datum en tyd wat verkeerd gestel is. Herbou slegs die blokdatabasis as jy seker is dat jou rekenaar se datum en tyd korrek is</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>Die transaksiebedrag is te klein om te stuur nadat die fooi afgetrek is</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>Dit is &apos;n voorvrystelling-toetsbou - gebruik op eie risiko - moenie vir mynbou- of handelaartoepassings gebruik nie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>Hierdie produk sluit sagteware in wat ontwikkel is deur die OpenSSL Project vir gebruik in die OpenSSL Toolkit &lt;https://www.openssl.org/&gt; en kriptografiese sagteware geskryf deur Eric Young en UPnP sagteware geskryf deur Thomas Bernard.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation>Totale lengte van netwerkweergawestring met uacomments bygevoeg het die maksimum lengte (%i) oorskry en is afgekap. Verminder die aantal of grootte van uacomments om afkapping te vermy.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation>Transaksie het %d insette en %d uitsette. Maksimum insette toegelaat is %d en maksimum uitsette is %d</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation>Transaksie het %d insette. Maksimum insette toegelaat is %d. Probeer om insette te verminder deur &apos;n kleiner bedrag oor te dra.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>WAARSKUWING: abnormaal hoë aantal blokke gegenereer, %d blokke ontvang in die afgelope %d uur (%d verwag)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>WAARSKUWING:  toets die status van u netwerk, %d blokke ontvang in die laaste %d ure (%d verwag)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation>Beursie is nie wagwoordbeskerm nie. Jou fondse kan in gevaar wees! Gaan na &quot;Settings&quot; en kies dan &quot;Encrypt Wallet&quot; om &apos;n wagwoord te skep.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation>Waarskuwing: Kon nie ontplooiing-opstelling-CSV-lêer &apos;%s&apos; oopmaak om te lees nie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>Waarskuwing: Dit lyk of die netwerk nie heeltemal saamstem nie! Dit lyk of sommige mynwerkers probleme ondervind.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>Waarskuwing: Onbekende blokweergawes word ontgin! Dit is moontlik dat onbekende reëls van krag is</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>Waarskuwing: Wallet-lêer korrup, data gered! Oorspronklike %s gestoor as %s in %s; as jou saldo of transaksies verkeerd is, moet jy herstel vanaf &apos;n rugsteun.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>Waarskuwing: Dit lyk asof ons nie heeltemal met ons eweknieë saamstem nie! Jy sal dalk moet opgradeer, of ander nodusse sal dalk moet opgradeer.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation>Jy probeer dieselfde beursie herstel wat jy probeer vervang.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation>Jy probeer om -wallet.auto te gebruik, maar nóg -spendzeroconfchange nóg -wallet.instant is aangeskakel</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation>Jy kan nie &quot;-salvagewallet&quot; as &apos;n HD-beursie gebruik nie.

Herbegin asseblief Nexa met &quot;-usehd=0&quot;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation>Jy kan nie gratis transaksies stuur as jy &apos;n -relay.limitFreeRelay van nul opgestel het nie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain</source>
        <translation>Jy moet die databasis herbou met -reindex om terug te gaan na ongesnoeide modus. Dit sal die hele blokketting weer aflaai</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation>&quot;Herstel beursie&quot; het geslaag en &apos;n rugsteun van die vorige beursie is gestoor na: %s.


Wanneer jy op &quot;OK&quot; klik, sal Nexa afskakel om die proses te voltooi.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation>%s </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s korrup, herwinning het misluk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation>-cache.maxTxPool moet ten minste %d MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation>-xthinbloomfiltersize moet ten minste %d grepe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Activating best chain...</source>
        <translation>Aktiveer tans beste ketting …</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot downgrade wallet</source>
        <translation>Kan nie beursie afgradeer nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>Kan nie -bind adres: &apos;%s&apos; oplos nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>Kan nie -externalip adres: &apos;%s&apos; oplos nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>Kan nie -whitebind adres: &apos;%s&apos; oplos nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot write default address</source>
        <translation>Kan nie verstekadres skryf nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation>Bewerk transaksie het misluk.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>Kopiereg (C) 2015-%i Die Bitcoin Unlimited-ontwikkelaars</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Corrupted block database detected</source>
        <translation>Korrupte blokdatabasis bespeur</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation>Ontplooiing konfigurasie lêer &apos;%s&apos; nie gevind nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>Wil jy die blokdatabasis nou herbou?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Done loading</source>
        <translation>Klaar gelaai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing block database</source>
        <translation>Kon nie blokdatabasis inisialiseer nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Kon nie beursie-databasisomgewing %s inisialiseer nie!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s</source>
        <translation>Kon nie %s laai nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>Kon nie %s laai nie: beursie is beskadig</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>Kon nie %s laai nie: Beursie vereis nuwer weergawe van %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>Kon nie %s laai nie: Jy kan nie HD op &apos;n reeds bestaande HD-beursie deaktiveer nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error opening block database</source>
        <translation>Kon nie blokdatabasis oopmaak nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error</source>
        <translation>Fout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>Fout: &apos;n Noodlottige interne fout het voorgekom, sien debug.log vir besonderhede</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: Disk space is low!</source>
        <translation>Fout: Skyfspasie is min!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation>Fout: Keypool het opgeraak, bel asseblief eers sleutelpoolhervul</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Kon nie op enige poort luister nie. Gebruik -listen=0 as jy dit wil hê.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>Invoer...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>Verkeerde of geen genese-blok gevind nie. Verkeerde datadir vir netwerk?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Information</source>
        <translation>Inligting</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>Inisialisering van gesonde verstand het misluk. %s is besig om af te sluit.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation>Onvoldoende fondse vir hierdie teken. Benodig nog %d.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation>Onvoldoende fondse of fondse nie bevestig nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Ongeldige -onion-adres: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Ongeldige proxy-adres: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>Ongeldige netmasker gespesifiseer in -whitelist: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation>Keypool het opgeraak, skakel asseblief eers keypoolrefill</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation>Laai Orphanpool</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation>Laai tans TxPool</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading addresses...</source>
        <translation>Laai tans adresse...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading banlist...</source>
        <translation>Laai tans banlys …</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading block index...</source>
        <translation>Laai tans blokindeks…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>Laai tans beursie…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>Moet &apos;n poort spesifiseer met -whitebind: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Not enough file descriptors available.</source>
        <translation>Nie genoeg lêerbeskrywings beskikbaar nie.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Block database...</source>
        <translation>Maak tans blokdatabasis oop …</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation>Maak tans muntekas-databasis oop...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Description database...</source>
        <translation>Maak tans Tokenbeskrywing-databasis oop...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Mintage database...</source>
        <translation>Maak tans Token Mintage-databasis oop...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation>Maak UTXO-databasis oop...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation>Gedeeltes Kopiereg (C) 2014-%i Die Bitcoin XT-ontwikkelaars</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>Snoei kan nie met &apos;n negatiewe waarde gekonfigureer word nie.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>Snoeimodus is onversoenbaar met -txindex.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pruning blockstore...</source>
        <translation>Snoei blokwinkel...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation>Heraanvaar beursie-transaksies</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>Herskandeer tans...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing token transaction failed</source>
        <translation>Kon nie tekentransaksie onderteken nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing transaction failed</source>
        <translation>Kon nie transaksie onderteken nie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation>Begin txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>Die transaksiebedrag is te klein om die fooi te betaal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Dit is eksperimentele sagteware.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Transaksiebedrag te klein</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>Transaksiebedrae moet positief wees</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation>Transaksie het %d uitsette. Maksimum uitsette toegelaat is %d</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation>Transaksie van %d grepe is te groot. Maksimum toegelaat is %d grepe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>Transaksie te groot vir fooibeleid</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation>Skakel outokonsolideer af en probeer weer stuur.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>Kan nie aan %s op hierdie rekenaar bind nie (bind het fout %s teruggegee)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>Kan nie aan %s op hierdie rekenaar bind nie. %s loop waarskynlik reeds.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation>Kan nie RPC-dienste begin nie. Sien ontfoutingslogboek vir besonderhede.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>Onbekende netwerk gespesifiseer in -onlynet: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation>Opgradeer blokdatabasis...Dit kan &apos;n rukkie neem.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation>Opgradering van txindex databasis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation>Opgradering van txindex databasis...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>Gebruikersagent-opmerking (%s) bevat onveilige karakters.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>Verifieer tans blokke …</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>Verifieer tans beursie …</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation>Wag vir Genesis Block...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>Beursie %s is buite die datagids %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>Beursie moes herskryf word: herbegin %s om te voltooi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning</source>
        <translation>Waarskuwing</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zapping all transactions from wallet...</source>
        <translation>Zap alle transaksies uit beursie...</translation>
    </message>
</context>
</TS>
