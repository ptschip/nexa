<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="vi">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Tạo một địa chỉ mới</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>Tạo mới</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Sao chép các địa chỉ đã được chọn vào bộ nhớ tạm thời của hệ thống</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>Sao chép</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Xóa địa chỉ hiện được chọn khỏi danh sách</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Xuất dữ liệu trong tab hiện tại sang tệp</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>Xuất khẩu</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>C&amp;lose</source>
        <translation>Đóng</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+51"/>
        <source>Choose the address to send coins to</source>
        <translation>Chọn địa chỉ gửi xu tới</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Chọn địa chỉ nhận coin với</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>Chọn</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>Gửi địa chỉ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Địa chỉ nhận</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Đây là những địa chỉ Nexa của bạn để gửi thanh toán. Luôn kiểm tra số tiền và địa chỉ nhận trước khi gửi tiền.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Đây là những địa chỉ Nexa của bạn để nhận thanh toán. Nên sử dụng địa chỉ nhận mới cho mỗi giao dịch.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Copy Address</source>
        <translation>Sao chép địa chỉ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy &amp;Label</source>
        <translation>Sao chép nhãn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>Biên tập</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>Xuất danh sách địa chỉ</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>Xuất không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Đã xảy ra lỗi khi lưu danh sách địa chỉ vào %1. Vui lòng thử lại.</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-50"/>
        <source>&amp;Delete</source>
        <translation>&amp;Xóa</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-15"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Tập tin tách biệt bởi dấu phẩy (*.csv)</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>Nhãn dữ liệu</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Địa chỉ</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(chưa có nhãn)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Hộp thoại cụm mật khẩu</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Nhập cụm mật khẩu</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Cụm mật khẩu mới</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Lặp lại cụm mật khẩu mới</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+43"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Nhập cụm mật khẩu mới vào ví.&lt;br/&gt;Vui lòng sử dụng cụm mật khẩu gồm &lt;b&gt;mười ký tự ngẫu nhiên trở lên&lt;/b&gt; hoặc &lt;b&gt;tám từ trở lên&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Encrypt wallet</source>
        <translation>Mã hóa ví</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>Thao tác này cần cụm mật khẩu ví của bạn để mở khóa ví.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Mở khóa ví</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>Thao tác này cần cụm mật khẩu ví của bạn để giải mã ví.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Giải mã ví</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Thay đổi cụm mật khẩu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Nhập cụm mật khẩu cũ và cụm mật khẩu mới vào ví.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Confirm wallet encryption</source>
        <translation>Xác nhận mã hóa ví</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Cảnh báo: Nếu bạn mã hóa ví của mình và mất cụm mật khẩu, bạn sẽ &lt;b&gt;MẤT TẤT CẢ SỐ TIỀN NEXA CỦA MÌNH&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Bạn có chắc chắn muốn mã hóa ví của mình không?</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Ví được mã hóa</translation>
    </message>
    <message>
        <location line="-59"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation>%1 sẽ đóng ngay bây giờ để hoàn tất quá trình mã hóa. Hãy nhớ rằng việc mã hóa ví của bạn không thể bảo vệ hoàn toàn tiền của bạn khỏi bị đánh cắp bởi phần mềm độc hại lây nhiễm vào máy tính của bạn.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons you should no longer keep previous unencrypted backups as your funds will be at risk if someone gains access to them.</source>
        <translation>QUAN TRỌNG: Mọi bản sao lưu trước đây mà bạn đã tạo cho tệp ví của mình phải được thay thế bằng tệp ví được mã hóa mới được tạo. Vì lý do bảo mật, bạn không nên giữ các bản sao lưu không được mã hóa trước đó nữa vì tiền của bạn sẽ gặp rủi ro nếu ai đó có quyền truy cập vào chúng.</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>Mã hóa ví không thành công</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>Mã hóa ví không thành công do lỗi nội bộ. Ví của bạn không được mã hóa.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>Cụm mật khẩu được cung cấp không khớp.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>Mở khóa ví không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>Cụm mật khẩu được nhập để giải mã ví không chính xác.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>Giải mã ví không thành công</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>Cụm mật khẩu ví đã được thay đổi thành công.</translation>
    </message>
    <message>
        <location line="+50"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>Cảnh báo: Phím Caps Lock đang bật!</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation>IP/Mặt nạ mạng</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>Đại lý người dùng</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation>Bị cấm cho đến khi</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation>Lý do cấm</translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+90"/>
        <source>Wallet</source>
        <translation>Cái ví</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Node</source>
        <translation>Nút</translation>
    </message>
    <message>
        <location line="+136"/>
        <source>&amp;Overview</source>
        <translation>Tổng quan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show general overview of wallet</source>
        <translation>Hiển thị tổng quan chung về ví</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Send</source>
        <translation>Gửi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Send coins to a Nexa address</source>
        <translation>Gửi tiền đến địa chỉ Nexa</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Receive</source>
        <translation>Nhận được</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation>Yêu cầu thanh toán (tạo mã QR và %1: URI)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>Giao dịch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Duyệt lịch sử giao dịch</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Tokens</source>
        <translation>Mã thông báo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse or Send Tokens</source>
        <translation>Duyệt hoặc gửi mã thông báo</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Token History</source>
        <translation>Lịch sử mã thông báo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Token History</source>
        <translation>Duyệt lịch sử mã thông báo</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>E&amp;xit</source>
        <translation>Lối ra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Thoát khỏi ứng dụng</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation>Khoảng %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation>Hiển thị thông tin về %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>Khoảng Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Hiển thị thông tin về Qt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>Tùy chọn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation>Sửa đổi tùy chọn cấu hình cho %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Show / Hide</source>
        <translation>Hiển / Trốn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>Hiển thị hoặc ẩn Cửa sổ chính</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Unlimited...</source>
        <translation>&amp;Unlimited...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation>Sửa đổi Bitcoin Unlimited Tùy chọn</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>Mã hóa ví</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>Mã hóa các khóa riêng thuộc về ví của bạn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Backup Wallet...</source>
        <translation>Ví dự phòng</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backup wallet to another location</source>
        <translation>Sao lưu ví sang vị trí khác</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Restore Wallet...</source>
        <translation>Khôi phục ví</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Restore wallet from another location</source>
        <translation>Khôi phục ví từ một vị trí khác</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Change Passphrase...</source>
        <translation>Thay đổi cụm mật khẩu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Thay đổi cụm mật khẩu được sử dụng để mã hóa ví</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sign &amp;message...</source>
        <translation>Ký tin nhắn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Ký tin nhắn bằng địa chỉ Nexa của bạn để chứng minh bạn sở hữu chúng</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Verify message...</source>
        <translation>Xác minh tin nhắn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>Xác minh tin nhắn để đảm bảo chúng được ký bằng địa chỉ Nexa được chỉ định</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Debug window</source>
        <translation>Cửa sổ gỡ lỗi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Mở bảng điều khiển gỡ lỗi và chẩn đoán</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Sending addresses...</source>
        <translation>Gửi địa chỉ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Hiển thị danh sách địa chỉ gửi và nhãn đã sử dụng</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Receiving addresses...</source>
        <translation>Địa chỉ nhận</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Hiển thị danh sách các địa chỉ và nhãn nhận được sử dụng</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open &amp;URI...</source>
        <translation>Sao chép &amp;URI</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open a %1: URI or payment request</source>
        <translation>Mở %1: URI hoặc yêu cầu thanh toán</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Command-line options</source>
        <translation>Tùy chọn dòng lệnh</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>Hiển thị thông báo trợ giúp %1 để nhận danh sách các tùy chọn dòng lệnh Nexa có thể có</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>&amp;File</source>
        <translation>Tài liệu</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Settings</source>
        <translation>Cài đặt</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>Giúp đỡ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Thanh công cụ tab</translation>
    </message>
    <message>
        <location line="+111"/>
        <source>%1 client</source>
        <translation>%1 khách hàng</translation>
    </message>
    <message numerus="yes">
        <location line="+193"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n kết nối đang hoạt động với mạng Nexa</numerusform>
        </translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Synchronizing with network...</source>
        <translation>Đang đồng bộ hóa với mạng...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Importing blocks from disk...</source>
        <translation>Đang nhập khối từ đĩa...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>Lập chỉ mục lại các khối trên đĩa...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No block source available...</source>
        <translation>Không có nguồn khối nào...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>Đã xử lý %n khối lịch sử giao dịch.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Up to date</source>
        <translation>Cập nhật</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1 behind</source>
        <translation>%1 phía sau</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Catching up...</source>
        <translation>Bắt kịp...</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>Khối nhận được lần cuối đã được tạo %1 trước đây.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Các giao dịch sau đó sẽ không được hiển thị.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error</source>
        <translation>Lỗi</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Cảnh báo</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>Thông tin</translation>
    </message>
    <message>
        <location line="+103"/>
        <source>Date: %1
</source>
        <translation>Ngày: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Số lượng: %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Loại: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Nhãn: %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Địa chỉ: %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Đã gửi giao dịch</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Giao dịch đến</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation>Việc tạo khóa HD được &lt;b&gt;bật&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation>Việc tạo khóa HD bị &lt;b&gt;tắt&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>Ví được &lt;b&gt;mã hóa&lt;/b&gt; và hiện &lt;b&gt;đã được mở khóa&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>Ví được &lt;b&gt;mã hóa&lt;/b&gt; và hiện &lt;b&gt;bị khóa&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>Lựa chọn tiền xu</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>Số lượng:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>Byte:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>Số lượng:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>Sự ưu tiên:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>Phí:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Bụi:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Sau khi tính phí:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Thay đổi:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>(un)chọn đầu vào tối đa</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>Chế độ cây</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>Chế độ danh sách</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>Số lượng</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>Đã nhận được nhãn</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>Đã nhận được với địa chỉ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Ngày</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>Xác nhận</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>Đã xác nhận</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>Sự ưu tiên</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>Sao chép địa chỉ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Sao chép nhãn</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>Sao chép số tiền</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>Sao chép id giao dịch</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>Khóa chưa dùng</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>Mở khóa chưa sử dụng</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>Sao chép số lượng</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Phí sao chép</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Sao chép sau phí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Sao chép byte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Ưu tiên sao chép</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Sao chép bụi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Sao chép thay đổi</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>cao nhất</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>cao hơn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>cao</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>Trung bình khá</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>trung bình</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>thấp-trung bình</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>thấp</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>thấp hơn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>thấp nhất</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 bị khóa)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>không có</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>yes</source>
        <translation>Đúng</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>không</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>Nhãn này chuyển sang màu đỏ nếu kích thước giao dịch lớn hơn 1000 byte.</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>Điều này có nghĩa là phải trả phí ít nhất %1 cho mỗi kB.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>Có thể thay đổi +/- 1 byte cho mỗi đầu vào.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>Các giao dịch có mức độ ưu tiên cao hơn sẽ có nhiều khả năng được đưa vào khối hơn.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>Nhãn này chuyển sang màu đỏ nếu mức độ ưu tiên nhỏ hơn &quot;trung bình&quot;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>Nhãn này chuyển sang màu đỏ nếu bất kỳ người nhận nào nhận được số tiền nhỏ hơn %1.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>Có thể thay đổi +/- %1 satoshi cho mỗi đầu vào.</translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(chưa có nhãn)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>thay đổi từ %1 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(thay đổi)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>Sửa địa chỉ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>Nhãn dữ liệu</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>Nhãn được liên kết với mục danh sách địa chỉ này</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Address</source>
        <translation>Địa chỉ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>Địa chỉ được liên kết với mục danh sách địa chỉ này. Điều này chỉ có thể được sửa đổi để gửi địa chỉ.</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>Địa chỉ nhận mới</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>Địa chỉ gửi mới</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>Chỉnh sửa địa chỉ nhận</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>Chỉnh sửa địa chỉ gửi</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>Địa chỉ đã nhập &quot;%1&quot; không phải là địa chỉ Nexa hợp lệ.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>Địa chỉ đã nhập &quot;%1&quot; đã có trong sổ địa chỉ.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not unlock wallet.</source>
        <translation>Không thể mở khóa ví.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>Tạo khóa mới không thành công.</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>Một thư mục dữ liệu mới sẽ được tạo.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>tên</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>Thư mục đã tồn tại. Thêm %1 nếu bạn định tạo một thư mục mới tại đây.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>Đường dẫn đã tồn tại và không phải là một thư mục.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>Không thể tạo thư mục dữ liệu ở đây.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>phiên bản</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1-chút)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation>Khoảng %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>Tùy chọn dòng lệnh</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>Cách sử dụng:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>tùy chọn dòng lệnh</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>Chào mừng</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation>Chào mừng đến với %1.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation>Vì đây là lần đầu tiên chương trình được khởi chạy nên bạn có thể chọn nơi %1 sẽ lưu trữ dữ liệu của nó.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation>%1 sẽ tải xuống và lưu trữ bản sao của chuỗi khối Nexa. Ít nhất %2GB dữ liệu sẽ được lưu trữ trong thư mục này và nó sẽ tăng lên theo thời gian. Ví cũng sẽ được lưu trữ trong thư mục này.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>Sử dụng thư mục dữ liệu mặc định</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>Sử dụng một thư mục dữ liệu tùy chỉnh:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>Lỗi: Không thể tạo thư mục dữ liệu đã chỉ định &quot;%1&quot;.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>Lỗi</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n GB dung lượng trống hiện có</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(cần %n GB)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Thông số định hình lưu lượng truy cập ngược dòng không được để trống</translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation>Hình thức</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation>Thông tin hiển thị có thể đã lỗi thời. Ví của bạn sẽ tự động đồng bộ hóa với mạng Nexa sau khi kết nối được thiết lập nhưng quá trình này vẫn chưa hoàn tất. Điều này có nghĩa là các giao dịch gần đây sẽ không hiển thị và số dư sẽ không được cập nhật cho đến khi quá trình này hoàn tất.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation>Việc tiêu xu có thể không thực hiện được trong giai đoạn đó!</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation>Số khối còn lại</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation>không xác định...</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation>Thời gian khối cuối cùng</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation>Tiến triển</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation>Tăng tiến độ mỗi giờ</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation>tính toán...</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation>Thời gian ước tính còn lại cho đến khi được đồng bộ hóa</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation>Trốn</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation>Không xác định. Đang lập chỉ mục lại (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation>Không xác định. Đang lập chỉ mục lại...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation>Không xác định. Đang đồng bộ hóa tiêu đề (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation>Không xác định. Đang đồng bộ hóa tiêu đề...</translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>URI mở</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>Mở yêu cầu thanh toán từ URI hoặc tệp</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>Chọn file yêu cầu thanh toán</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>Chọn file yêu cầu thanh toán để mở</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Tùy chọn</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>Chủ yếu</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>Tự động khởi động %1 sau khi đăng nhập vào hệ thống.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>Bắt đầu %1 khi đăng nhập hệ thống</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tự động bắt đầu lập chỉ mục lại toàn bộ cơ sở dữ liệu một lần duy nhất trong lần khởi động tiếp theo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation>Lập chỉ mục lại khi khởi động</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Số lượng tập lệnh và chuỗi xác minh</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = tự động, &lt;0 = để trống nhiều lõi đó)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a full blockchain resynchronization on the next startup (one time only).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tự động bắt đầu quá trình đồng bộ lại toàn bộ chuỗi khối vào lần khởi động tiếp theo (chỉ một lần).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Resynchronize block data on startup</source>
        <translation>Đồng bộ lại dữ liệu khối khi khởi động</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>W&amp;allet</source>
        <translation>Cái ví</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Chuyên gia</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Có hiển thị tính năng kiểm soát xu hay không.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable coin &amp;control features</source>
        <translation>Kích hoạt tính năng kiểm soát và xu</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>Nếu bạn vô hiệu hóa chi tiêu của thay đổi chưa được xác nhận, thay đổi từ một giao dịch sẽ không thể được sử dụng cho đến khi giao dịch đó có ít nhất một xác nhận. Điều này cũng ảnh hưởng đến cách tính số dư của bạn.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>Chi tiêu thay đổi chưa được xác nhận</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Khi bật Giao dịch tức thì, bạn có thể chi tiêu ngay lập tức các giao dịch chưa được xác nhận.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation>Giao dịch tức thời</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Khi tạo và gửi giao dịch, tính năng tự động hợp nhất, nếu được yêu cầu, sẽ tự động tạo một chuỗi giao dịch có đầu vào không lớn hơn giới hạn đầu vào đồng thuận.&lt;/p&gt;&lt;/body &gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation>Tự động hợp nhất</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>&amp;Rescan wallet on startup</source>
        <translation>Quét lại ví khi khởi động</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Network</source>
        <translation>Mạng</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Tự động mở cổng máy khách Nexa trên bộ định tuyến. Điều này chỉ hoạt động khi bộ định tuyến của bạn hỗ trợ UPnP và nó được bật.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Cổng bản đồ sử dụng UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation>Chấp nhận kết nối từ bên ngoài.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Allow incoming connections</source>
        <translation>Cho phép kết nối đến</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Kết nối với mạng Nexa thông qua proxy SOCKS5.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>Kết nối thông qua proxy SOCKS5 (proxy mặc định):</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>IP ủy quyền:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>Địa chỉ IP của proxy (ví dụ: IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="-180"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;Hải cảng:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Cổng proxy (ví dụ: 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Được sử dụng để tiếp cận các đồng nghiệp thông qua:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation>Hiển thị liệu proxy SOCKS5 mặc định được cung cấp có được sử dụng để tiếp cận các thiết bị ngang hàng thông qua loại mạng này hay không.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Kết nối với mạng Nexa thông qua proxy SOCKS5 riêng cho các dịch vụ ẩn Tor.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Sử dụng proxy SOCKS5 riêng biệt để tiếp cận các đồng nghiệp thông qua các dịch vụ ẩn Tor:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>Cửa sổ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Chỉ hiển thị biểu tượng khay sau khi thu nhỏ cửa sổ.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>Thu nhỏ vào khay thay vì thanh tác vụ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>Thu nhỏ thay vì thoát ứng dụng khi đóng cửa sổ. Khi tùy chọn này được bật, ứng dụng sẽ chỉ bị đóng sau khi chọn Thoát trong menu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>M&amp;inimize on close</source>
        <translation>Giảm thiểu khi đóng</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>Trưng bày</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>Giao diện người dùng và ngôn ngữ:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>Ngôn ngữ giao diện người dùng có thể được đặt ở đây. Cài đặt này sẽ có hiệu lực sau khi khởi động lại %1.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>Đơn vị hiển thị số tiền bằng:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Chọn đơn vị phân chia mặc định hiển thị trong giao diện và khi gửi xu.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>URL của bên thứ ba (ví dụ: trình khám phá khối) xuất hiện trong tab giao dịch dưới dạng mục menu ngữ cảnh. %s trong URL được thay thế bằng hàm băm giao dịch. Nhiều URL được phân tách bằng thanh dọc |.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>URL giao dịch của bên thứ ba</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Các tùy chọn dòng lệnh hiện hoạt ghi đè các tùy chọn trên:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>Đặt lại tất cả các tùy chọn máy khách về mặc định.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>Đặt lại tùy chọn</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>ĐƯỢC RỒI</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>Hủy bỏ</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+124"/>
        <source>default</source>
        <translation>mặc định</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>none</source>
        <translation>không có</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Confirm options reset</source>
        <translation>Xác nhận đặt lại tùy chọn</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>Yêu cầu khởi động lại máy khách để kích hoạt các thay đổi.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Khách hàng sẽ bị tắt. Bạn có muốn tiếp tục?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Thay đổi này sẽ yêu cầu khởi động lại máy khách.</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>Địa chỉ proxy được cung cấp không hợp lệ.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Hình thức</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Balances</source>
        <translation>Cân bằng</translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>Thông tin hiển thị có thể đã lỗi thời. Ví của bạn sẽ tự động đồng bộ hóa với mạng Nexa sau khi kết nối được thiết lập nhưng quá trình này vẫn chưa hoàn tất.</translation>
    </message>
    <message>
        <location line="-333"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Giao dịch chưa được xác nhận đến địa chỉ chỉ xem</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Tổng số giao dịch chưa được xác nhận và chưa được tính vào số dư có thể chi tiêu</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Số dư đã khai thác trong các địa chỉ chỉ xem chưa hết hạn</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Total:</source>
        <translation>Tổng cộng:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Số dư đã khai thác chưa đáo hạn</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Immature:</source>
        <translation>Chưa trưởng thành:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current total balance</source>
        <translation>Tổng số dư hiện tại của bạn</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Tổng số dư hiện tại trong các địa chỉ chỉ xem</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Watch-only:</source>
        <translation>Chỉ xem</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>Có sẵn:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>Số dư có thể chi tiêu hiện tại của bạn</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>Số dư hiện tại của bạn trong các địa chỉ chỉ xem</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Pending:</source>
        <translation>Chưa giải quyết:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Spendable:</source>
        <translation>Có thể chi tiêu:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Giao dịch gần đây</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+375"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>Lỗi yêu cầu thanh toán</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation></translation>
    </message>
    <message>
        <location line="+95"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation></translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid payment address %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+65"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation></translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Payment request expired.</source>
        <translation>Yêu cầu thanh toán đã hết hạn.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment request is not initialized.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation></translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Refund from %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Network request error</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment acknowledged</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+106"/>
        <source>Node/Service</source>
        <translation>Nút/Dịch vụ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>Đại lý người dùng</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Thời gian Ping</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Số lượng</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation>Nhập địa chỉ NEXA (ví dụ: %1)</translation>
    </message>
    <message>
        <location line="+832"/>
        <source>%1 d</source>
        <translation>%1 ngày</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 giờ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 phút</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 giây</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Không có</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>không áp dụng</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 mili giây</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation>
            <numerusform>%n giây</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation>
            <numerusform>%n phút</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%1 giờ</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n ngày</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n tuần</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 và 2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n năm</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>Lưu hình ảnh...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>Sao chép hình ảnh</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>Lưu mã QR</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>Hình ảnh PNG (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+14"/>
        <source>Debug window</source>
        <translation>Cửa sổ gỡ lỗi</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Information</source>
        <translation>thông tin</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Client version</source>
        <translation>Phiên bản máy khách</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>Đại lý người dùng</translation>
    </message>
    <message>
        <location line="-2380"/>
        <source>Using BerkeleyDB version</source>
        <translation>Sử dụng phiên bản BerkeleyDB</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Datadir</source>
        <translation>Datadir</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Startup time</source>
        <translation>Thời gian khởi động</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name</source>
        <translation>Tên</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>Số lượng kết nối</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Current number of blocks</source>
        <translation>Số khối hiện tại</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block time (time since)</source>
        <translation>Thời gian khối cuối cùng (thời gian kể từ đó)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation>Thời gian khối cuối cùng</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation>Giao dịch trong nhóm Tx</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation>Giao dịch trong nhóm mồ côi</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation>Tin nhắn trong nhóm CAPD</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation>Nhóm Tx - cách sử dụng</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation>Nhóm Tx - txns mỗi giây</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation>XThin (Tổng cộng)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation>XThin (Trung bình 24 giờ)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation>Compact (Tổng cộng)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation>Compact (Trung bình 24 giờ)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation>Graphene (Tổng cộng)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation>Graphene (Trung bình 24 giờ)</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Debug log file</source>
        <translation>Tệp nhật ký gỡ lỗi</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>Mở tệp nhật ký gỡ lỗi %1 từ thư mục dữ liệu hiện tại. Việc này có thể mất vài giây đối với các tệp nhật ký lớn.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Open</source>
        <translation>Mở</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>không áp dụng</translation>
    </message>
    <message>
        <location line="-2407"/>
        <source>Block Propagation</source>
        <translation>Khối lan truyền</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation>Nhóm giao dịch</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Block chain</source>
        <translation>Chuỗi khối</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Network</source>
        <translation>Mạng</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>General</source>
        <translation>Tổng quan</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Console</source>
        <translation>Bảng điều khiển</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Decrease font size</source>
        <translation>Giảm kích thước phông chữ</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>Tăng kích thước phông chữ</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Clear console</source>
        <translation>Giao diện điều khiển rõ ràng</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>&amp;Network Traffic</source>
        <translation>Lưu lượng mạng</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>Xóa</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Tổng số</translation>
    </message>
    <message>
        <location line="+64"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Đã nhận</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Đã gửi</translation>
    </message>
    <message>
        <location line="-1550"/>
        <source>&amp;Transaction Rate</source>
        <translation>Tỷ giá giao dịch</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation>Tốc độ tức thời (1 giây)</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation>Đỉnh cao</translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation>Thời gian chạy</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation>24 giờ</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation>Hiển thị</translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation>Trung bình</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation>Tốc độ làm mịn (60s)</translation>
    </message>
    <message>
        <location line="+539"/>
        <source>&amp;Peers</source>
        <translation>ngang hàng</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Đồng nghiệp bị cấm</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+856"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Chọn một đồng nghiệp để xem thông tin chi tiết.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>Danh sách trắng</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>Phương hướng</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Phiên bản</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Services</source>
        <translation>Dịch vụ</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Starting Block</source>
        <translation>Khối bắt đầu</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>Tiêu đề được đồng bộ hóa</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>Khối được đồng bộ hóa</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Ban Score</source>
        <translation>Cấm điểm</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Thời gian kết nối</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Lần gửi cuối cùng</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Nhận lần cuối</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Thời gian Ping</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>Thời lượng của một ping hiện đang tồn tại.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Ping chờ</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Bù đắp thời gian</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-736"/>
        <source>&amp;Disconnect Node</source>
        <translation>Ngắt kết nối nút</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>Cấm nút cho</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 giờ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 ngày</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 tuần</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 năm</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>Bỏ lệnh cấm Nút</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>Chào mừng bạn đến với bảng điều khiển RPC %1.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>Sử dụng mũi tên lên và xuống để điều hướng lịch sử và &lt;b&gt;Ctrl-L&lt;/b&gt; để xóa màn hình.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>In:</source>
        <translation>Trong:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>Outy:</translation>
    </message>
    <message>
        <location line="+90"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation>Tàn tật</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(id nút: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>qua %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>không bao giờ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>Trong nước</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>Đi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Đúng</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Không</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>Không xác định</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+45"/>
        <source>&amp;Request payment</source>
        <translation>Yêu cầu thanh toán</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Clear all fields of the form.</source>
        <translation>Xóa tất cả các trường của biểu mẫu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Xóa</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+7"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>Một số tiền tùy chọn để yêu cầu. Để trống hoặc bằng 0 để không yêu cầu số tiền cụ thể.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Amount:</source>
        <translation>Số lượng:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+56"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>Nhãn tùy chọn để liên kết với địa chỉ nhận mới.</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>&amp;Label:</source>
        <translation>Nhãn dữ liệu</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Sử dụng lại một trong những địa chỉ nhận đã sử dụng trước đó. Việc sử dụng lại địa chỉ có vấn đề về bảo mật và quyền riêng tư. Không sử dụng điều này trừ khi tạo lại yêu cầu thanh toán được thực hiện trước đó.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>Sử dụng lại địa chỉ nhận hiện có (không khuyến khích)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Sử dụng biểu mẫu này để yêu cầu thanh toán. Tất cả các trường đều &lt;b&gt;tùy chọn&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+23"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>Một thông báo tùy chọn để đính kèm với yêu cầu thanh toán, thông báo này sẽ được hiển thị khi yêu cầu được mở. Lưu ý: Tin nhắn sẽ không được gửi cùng với khoản thanh toán qua mạng Nexa.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>&amp;Message:</source>
        <translation>Tin nhắn</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Coin freezing locks coins to make them temporarily unspendable. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Việc đóng băng tiền xu sẽ khóa tiền xu khiến chúng tạm thời không thể chi tiêu được. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Coin &amp;Freeze</source>
        <translation>Đồng xu và đóng băng</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Requested payments history</source>
        <translation>Lịch sử thanh toán được yêu cầu</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Hiển thị yêu cầu đã chọn (thực hiện tương tự như nhấp đúp vào mục nhập)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Trình diễn</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Xóa các mục đã chọn khỏi danh sách</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Di dời</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+53"/>
        <source>Copy URI</source>
        <translation>Sao chép URI</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Sao chép nhãn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Sao chép tin nhắn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Sao chép số tiền</translation>
    </message>
</context>
<context>
    <name>ReceiveFreezeDialog</name>
    <message>
        <location filename="../forms/receivefreezedialog.ui" line="+14"/>
        <source>Coin Freeze</source>
        <translation></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>WARNING! Freezing coins means they will be UNSPENDABLE until the release date or block specified below.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Freeze until block :</source>
        <translation></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify the block when coins are released from the freeze. Coins are UNSPENDABLE until the freeze block.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+99"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location line="-57"/>
        <source>OR</source>
        <translation></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Freeze until date and time :</source>
        <translation></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Specify the future date and time when coins are released from the freeze. Coins are UNSPENDABLE until after the freeze date and time.</source>
        <translation></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>&amp;Reset</source>
        <translation></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+R</source>
        <translation></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&amp;OK</source>
        <translation>ĐƯỢC RỒI</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>Mã QR</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>Sao chép &amp;URI</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>Sao chép địa chỉ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>Lưu hình ảnh...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>Yêu cầu thanh toán tới %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Thông tin thanh toán</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Địa chỉ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>Số lượng</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Nhãn dữ liệu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>Tin nhắn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Freeze until</source>
        <translation>Đóng băng cho đến khi</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>Kết quả URI quá dài, hãy thử giảm bớt văn bản cho nhãn/tin nhắn.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Lỗi mã hóa URI thành QR Code.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Label</source>
        <translation>Nhãn dữ liệu</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Date</source>
        <translation>Ngày</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Tin nhắn</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>(no message)</source>
        <translation>(không có tin nhắn)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(không có số lượng)</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Amount</source>
        <translation>Số lượng</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(chưa có nhãn)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+633"/>
        <source>Send Coins</source>
        <translation></translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Tính năng kiểm soát tiền xu</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Select specific coins you want to use in this transaction</source>
        <translation>Chọn đồng tiền cụ thể mà bạn muốn sử dụng trong giao dịch này</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Inputs...</source>
        <translation>Đầu vào...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>được chọn tự động</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Không đủ tiền!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Số lượng:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Byte:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Số lượng:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>Sự ưu tiên:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Phí:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Bụi:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Sau khi tính phí:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Thay đổi:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>Nếu điều này được kích hoạt nhưng địa chỉ thay đổi trống hoặc không hợp lệ, thay đổi sẽ được gửi đến địa chỉ mới được tạo.</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+16"/>
        <source>Custom change address</source>
        <translation>Địa chỉ thay đổi tùy chỉnh</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Transaction Fee:</source>
        <translation>Phí giao dịch:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose your transaction fee.</source>
        <translation>Chọn phí giao dịch của bạn.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose...</source>
        <translation>Chọn...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>thu gọn cài đặt phí</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide</source>
        <translation>Trốn</translation>
    </message>
    <message>
        <location line="+48"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>Nếu phí tùy chỉnh được đặt thành 1000 satoshi và giao dịch chỉ có 250 byte thì &quot;mỗi kilobyte&quot; chỉ trả phí 250 satoshi, trong khi &quot;tổng ít nhất&quot; trả 1000 satoshi. Đối với các giao dịch lớn hơn một kilobyte, cả hai đều thanh toán bằng kilobyte.</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>per kilobyte</source>
        <translation>mỗi kilobyte</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>total at least</source>
        <translation>tổng cộng ít nhất</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Custom fee amount</source>
        <translation>Số tiền phí tùy chỉnh</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>Chỉ trả mức phí tối thiểu cũng được miễn là khối lượng giao dịch ít hơn không gian trong khối. Nhưng hãy lưu ý rằng điều này có thể dẫn đến giao dịch không bao giờ được xác nhận khi có nhiều nhu cầu về giao dịch nexa hơn mức mạng có thể xử lý.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(đọc chú giải công cụ)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Use the recommended fee amount.  You can select a faster or slower confirmation time by moving the &quot;Confirmation Time&quot; slider.</source>
        <translation>Sử dụng số tiền phí được đề xuất. Bạn có thể chọn thời gian xác nhận nhanh hơn hoặc chậm hơn bằng cách di chuyển thanh trượt &quot;Thời gian xác nhận&quot;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Recommended:</source>
        <translation>Khuyến khích:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Choose a custom fee amount</source>
        <translation>Chọn số tiền phí tùy chỉnh</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom:</source>
        <translation>Phong tục:</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(Phí thông minh chưa được khởi tạo. Quá trình này thường mất vài khối...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+33"/>
        <source>Transaction confirmation time</source>
        <translation>Thời gian xác nhận giao dịch</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Confirmation time:</source>
        <translation>Thời gian xác nhận:</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>normal</source>
        <translation>Bình thường</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>nhanh</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>Gửi dưới dạng giao dịch không tính phí nếu có thể</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(việc xác nhận có thể mất nhiều thời gian hơn)</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Confirm the send action</source>
        <translation>Xác nhận hành động gửi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>Gửi</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Clear all fields of the form.</source>
        <translation>Xóa tất cả các trường của biểu mẫu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear &amp;All</source>
        <translation>xóa tất cả</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Send to multiple recipients at once</source>
        <translation>Gửi cho nhiều người nhận cùng một lúc</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Thêm người nhận</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Current Balance</source>
        <translation>Số dư hiện tại</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Balance:</source>
        <translation>Cân bằng:</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-565"/>
        <source>Copy quantity</source>
        <translation>Sao chép số lượng</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Sao chép số tiền</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>Phí sao chép</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Sao chép sau phí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Sao chép byte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Ưu tiên sao chép</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Sao chép bụi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Sao chép thay đổi</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Nhãn công khai:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 đến 2</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING!!! DESTINATION IS A FREEZE ADDRESS&lt;br&gt;UNSPENDABLE UNTIL&lt;/b&gt; %1 &lt;br&gt;*************************************************&lt;br&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;b&gt;CẢNH BÁO!!! ĐIỂM ĐẾN LÀ ĐỊA CHỈ ĐÓNG BĂNG&lt;br&gt;KHÔNG THỂ CHIA SẺ ĐẾN&lt;/b&gt; %1 &lt;br&gt;********************************* *******************&lt;br&gt;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Are you sure you want to send?</source>
        <translation>Bạn có chắc chắn muốn gửi không?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>được thêm vào dưới dạng phí giao dịch</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Total Amount %1</source>
        <translation>Tổng số tiền %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>hoặc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Confirm send coins</source>
        <translation>Xác nhận gửi xu</translation>
    </message>
    <message>
        <location line="+185"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>Địa chỉ người nhận không hợp lệ. Vui lòng kiểm tra lại.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>Số tiền phải trả phải lớn hơn 0.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>Số tiền vượt quá số dư của bạn.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>Tổng số vượt quá số dư của bạn khi bao gồm phí giao dịch %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>Đã tìm thấy địa chỉ trùng lặp: mỗi địa chỉ chỉ nên được sử dụng một lần.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction creation failed!</source>
        <translation>Tạo giao dịch không thành công!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>Giao dịch đã bị từ chối! Điều này có thể xảy ra nếu một số xu trong ví của bạn đã được sử dụng, chẳng hạn như nếu bạn sử dụng bản sao của wallet.dat và số xu đã được tiêu trong bản sao nhưng không được đánh dấu là đã chi tiêu ở đây.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>Một khoản phí cao hơn %1 được coi là một khoản phí cao vô lý.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>Yêu cầu thanh toán đã hết hạn.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation>Nhãn công khai vượt quá giới hạn </translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Pay only the required fee of %1</source>
        <translation>Chỉ thanh toán khoản phí bắt buộc là %1</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Cảnh báo: Địa chỉ Nexa không hợp lệ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Warning: Unknown change address</source>
        <translation>Cảnh báo: Địa chỉ thay đổi không xác định</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirm custom change address</source>
        <translation>Xác nhận địa chỉ thay đổi tùy chỉnh</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation>Địa chỉ bạn đã chọn để thay đổi không phải là một phần của ví này. Bất kỳ hoặc tất cả số tiền trong ví của bạn đều có thể được gửi đến địa chỉ này. Bạn có chắc không?</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(chưa có nhãn)</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+44"/>
        <source>Amount to send</source>
        <translation>Số tiền cần gửi</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>Lệ phí sẽ được khấu trừ vào số tiền được gửi. Người nhận sẽ nhận được ít xu hơn số tiền bạn nhập vào trường số tiền. Nếu chọn nhiều người nhận thì phí sẽ được chia đều.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>Trừ phí từ số tiền</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Private Description:</source>
        <translation>Mô tả riêng:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation>Nhãn hiệu riêng:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+677"/>
        <location line="+560"/>
        <source>A&amp;mount:</source>
        <translation>Số lượng:</translation>
    </message>
    <message>
        <location line="-1224"/>
        <source>Pay &amp;To:</source>
        <translation>Trả cho:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Địa chỉ Nexa để gửi thanh toán tới</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>Chọn địa chỉ đã sử dụng trước đó</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Dán địa chỉ từ clipboard</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+553"/>
        <location line="+560"/>
        <source>Remove this entry</source>
        <translation>Xóa mục này</translation>
    </message>
    <message>
        <location line="-1091"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Một thông báo được đính kèm với đồng xu: URI sẽ được lưu trữ cùng với giao dịch để bạn tham khảo. Lưu ý: Tin nhắn này sẽ không được gửi qua mạng Nexa.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation>Nhập nhãn riêng cho địa chỉ này để thêm nó vào danh sách các địa chỉ được sử dụng</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation>Nhập nhãn công khai cho giao dịch này</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation>Nhãn công cộng:</translation>
    </message>
    <message>
        <location line="+466"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Đây là một yêu cầu thanh toán không được xác thực.</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+556"/>
        <source>Pay To:</source>
        <translation>Trả cho:</translation>
    </message>
    <message>
        <location line="-522"/>
        <location line="+560"/>
        <source>Memo:</source>
        <translation>Bản ghi nhớ:</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>This is an authenticated payment request.</source>
        <translation>Đây là một yêu cầu thanh toán được xác thực.</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Một thông báo được đính kèm với URI %1 sẽ được lưu trữ cùng với giao dịch để bạn tham khảo. Lưu ý: Tin nhắn này sẽ không được gửi qua mạng Nexa.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation>Nhập nhãn riêng cho địa chỉ này để thêm nó vào sổ địa chỉ của bạn</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>%1 đang tắt...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>Không tắt máy tính cho đến khi cửa sổ này biến mất.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Chữ ký - Ký / Xác minh tin nhắn</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>Ký tin nhắn</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Bạn có thể ký tin nhắn/thỏa thuận với địa chỉ của mình để chứng minh rằng bạn có thể nhận được tiền gửi cho họ. Hãy cẩn thận để không ký bất cứ điều gì mơ hồ hoặc ngẫu nhiên, vì các cuộc tấn công lừa đảo có thể cố lừa bạn ký tên vào danh tính của bạn cho họ. Chỉ ký các tuyên bố đầy đủ chi tiết mà bạn đồng ý.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>Địa chỉ Nexa để ký tin nhắn</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Chọn địa chỉ đã sử dụng trước đó</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Dán địa chỉ từ clipboard</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Nhập tin nhắn bạn muốn ký vào đây</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Chữ ký</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Sao chép chữ ký hiện tại vào bảng tạm hệ thống</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Ký vào tin nhắn để chứng minh bạn sở hữu địa chỉ Nexa này</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>Ký tin nhắn</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Đặt lại tất cả các trường thông báo ký hiệu</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>xóa tất cả</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>Xác minh tin nhắn</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>Nhập địa chỉ, tin nhắn của người nhận (đảm bảo bạn sao chép chính xác ngắt dòng, dấu cách, tab, v.v.) và chữ ký bên dưới để xác minh tin nhắn. Hãy cẩn thận để không đọc nhiều hơn vào chữ ký so với nội dung trong chính tin nhắn đã ký để tránh bị lừa bởi một cuộc tấn công trung gian. Lưu ý rằng điều này chỉ chứng minh bên ký nhận nhận được bằng địa chỉ, không thể chứng minh được người gửi của bất kỳ giao dịch nào!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>Địa chỉ Nexa mà tin nhắn được ký</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Xác minh tin nhắn để đảm bảo nó được ký bằng địa chỉ Nexa được chỉ định</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>Xác minh tin nhắn</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Đặt lại tất cả các trường tin nhắn xác minh</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>Nhấp vào &quot;Ký tin nhắn&quot; để tạo chữ ký</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>Mở khóa ví đã bị hủy.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Tin nhắn đã được ký.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Đã xác minh tin nhắn.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TokenDescDialog</name>
    <message>
        <location filename="../forms/tokendescdialog.ui" line="+17"/>
        <source>Token details</source>
        <translation>Chi tiết mã thông báo</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This pane shows a detailed description of the token&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Khung này hiển thị mô tả chi tiết về mã thông báo&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>TokenHistoryView</name>
    <message>
        <location filename="../tokenhistoryview.cpp" line="+78"/>
        <location line="+19"/>
        <source>All</source>
        <translation></translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Hôm nay</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Tuần này</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Tháng này</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Tháng trước</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Năm nay</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Phạm vi...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Đã nhận được với</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent</source>
        <translation>Đã gửi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Cho chính bạn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint</source>
        <translation>Xu bạc hà</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Khác</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter token ID to search</source>
        <translation>Nhập mã thông báo ID để tìm kiếm</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Số tiền tối thiểu</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy token id</source>
        <translation>Sao chép id mã thông báo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Sao chép số tiền</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>Sao chép idem giao dịch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>Sao chép giao dịch thô</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Hiển thị chi tiết giao dịch</translation>
    </message>
    <message>
        <location line="+162"/>
        <source>Export Token History</source>
        <translation>Xuất lịch sử mã thông báo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Tập tin tách biệt bởi dấu phẩy (*.csv)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Đã xác nhận</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>Chỉ xem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Ngày</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Loại</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Nhãn dữ liệu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Địa chỉ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction ID</source>
        <translation>ID giao dịch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token ID</source>
        <translation>ID mã thông báo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token Amount</source>
        <translation>Số lượng mã thông báo</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>Xuất không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the token history to %1.</source>
        <translation>Đã xảy ra lỗi khi cố lưu lịch sử mã thông báo vào %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Xuất thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The token history was successfully saved to %1.</source>
        <translation>Lịch sử mã thông báo đã được lưu thành công vào %1.</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Range:</source>
        <translation>Phạm vi:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>cho</translation>
    </message>
</context>
<context>
    <name>TokenTableModel</name>
    <message>
        <location filename="../tokentablemodel.cpp" line="+244"/>
        <source>Date</source>
        <translation>Ngày</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Loại</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Token ID</source>
        <translation>ID mã thông báo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Net Amount</source>
        <translation>Số lượng tịnh</translation>
    </message>
    <message numerus="yes">
        <location line="+49"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Mở thêm %n khối nữa</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>Mở cho đến %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Ngoại tuyến</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Chưa được xác nhận</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Đang xác nhận (%1 trong số %2 xác nhận được đề xuất)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Đã xác nhận (%1 xác nhận)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>Mâu thuẫn</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Chi tiêu gấp đôi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Bị bỏ rơi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Chưa trưởng thành (xác nhận %1, sẽ có sau %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Khối này không được bất kỳ nút nào khác nhận được và có thể sẽ không được chấp nhận!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Đã tạo nhưng không được chấp nhận</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Đã nhận được với</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Nhận được tư</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent</source>
        <translation>Đã gửi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Thanh toán cho chính bạn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mint</source>
        <translation>Xu bạc hà</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Melt</source>
        <translation>Tan chảy</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Nhãn công cộng</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Khác</translation>
    </message>
    <message>
        <location line="+323"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Trạng thái giao dịch. Di chuột qua trường này để hiển thị số lượng xác nhận.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Ngày và giờ giao dịch được nhận.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Loại giao dịch.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Địa chỉ chỉ xem có tham gia vào giao dịch này hay không.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Mục đích/mục đích của giao dịch do người dùng xác định.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Số tiền được loại bỏ hoặc thêm vào số dư.</translation>
    </message>
</context>
<context>
    <name>TokensViewDialog</name>
    <message>
        <location filename="../forms/tokensviewdialog.ui" line="+14"/>
        <source>Tokens</source>
        <translation>Mã thông báo</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Amount: </source>
        <translation>Số lượng: </translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of tokens to send&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Số lượng token cần gửi&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>  Pay To: </source>
        <translation>Trả cho:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Địa chỉ Nexa để gửi thanh toán tới</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation>Chọn địa chỉ đã sử dụng trước đó</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Dán địa chỉ từ clipboard</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove this entry</source>
        <translation>Xóa mục này</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Send tokens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Gửi mã thông báo&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Send</source>
        <translation>Gửi</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Token ID</source>
        <translation>ID mã thông báo</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The unique token identifier</source>
        <translation>Mã định danh mã thông báo duy nhất</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>The token group identifier</source>
        <translation>Mã định danh nhóm mã thông báo</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Name</source>
        <translation>Tên</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The name of the token</source>
        <translation>Tên của mã thông báo</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ticker</source>
        <translation>Biểu tượng</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The token ticker symbol</source>
        <translation>Biểu tượng mã thông báo</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Balance</source>
        <translation>Cân bằng</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed token balance</source>
        <translation>Số dư mã thông báo đã được xác nhận</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pending</source>
        <translation>Chưa giải quyết</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed token balance</source>
        <translation>Số dư mã thông báo chưa được xác nhận</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sub</source>
        <translation>Dưới</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If checked then this item is a subgroup.</source>
        <translation>Nếu được chọn thì mục này là một nhóm con.</translation>
    </message>
    <message>
        <location filename="../tokensviewdialog.cpp" line="+367"/>
        <location line="+7"/>
        <location line="+4"/>
        <source>Data</source>
        <translation>Dữ liệu</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>string</source>
        <translation>chuỗi ký tự</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+4"/>
        <source>num</source>
        <translation>con số</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>NaN</source>
        <translation>NaN</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+84"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Total Mintage:</source>
        <translation>Tổng số tiền:</translation>
    </message>
    <message>
        <location line="-93"/>
        <location line="+94"/>
        <source>Token mintage is unavailable because the database needs a reindex</source>
        <translation>Việc tạo mã thông báo không khả dụng vì cơ sở dữ liệu cần lập chỉ mục lại</translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+43"/>
        <source>Name:</source>
        <translation>Tên:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Ticker:</source>
        <translation>Biểu tượng:</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+43"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+43"/>
        <source>Hash:</source>
        <translation>Băm:</translation>
    </message>
    <message>
        <location line="-39"/>
        <location line="+43"/>
        <source>Decimals:</source>
        <translation>Số thập phân:</translation>
    </message>
    <message>
        <location line="-37"/>
        <location line="+14"/>
        <location line="+62"/>
        <location line="+14"/>
        <source>Current Authorities:</source>
        <translation>Cơ quan có thẩm quyền hiện tại:</translation>
    </message>
    <message>
        <location line="-88"/>
        <location line="+76"/>
        <source>Mint:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Melt:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Renew:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Rescript:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-74"/>
        <location line="+76"/>
        <source>Subgroup:</source>
        <translation></translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+76"/>
        <source>Token authorities are unavailable because the database needs a reindex</source>
        <translation>Cơ quan quản lý mã thông báo không khả dụng vì cơ sở dữ liệu cần lập chỉ mục lại</translation>
    </message>
    <message>
        <location line="-71"/>
        <location line="+77"/>
        <source>Balance:</source>
        <translation>Cân bằng:</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+77"/>
        <source>Pending:</source>
        <translation>Chưa giải quyết:</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Are you sure you want to send?</source>
        <translation>Bạn có chắc chắn muốn gửi không?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;b&gt;Token(s)&lt;/b&gt;</source>
        <translation>&lt;b&gt;Mã thông báo&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Confirm send tokens</source>
        <translation>Xác nhận gửi mã thông báo</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message numerus="yes">
        <location filename="../transactiondesc.cpp" line="+34"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Mở thêm %n khối nữa</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open until %1</source>
        <translation>Mở cho đến %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>double spent</source>
        <translation>chi tiêu gấp đôi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation>bị bỏ rơi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>conflicted</source>
        <translation>mâu thuẫn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/ngoại tuyến</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/chưa được xác nhận</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>xác nhận %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>Tình trạng</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, vẫn chưa được phát sóng thành công</translation>
    </message>
    <message numerus="yes">
        <location line="+2"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, phát sóng qua %n nút</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Ngày</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Nguyên nhân</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Đã tạo</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+114"/>
        <source>To</source>
        <translation>Cho</translation>
    </message>
    <message>
        <location line="-163"/>
        <location line="+13"/>
        <location line="+128"/>
        <source>From</source>
        <translation>Từ</translation>
    </message>
    <message>
        <location line="-128"/>
        <source>unknown</source>
        <translation>không xác định</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>change address</source>
        <translation>Dịa chỉ đổi xu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>Dịa chỉ của riêng tôi</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+119"/>
        <source>watch-only</source>
        <translation>chỉ xem</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+28"/>
        <location line="+113"/>
        <source>label</source>
        <translation>nhãn dữ liệu</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+63"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation>Nhãn công cộng:</translation>
    </message>
    <message>
        <location line="-167"/>
        <source>Freeze until</source>
        <translation>Đóng băng cho đến khi</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+213"/>
        <source>Credit</source>
        <translation>Tín dụng</translation>
    </message>
    <message numerus="yes">
        <location line="-361"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>trưởng thành sau %n khối nữa</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>không được chấp nhận</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+218"/>
        <source>Debit</source>
        <translation>Ghi nợ</translation>
    </message>
    <message>
        <location line="-237"/>
        <source>Total debit</source>
        <translation>Tổng số ghi nợ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Tổng số tín dụng</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Phí giao dịch</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Số lượng tịnh</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+12"/>
        <source>Message</source>
        <translation>Tin nhắn</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Comment</source>
        <translation>Bình luận</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction Idem</source>
        <translation>Idem giao dịch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation>Quy mô giao dịch</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Thương gia</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Đồng xu được tạo phải hoàn thiện %1 khối trước khi chúng có thể được chi tiêu. Khi bạn tạo khối này, nó sẽ được phát lên mạng để thêm vào chuỗi khối. Nếu không vào được chuỗi, trạng thái của nó sẽ chuyển thành &quot;không được chấp nhận&quot; và số tiền đó sẽ không thể chi tiêu được. Điều này đôi khi có thể xảy ra nếu một nút khác tạo ra một khối cách nút của bạn vài giây.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Token ID</source>
        <translation>ID mã thông báo</translation>
    </message>
    <message>
        <location line="-50"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Ticker</source>
        <translation>Biểu tượng</translation>
    </message>
    <message>
        <location line="-54"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Name</source>
        <translation>Tên</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+24"/>
        <location line="+31"/>
        <source>Decimals</source>
        <translation>Số thập phân</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>Melt</source>
        <translation>Tan chảy</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+34"/>
        <source>Mint</source>
        <translation>Xu bạc hà</translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Amount Sent</source>
        <translation>Số tiền đã gửi</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Amount Received</source>
        <translation>Số tiền nhận được</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Debug information</source>
        <translation>Thông tin gỡ lỗi</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Giao dịch</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>Đầu vào</translation>
    </message>
    <message>
        <location line="-95"/>
        <location line="+28"/>
        <location line="+34"/>
        <location line="+55"/>
        <source>Amount</source>
        <translation>Số lượng</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>đúng</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>không đúng</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>Chi tiết giao dịch</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Khung này hiển thị mô tả chi tiết về giao dịch</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation>tps</translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Ngày</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Loại</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address or Label</source>
        <translation>Địa chỉ hoặc Nhãn</translation>
    </message>
    <message numerus="yes">
        <location line="+60"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Mở thêm %n khối nữa</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation>Mở cho đến %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation>Ngoại tuyến</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Chưa được xác nhận</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Đang xác nhận (%1 trong số %2 xác nhận được đề xuất)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Đã xác nhận (%1 xác nhận)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation>Mâu thuẫn</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Chi tiêu gấp đôi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Bị bỏ rơi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Chưa trưởng thành (xác nhận %1, sẽ có sau %2)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Khối này không được bất kỳ nút nào khác nhận được và có thể sẽ không được chấp nhận!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Đã tạo nhưng không được chấp nhận</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Received with</source>
        <translation>Đã nhận được với</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Nhận được tư</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Gửi đến</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Thanh toán cho chính bạn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Đã khai thác</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Nhãn công cộng</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Khác</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>chỉ xem</translation>
    </message>
    <message>
        <location line="+276"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Trạng thái giao dịch. Di chuột qua trường này để hiển thị số lượng xác nhận.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Ngày và giờ giao dịch được nhận.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Loại giao dịch.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Địa chỉ chỉ xem có tham gia vào giao dịch này hay không.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Mục đích/mục đích của giao dịch do người dùng xác định.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Số tiền được loại bỏ hoặc thêm vào số dư.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Tất cả</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Hôm nay</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Tuần này</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Tháng này</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Tháng trước</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Năm nay</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Phạm vi...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Đã nhận được với</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Gửi đến</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Cho chính bạn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Đã khai thác</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Khác</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Nhập địa chỉ hoặc nhãn để tìm kiếm</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Số tiền tối thiểu</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Sao chép địa chỉ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Sao chép nhãn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Sao chép số tiền</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction idem</source>
        <translation>Sao chép id giao dịch</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy raw transaction</source>
        <translation>Sao chép giao dịch thô</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>Chỉnh sửa nhãn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Hiển thị chi tiết giao dịch</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Tập tin tách biệt bởi dấu phẩy (*.csv)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Export Transaction History</source>
        <translation>Xuất lịch sử giao dịch</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Confirmed</source>
        <translation>Đã xác nhận</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation>Chỉ xem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>Ngày</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Loại</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Nhãn dữ liệu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Địa chỉ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exporting Failed</source>
        <translation>Xuất không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>Đã xảy ra lỗi khi lưu lịch sử giao dịch vào %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Xuất thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>Lịch sử giao dịch đã được lưu thành công vào %1.</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Range:</source>
        <translation>Phạm vi:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>cho</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Đơn vị hiển thị số tiền. Bấm để chọn đơn vị khác.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation>Unlimited</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation>Khai thác mỏ</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation>Khối lớn nhất sẽ được khai thác</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation>Kích thước khối được tạo tối đa (byte)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation>Mạng</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation>Giới hạn băng thông tính bằng KBytes/giây (chọn để bật):</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation>Gửi</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation>Tối đa</translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation>Trung bình</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation>Nhận được</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation>Các tùy chọn dòng lệnh hiện hoạt ghi đè các tùy chọn trên:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation>Đặt lại tất cả các tùy chọn máy khách về mặc định.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>Đặt lại tùy chọn</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>ĐƯỢC RỒI</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>Hủy bỏ</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation>Xác nhận đặt lại tùy chọn</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation>Đây là thiết lập lại toàn cầu của tất cả các cài đặt!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation>Yêu cầu khởi động lại máy khách để kích hoạt các thay đổi.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Khách hàng sẽ bị tắt. Bạn có muốn tiếp tục?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Thông số định hình lưu lượng truy cập ngược dòng không được để trống</translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation>Các tham số định hình lưu lượng phải lớn hơn 0.</translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+31"/>
        <source>No wallet has been loaded.</source>
        <translation>Không có ví nào được tải.</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Token Database Status</source>
        <translation>Trạng thái cơ sở dữ liệu mã thông báo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Token wallet not available because a -reindex is needed. Click &quot;Ok&quot; to perform a one time -reindex on the next startup. Then shutdown Nexa-Qt and restart to complete the process.</source>
        <translation>Ví mã thông báo không có sẵn vì cần có -reindex. Nhấp vào &quot;Ok&quot; để thực hiện -reindex một lần trong lần khởi động tiếp theo. Sau đó tắt Nexa-Qt và khởi động lại để hoàn tất quá trình.</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+291"/>
        <source>Send Coins</source>
        <translation>Gửi xu</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+46"/>
        <location line="+20"/>
        <source>&amp;Export</source>
        <translation>Xuất khẩu</translation>
    </message>
    <message>
        <location line="-19"/>
        <location line="+20"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Xuất dữ liệu trong tab hiện tại sang tệp</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Backup Wallet</source>
        <translation>Ví dự phòng</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+21"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Dữ liệu Ví (*.dat)</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Backup Failed</source>
        <translation>Sao lưu không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>Đã xảy ra lỗi khi lưu dữ liệu ví vào %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Backup Successful</source>
        <translation>Sao lưu thành công</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>Dữ liệu ví đã được lưu thành công vào %1.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Restore Wallet</source>
        <translation>Khôi phục ví</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Restore Failed</source>
        <translation>Khôi phục không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to restore the wallet data to %1.</source>
        <translation>Đã xảy ra lỗi khi cố khôi phục dữ liệu ví về %1.</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+12"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>Nhà phát triển %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation>Bitcoin Bitcoin XT và Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation>-wallet.maxTxFee được đặt rất cao! Khoản phí lớn này có thể được thanh toán trong một giao dịch.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation>-wallet.payTxFee được đặt rất cao! Đây là phí giao dịch bạn sẽ phải trả nếu thực hiện giao dịch.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>Không thể khóa thư mục dữ liệu %s. %s có lẽ đã chạy rồi.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation>Không thể tìm thấy thông tin xác thực RPC. Không tìm thấy cookie xác thực và không có mật khẩu rpc nào được đặt trong tệp cấu hình (%s)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation>Tệp cấu hình triển khai &apos;%s&apos; chứa dữ liệu không hợp lệ - xem debug.log</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Được phân phối theo giấy phép phần mềm MIT, xem tệp đính kèm SAO CHÉP hoặc &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>Lỗi tải %s: Bạn không thể bật HD trên ví không phải HD hiện có</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>Lỗi đọc %s! Tất cả các khóa đều đọc chính xác nhưng dữ liệu giao dịch hoặc mục nhập sổ địa chỉ có thể bị thiếu hoặc không chính xác.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation>Lỗi đọc từ cơ sở dữ liệu tiền xu.
Chi tiết: %s

Bạn có muốn lập chỉ mục lại vào lần khởi động lại tiếp theo không?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Lỗi: Không thể nghe kết nối đến (lỗi trả về nghe %s)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation>Không thể nghe trên tất cả các cổng P2P. Không theo yêu cầu của -bindallorfail.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation>Phí: %ld lớn hơn mức phí tối đa được phép được định cấu hình là: %ld. Để thay đổi, hãy đặt &apos;wallet.maxTxFee&apos;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation>Số tiền không hợp lệ cho -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (ít nhất phải bằng phí chuyển tiếp tối thiểu là %s để tránh giao dịch bị kẹt)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation>Số tiền không hợp lệ cho -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (ít nhất phải là %s)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>Vui lòng kiểm tra xem ngày giờ trên máy tính của bạn có chính xác không! Nếu đồng hồ của bạn sai, %s sẽ không hoạt động bình thường.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>Cắt bớt cấu hình dưới mức tối thiểu %d MiB. Vui lòng sử dụng số cao hơn.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>Prune: đồng bộ hóa ví lần cuối vượt xa dữ liệu được cắt bớt. Bạn cần -reindex (tải lại toàn bộ blockchain trong trường hợp nút bị cắt bớt)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation>Giảm kết nối -maxconnections %d xuống %d do giới hạn bộ mô tả tệp (unix) hoặc giới hạn winsocket fd_set (windows). Nếu bạn là người dùng windows, giới hạn trên cứng là 1024 không thể thay đổi bằng cách điều chỉnh cấu hình của nút.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Rescans are not possible in pruned mode. You will need to use -reindex which will download the whole blockchain again.</source>
        <translation>Không thể quét lại ở chế độ cắt bớt. Bạn sẽ cần sử dụng -reindex để tải lại toàn bộ blockchain.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>Cơ sở dữ liệu khối chứa một khối dường như đến từ tương lai. Điều này có thể do ngày và giờ trên máy tính của bạn được đặt không chính xác. Chỉ xây dựng lại cơ sở dữ liệu khối nếu bạn chắc chắn rằng ngày giờ trên máy tính của mình là chính xác</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>Số tiền giao dịch quá nhỏ để gửi sau khi đã trừ phí</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>Đây là bản dựng thử nghiệm trước khi phát hành - bạn tự chịu rủi ro khi sử dụng - không sử dụng cho các ứng dụng khai thác hoặc thương mại</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>Sản phẩm này bao gồm phần mềm do Dự án OpenSSL phát triển để sử dụng trong Bộ công cụ OpenSSL &lt;https://www.openssl.org/&gt; và phần mềm mật mã do Eric Young viết và phần mềm UPnP do Thomas Bernard viết.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation>Tổng độ dài của chuỗi phiên bản mạng có thêm nhận xét ua đã vượt quá độ dài tối đa (%i) và đã bị cắt bớt. Giảm số lượng hoặc kích thước của uacomments để tránh bị cắt bớt.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation>Giao dịch có %d đầu vào và %d đầu ra. Đầu vào tối đa được phép là %d và đầu ra tối đa là %d</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation>Giao dịch có đầu vào %d. Đầu vào tối đa được phép là %d. Hãy thử giảm đầu vào bằng cách chuyển một lượng nhỏ hơn.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>CẢNH BÁO: số khối được tạo cao bất thường, %d khối nhận được trong %d giờ qua (%d dự kiến)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>CẢNH BÁO: kiểm tra kết nối mạng của bạn, %d khối đã nhận được trong %d giờ qua (%d dự kiến)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation>Ví không được bảo vệ bằng mật khẩu. Tiền của bạn có thể gặp rủi ro! Đi tới &quot;Cài đặt&quot; và sau đó chọn &quot;Mã hóa ví&quot; để tạo mật khẩu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation>Cảnh báo: Không thể mở tệp CSV cấu hình triển khai &apos;%s&apos; để đọc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>Cảnh báo: Mạng dường như không hoàn toàn đồng ý! Một số thợ mỏ dường như đang gặp vấn đề.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>Cảnh báo: Các phiên bản khối không xác định đang được khai thác! Có thể các quy định chưa biết đang có hiệu lực</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>Cảnh báo: Tệp ví bị hỏng, dữ liệu đã được cứu! %s gốc được lưu dưới dạng %s trong %s; nếu số dư hoặc giao dịch của bạn không chính xác, bạn nên khôi phục từ bản sao lưu.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>Cảnh báo: Chúng tôi dường như không hoàn toàn đồng ý với các đồng nghiệp của mình! Bạn có thể cần nâng cấp hoặc các nút khác có thể cần nâng cấp.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to restore the same wallet which you are trying to replace.</source>
        <translation>Bạn đang cố gắng khôi phục lại chính chiếc ví mà bạn đang cố gắng thay thế.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation>Bạn đang cố gắng sử dụng -wallet.auto nhưng cả -spendzeroconfchange lẫn -wallet.instant đều không được bật</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You can not run &quot;-salvagewallet&quot; as an HD wallet.

Please relaunch Nexa with &quot;-usehd=0&quot;.</source>
        <translation>Bạn không thể chạy &quot;-salvagewallet&quot; dưới dạng ví HD.

Vui lòng khởi chạy lại Nexa với &quot;-usehd=0&quot;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation>Bạn không thể gửi giao dịch miễn phí nếu bạn đã định cấu hình -relay.limitFreeRelay bằng 0</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain</source>
        <translation>Bạn cần xây dựng lại cơ sở dữ liệu bằng cách sử dụng -reindex để quay lại chế độ không được cắt xén. Điều này sẽ tải xuống lại toàn bộ blockchain</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&quot;Restore Wallet&quot; succeeded and a backup of the previous wallet was saved to: %s.


When you click &quot;OK&quot; Nexa will shutdown to complete the process.</source>
        <translation>&quot;Khôi phục Ví&quot; đã thành công và bản sao lưu của ví trước đó đã được lưu vào: %s.


Khi bạn nhấp vào &quot;OK&quot; Nexa sẽ tắt để hoàn tất quá trình.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation>%s </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s bị hỏng, việc cứu hộ không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation>-maxtxpool phải có ít nhất %d MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation>-xthinbloomfiltersize phải có ít nhất %d Byte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Activating best chain...</source>
        <translation>Kích hoạt chuỗi tốt nhất...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot downgrade wallet</source>
        <translation>Không thể hạ cấp ví</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>Không thể giải quyết -địa chỉ liên kết: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>Không thể giải quyết -địa chỉ  -externalip:&apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>Không thể giải quyết địa chỉ -whitebind: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot write default address</source>
        <translation>Không thể ghi địa chỉ mặc định</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation>Giao dịch cam kết không thành công.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Corrupted block database detected</source>
        <translation>Cơ sở dữ liệu khối bị hỏng được phát hiện</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation>Không tìm thấy tệp cấu hình triển khai &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>Bạn có muốn xây dựng lại cơ sở dữ liệu khối bây giờ không?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Done loading</source>
        <translation>Tải xong</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing block database</source>
        <translation>Lỗi khởi tạo cơ sở dữ liệu khối</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Lỗi khởi tạo môi trường cơ sở dữ liệu ví %s!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s</source>
        <translation>Lỗi tải %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>Lỗi tải %s: Ví bị hỏng</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>Lỗi tải %s: Ví yêu cầu phiên bản mới hơn của %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>Lỗi tải %s: Bạn không thể tắt HD trên ví HD hiện có</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error opening block database</source>
        <translation>Lỗi mở cơ sở dữ liệu khối</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error</source>
        <translation>Lỗi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>Lỗi: Đã xảy ra lỗi nội bộ nghiêm trọng, hãy xem debug.log để biết chi tiết</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: Disk space is low!</source>
        <translation>Lỗi: Dung lượng ổ đĩa sắp hết!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: Keypool ran out, please call keypoolrefill first</source>
        <translation>Lỗi: Keypool hết, vui lòng gọi keypoolrefill trước</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Không thể nghe trên bất kỳ cổng nào. Sử dụng -listen=0 nếu bạn muốn điều này.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>Đang nhập khẩu...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>Khối nguồn gốc không chính xác hoặc không được tìm thấy. Datadir sai cho mạng?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Information</source>
        <translation>Thông tin</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>Kiểm tra độ chính xác khởi tạo không thành công. %s đang tắt.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds for this token.  Need %d more.</source>
        <translation>Không đủ tiền cho mã thông báo này. Cần thêm %d.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation>Không đủ tiền hoặc tiền không được xác nhận</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Địa chỉ -onion không hợp lệ: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Địa chỉ -proxy không hợp lệ: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>Mặt nạ mạng không hợp lệ được chỉ định trong -whitelist: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation>Keypool đã hết, vui lòng gọi keypoolrefill trước</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation>Đang tải bể mồ côi</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation>Đang tải TxPool</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading addresses...</source>
        <translation>Đang tải địa chỉ...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading banlist...</source>
        <translation>Đang tải danh sách cấm...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading block index...</source>
        <translation>Đang tải chỉ mục khối...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>Đang tải ví...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>Cần chỉ định một cổng với -whitebind: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Not enough file descriptors available.</source>
        <translation>Không có đủ bộ mô tả tập tin.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Block database...</source>
        <translation>Đang mở cơ sở dữ liệu khối...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation>Đang mở cơ sở dữ liệu Coins Cache...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Description database...</source>
        <translation>Đang mở cơ sở dữ liệu Mô tả mã thông báo...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Token Mintage database...</source>
        <translation>Đang mở cơ sở dữ liệu Token Mintage...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation>Đang mở cơ sở dữ liệu UTXO...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>Prune không thể được cấu hình với giá trị âm.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>Chế độ Prune không tương thích với -txindex.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pruning blockstore...</source>
        <translation>Cắt tỉa cửa hàng khối...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation>Chấp nhận lại các giao dịch trên Ví</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>Đang quét lại...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing token transaction failed</source>
        <translation>Giao dịch mã thông báo ký không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing transaction failed</source>
        <translation>Ký giao dịch không thành công</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation>Bắt đầu txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>Số tiền giao dịch quá nhỏ để trả phí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Đây là phần mềm thử nghiệm.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Số tiền giao dịch quá nhỏ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>Số tiền giao dịch phải dương</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation>Giao dịch có đầu ra %d. Đầu ra tối đa được phép là %d</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation>Giao dịch %d byte quá lớn. Tối đa được phép là %d byte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>Giao dịch quá lớn so với chính sách phí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation>Tắt tự động hợp nhất và thử gửi lại.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>Không thể liên kết với %s trên máy tính này (liên kết trả về lỗi %s)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>Không thể liên kết với %s trên máy tính này. %s có lẽ đã chạy rồi.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation>Không thể khởi động dịch vụ RPC. Xem nhật ký gỡ lỗi để biết chi tiết.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>Mạng không xác định được chỉ định trong -onlynet: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation>Đang nâng cấp cơ sở dữ liệu khối...Quá trình này có thể mất một lúc.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation>Nâng cấp cơ sở dữ liệu txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation>Đang nâng cấp cơ sở dữ liệu txindex...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>Nhận xét Tác nhân Người dùng (%s) chứa các ký tự không an toàn.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>Đang xác minh khối...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>Đang xác minh ví...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation>Đang chờ Khối Genesis...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>Ví %s nằm ngoài thư mục dữ liệu %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>Cần phải viết lại ví: khởi động lại %s để hoàn tất</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning</source>
        <translation>Cảnh báo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zapping all transactions from wallet...</source>
        <translation>Chuyển đổi tất cả các giao dịch từ ví...</translation>
    </message>
</context>
</TS>
