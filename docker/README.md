# Nexa docker files

The 3 folders respectively contain:

- `current-stable`: docker files to build and publish the latest release of Nexa
- `gitlab-ci`: docker files to crate the docker image used in Nexa CI (deprecated, soon to be removed)
- `testing`: docker and configuration files to locally troubleshoot errors occurred while running nexa CI on gitlab.com
